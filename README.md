# thinksolutions

Eliminate miscommunication, reduce administrative work, avoid lost reports Gain trust, increase accountability, saves time!

Documentation Task
--------------------

1) All the API function is in HttpHelper.java

2) The base url for API is located at string.xml under the name base_url

3) All database insertion, update delete and selection is located at CRUDHelper.java

4) All create table SQL is located at DBHelper.java

Manual Build apk
--------------

The key store path must pin point to taskmgr.jks in taskmgr folder.

The key store password is abc123, the key alias is abc123, the key password is abc123..

The version name, version code is programmed in build.gradle (Module:app).



API Reference
--------------

```
http://thinksolutions.com.my/tmanager/Help

```


Library Reference
-----------------
* https://github.com/flavienlaurent/datetimepicker

* https://github.com/bumptech/glide

* https://github.com/umano/AndroidSlidingUpPanel

* https://github.com/JodaOrg/joda-time


Design Flow
--------------------
![GitHub Logo](https://bitbucket.org/mysolution_web_android/think_android/raw/a80154cc5bccd4b1a8e6ee8107978bcc58201754/mobile-App.jpg | width=500)
