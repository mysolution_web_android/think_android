package com.applab.taskmgr.Dashboard.activity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.applab.taskmgr.Dashboard.adapter.DashboardAdapter;
import com.applab.taskmgr.Menu.fragment.NavigationDrawerFragment;
import com.applab.taskmgr.MyTask.activity.MyTaskActivity;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Task.activity.TaskManagerActivity;
import com.applab.taskmgr.Task.model.Task;
import com.applab.taskmgr.Utilities.CircleTransform;
import com.applab.taskmgr.Utilities.ItemClickSupport;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 -- =============================================
 -- Author     : Vicknesh Balasubramaniam
 -- Create date: 20/1/2016
 -- Description: AssigneeList .java
 -- =============================================
 HISTORY OF UPDATE

 NO     DEVELOPER         DATETIME                      DESCRIPTION
 ********************************************************************************
 1
 2
 */

public class DashboardActivity extends AppCompatActivity {
    private Toolbar mToolbar;
    private TextView mTxtToolbarTitle;
    private RecyclerView mRecyclerView;
    private int mLoaderId = 568;
    private Cursor mCursor;
    private DashboardAdapter mAdapter;
    private NavigationDrawerFragment mDrawerFragment;
    private Context context;
    private ImageView imageProfile;
    private ArrayList<Task> mTask = new ArrayList<>();
    private DrawerLayout mDrawerLayout;
    private LinearLayout btnView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mToolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(mToolbar);
        mTxtToolbarTitle = (TextView) mToolbar.findViewById(R.id.txtTitle);
        mTxtToolbarTitle.setText(getString(R.string.title_activity_dashboard));
        mDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        mDrawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        mDrawerFragment.setSelectedPosition(0);
        mDrawerFragment.mDrawerToggle.setDrawerIndicatorEnabled(false);
        mToolbar.setNavigationIcon(R.mipmap.menu);
        mToolbar.setNavigationOnClickListener(mToolbarOnClickListener);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);

        mAdapter = new DashboardAdapter(this, mTask);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        ItemClickSupport.addTo(mRecyclerView).setOnItemClickListener(mItemClickListener);
        ItemClickSupport.addTo(mRecyclerView).setOnItemLongClickListener(mItemLongClickListener);

        imageProfile = (ImageView) findViewById(R.id.imgProfile);

        //Glide - for photo
        Glide.with(this)
                .load(R.drawable.profile_photo)
                .placeholder(R.mipmap.ic_action_person_light)
                .transform(new CircleTransform(this))
                .into(imageProfile);

        btnView = (LinearLayout)findViewById(R.id.btnView);
        btnView.setOnClickListener(mViewAllTask);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mDrawerFragment.setOpenCloseDrawer();
    }

    @Override
    public void onBackPressed() {

        if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            mDrawerLayout.closeDrawer(Gravity.LEFT);
        } else {
            finish();
        }
        moveTaskToBack(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return mDrawerFragment.mDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }


    private ItemClickSupport.OnItemClickListener mItemClickListener = new ItemClickSupport.OnItemClickListener() {
        @Override
        public void onItemClicked(RecyclerView recyclerView, final int position, View v) {
            TextView txtName = (TextView) v.findViewById(R.id.txtName);
            Task task = (Task) txtName.getTag();
            Intent intent = new Intent(DashboardActivity.this, MyTaskActivity.class);
            startActivity(intent);
        }
    };

    private ItemClickSupport.OnItemLongClickListener mItemLongClickListener = new ItemClickSupport.OnItemLongClickListener() {
        @Override
        public boolean onItemLongClicked(RecyclerView recyclerView, final int position, View v) {
            return false;
        }
    };

    private View.OnClickListener mToolbarOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mDrawerFragment.setOpenCloseDrawer();
        }
    };

    private View.OnClickListener mViewAllTask = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(DashboardActivity.this, TaskManagerActivity.class);
            startActivity(intent);
        }
    };



}
