package com.applab.taskmgr.Dashboard.adapter;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applab.taskmgr.Dashboard.holder.DashboardViewHolder;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Task.model.Task;

import java.util.ArrayList;

/**
 -- =============================================
 -- Author     : Vicknesh Balasubramaniam
 -- Create date: 20/1/2016
 -- Description: AssigneeList .java
 -- =============================================
 HISTORY OF UPDATE

 NO     DEVELOPER         DATETIME                      DESCRIPTION
 ********************************************************************************
 1
 2
 */

public class DashboardAdapter extends RecyclerView.Adapter<DashboardViewHolder> {
    private LayoutInflater inflater;
    private ArrayList<Task> mTask = new ArrayList<>();
    private Context context;

    public DashboardAdapter(Context context, ArrayList<Task> task) {
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        mTask = task;
    }

    @Override
    public DashboardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i("DashboardViewHolder", "onCreateViewHolder called");
        View view = inflater.inflate(R.layout.custom_dashboard_row, parent, false);
        DashboardViewHolder holder = new DashboardViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(DashboardViewHolder holder, int position) {
        Task current = mTask.get(position);
        ;

    }

    @Override
    public int getItemCount() {
        return mTask.size();
    }


    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            DashboardAdapter.this.notifyDataSetChanged();
        }
    };


}
