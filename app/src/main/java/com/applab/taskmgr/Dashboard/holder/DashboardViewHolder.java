package com.applab.taskmgr.Dashboard.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.applab.taskmgr.R;

/**
 -- =============================================
 -- Author     : Vicknesh Balasubramaniam
 -- Create date: 20/1/2016
 -- Description: AssigneeList .java
 -- =============================================
 HISTORY OF UPDATE

 NO     DEVELOPER         DATETIME                      DESCRIPTION
 ********************************************************************************
 1
 2
 */
public class DashboardViewHolder extends RecyclerView.ViewHolder {
    private TextView txtTitle, txtName;

    public TextView getTxtTitle() {
        return txtTitle;
    }

    public TextView getTxtName() {
        return txtName;
    }

    public DashboardViewHolder(View itemView) {
        super(itemView);
        txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
        txtName = (TextView) itemView.findViewById(R.id.txtName);
    }
}
