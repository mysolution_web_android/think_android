package com.applab.taskmgr.Utilities;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.applab.taskmgr.AllMessage.provider.TaskMessageProvider;
import com.applab.taskmgr.AssignTo.provider.UserProvider;
import com.applab.taskmgr.CheckInOut.provider.CheckInOutProvider;
import com.applab.taskmgr.Login.provider.MessageProvider;
import com.applab.taskmgr.Login.provider.TokenProvider;
import com.applab.taskmgr.MyTask.provider.CountProvider;
import com.applab.taskmgr.Profile.provider.ProfileProvider;
import com.applab.taskmgr.Task.provider.AssigneeProvider;
import com.applab.taskmgr.Task.provider.TaskProvider;

/**
 * -- =============================================
 * -- Author     : Vicknesh Balasubramaniam
 * -- Create date: 19/1/2016
 * -- Description: AssigneeList .java
 * -- =============================================
 * HISTORY OF UPDATE
 * <p/>
 * NO     DEVELOPER         DATETIME                      DESCRIPTION
 * *******************************************************************************
 * 1
 * 2
 */
public class DBHelper extends SQLiteOpenHelper {
    //Database Applab
    public static final String DATABASE_NAME = "applab.db";

    private Context mContext;

    //Table Dashboard
    public static final String MENU_TABLE_NAME = "applab_dashboard";
    public static final String MENU_COLUMN_ID = "id";
    public static final String MENU_COLUMN_TITLE = "title";
    public static final String MENU_COLUMN_NO = "no";

    //Table Message Setting
    public static final String MESSAGE_TABLE_NAME = "applab_message";
    public static final String MESSAGE_COLUMN_ID = "id";
    public static final String MESSAGE_COLUMN_CODE = "code";
    public static final String MESSAGE_COLUMN_MESSAGE = "message";
    public static final String MESSAGE_COLUMN_STATUS = "status";
    public static final String MESSAGE_COLUMN_CREATED_DATE = "created_date";
    public static final String MESSAGE_COLUMN_UPDATED_DATE = "updated_date";

    //Table Token
    public static final String TOKEN_TABLE_NAME = "applab_token";
    public static final String TOKEN_COLUMN_ID = "id";
    public static final String TOKEN_COLUMN_ACCESS_TOKEN = "access_token";
    public static final String TOKEN_COLUMN_USER_ID = "user_id";
    public static final String TOKEN_COLUMN_EXPIRED_DATE = "expired_date";

    //Table Profile
    public static final String PROFILE_TABLE_NAME = "applab_profile";
    public static final String PROFILE_COLUMN_ID = "id";
    public static final String PROFILE_COLUMN_USER_ID = "user_id";
    public static final String PROFILE_COLUMN_NAME = "name";
    public static final String PROFILE_COLUMN_COMPANY_NAME = "company_name";
    public static final String PROFILE_COLUMN_COMPANY_ADDRESS = "company_address";
    public static final String PROFILE_COLUMN_EMAIL = "email";
    public static final String PROFILE_COLUMN_COMPANY_ID = "company_id";
    public static final String PROFILE_COLUMN_CONTACT_NO = "contact_no";
    public static final String PROFILE_COLUMN_MOBILE = "mobile";
    public static final String PROFILE_COLUMN_DESIGNATION = "designation";
    public static final String PROFILE_COLUMN_DEPARTMENT_ID = "department_id";
    public static final String PROFILE_COLUMN_DEPARTMENT_NAME = "department_name";
    public static final String PROFILE_COLUMN_PROFILE_IMAGE = "profile_image";
    public static final String PROFILE_COLUMN_USER_START_DATE = "user_start_date";
    public static final String PROFILE_COLUMN_USER_END_DATE = "user_end_date";
    public static final String PROFILE_COLUMN_PASSWORD_LAST_CHANGED_DATE = "password_last_changed_date";
    public static final String PROFILE_COLUMN_LAST_UPDATED_DATE = "last_updated_date";

    //Table Task
    public static final String TASK_TABLE_NAME = "applab_task";
    public static final String TASK_COLUMN_ID = "id";
    public static final String TASK_COLUMN_TASK_ID = "task_id";
    public static final String TASK_COLUMN_TITLE = "title";
    public static final String TASK_COLUMN_DATE = "date";
    public static final String TASK_COLUMN_DUE_DATE = "due_date";
    public static final String TASK_COLUMN_DESCRIPTION = "description";
    public static final String TASK_COLUMN_LOCTION = "location";
    public static final String TASK_COLUMN_MAP = "map";
    public static final String TASK_COLUMN_PRIORITY = "prority";
    public static final String TASK_COLUMN_FILE = "file";
    public static final String TASK_COLUMN_ASSIGNER_ID = "assigner_id";
    public static final String TASK_COLUMN_ASSIGNER_PHOTO = "assigner_photo";
    public static final String TASK_COLUMN_ASSIGNER_NAME = "assigner_name";
    public static final String TASK_COLUMN_CREATE_DATE = "create_date";
    public static final String TASK_COLUMN_UPDATE_DATE = "update_date";
    public static final String TASK_COLUMN_UPDATE_BY = "update_by";
    public static final String TASK_COLUMN_COMPLETION_STATUS = "completion_status";
    public static final String TASK_COLUMN_TYPE = "type";
    public static final String TASK_COLUMN_LAST_CHECK_STATUS = "last_check_status";
    public static final String TASK_COLUMN_STORED_LOCATION = "stored_location";

    //AssigneeList Table
    public static final String ASSIGNEELIST_TABLE_NAME = "applab_assignee_list";
    public static final String ASSIGNEELIST_COLUMN_ID = "id";
    public static final String ASSIGNEELIST_COLUMN_USER_ID = "user_id";
    public static final String ASSIGNEELIST_COLUMN_USER_NAME = "user_name";
    public static final String ASSIGNEELIST_COLUMN_CREATE_DATE = "create_date";
    public static final String ASSIGNEELIST_COLUMN_TASK_ID = "task_id";
    public static final String ASSIGNEELIST_COLUMN_TASK_TYPE = "type";

    //Table User List
    public static final String USER_TABLE_NAME = "applab_user";
    public static final String USER_COLUMN_ID = "id";
    public static final String USER_COLUMN_USER_ID = "user_id";
    public static final String USER_COLUMN_NAME = "name";
    public static final String USER_COLUMN_COMPANY_NAME = "company_name";
    public static final String USER_COLUMN_COMPANY_ID = "company_id";
    public static final String USER_COLUMN_COMPANY_ADDRESS = "company_address";
    public static final String USER_COLUMN_CONTACT_NO = "contact_no";
    public static final String USER_COLUMN_MOBILE_NO = "mobile_no";
    public static final String USER_COLUMN_DESIGNATION = "designation";
    public static final String USER_COLUMN_DEPARTMENT_ID = "department_id";
    public static final String USER_COLUMN_DEPARTMENT_NAME = "department_name";
    public static final String USER_COLUMN_PROFILE_IMAGE = "profile_image";
    public static final String USER_COLUMN_USER_START_DATE = "user_start_date";
    public static final String USER_COLUMN_USER_END_DATE = "user_end_date";
    public static final String USER_COLUMN_PASSWORD_LAST_CHANGED_DATE = "password_last_changed_date";
    public static final String USER_COLUMN_LAST_UPDATED_DATE = "last_updated_date";

    //Check In Out Table
    public static final String CHECK_TABLE_NAME = "applab_check";
    public static final String CHECK_COLUMN_ID = "id";
    public static final String CHECK_COLUMN_TASK_ID = "task_id";
    public static final String CHECK_COLUMN_USER_ID = "user_id";
    public static final String CHECK_COLUMN_USER_NAME = "user_name";
    public static final String CHECK_COLUMN_USER_PHOTO = "user_photo";
    public static final String CHECK_COLUMN_STATUS = "check_status";
    public static final String CHECK_COLUMN_NETWORK_LAT = "network_lat";
    public static final String CHECK_COLUMN_NETWORK_LONG = "network_long";
    public static final String CHECK_COLUMN_DEVICE_LAT = "device_lat";
    public static final String CHECK_COLUMN_DEVICE_LONG = "device_long";
    public static final String CHECK_COLUMN_CREATE_DATE = "create_date";

    //Table Task Message
    public static final String TASK_MESSAGE_TABLE_NAME = "applab_task_message";
    public static final String TASK_MESSAGE_COLUMN_ID = "id";
    public static final String TASK_MESSAGE_COLUMN_TASK_ID = "task_id";
    public static final String TASK_MESSAGE_COLUMN_FROM_ID = "from_id";
    public static final String TASK_MESSAGE_COLUMN_FROM_NAME = "from_name";
    public static final String TASK_MESSAGE_COLUMN_FROM_PHOTO = "from_photo";
    public static final String TASK_MESSAGE_COLUMN_MESSAGE = "message";
    public static final String TASK_MESSAGE_COLUMN_FILE = "file";
    public static final String TASK_MESSAGE_COLUMN_CREATE_DATE = "create_date";
    public static final String TASK_MESSAGE_COLUMN_STORE_LOCATION = "store_location";

    //Count Table
    public static final String COUNT_TABLE_NAME = "applab_count";
    public static final String COUNT_COLUMN_ID = "id";
    public static final String COUNT_COLUMN_TASK_ID = "task_id";
    public static final String COUNT_MESSAGE_COUNT = "message_count";
    public static final String COUNT_CHECKING_COUNT = "checking_count";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
        mContext = context;
    }

    public void clearData() {
        mContext.getContentResolver().delete(TaskProvider.CONTENT_URI, null, null);
        mContext.getContentResolver().delete(TokenProvider.CONTENT_URI, null, null);
        mContext.getContentResolver().delete(UserProvider.CONTENT_URI, null, null);
        mContext.getContentResolver().delete(ProfileProvider.CONTENT_URI, null, null);
        mContext.getContentResolver().delete(AssigneeProvider.CONTENT_URI, null, null);
        mContext.getContentResolver().delete(CountProvider.CONTENT_URI, null, null);
        mContext.getContentResolver().delete(TaskMessageProvider.CONTENT_URI, null, null);
        mContext.getContentResolver().delete(CheckInOutProvider.CONTENT_URI, null, null);
        mContext.getContentResolver().delete(MessageProvider.CONTENT_URI, null, null);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + MENU_TABLE_NAME +
                        "(" + MENU_COLUMN_ID + " INTEGER PRIMARY KEY, " +
                        "" + MENU_COLUMN_TITLE + " TEXT," +
                        "" + MENU_COLUMN_NO + " INTEGER) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + PROFILE_TABLE_NAME +
                        "(" + PROFILE_COLUMN_ID + " INTEGER PRIMARY KEY, " +
                        "" + PROFILE_COLUMN_NAME + " TEXT," +
                        "" + PROFILE_COLUMN_USER_ID + " TEXT," +
                        "" + PROFILE_COLUMN_COMPANY_NAME + " TEXT," +
                        "" + PROFILE_COLUMN_COMPANY_ID + " TEXT," +
                        "" + PROFILE_COLUMN_CONTACT_NO + " TEXT," +
                        "" + PROFILE_COLUMN_MOBILE + " TEXT," +
                        "" + PROFILE_COLUMN_DESIGNATION + " TEXT," +
                        "" + PROFILE_COLUMN_DEPARTMENT_ID + " TEXT," +
                        "" + PROFILE_COLUMN_DEPARTMENT_NAME + " TEXT," +
                        "" + PROFILE_COLUMN_PROFILE_IMAGE + " TEXT," +
                        "" + PROFILE_COLUMN_USER_START_DATE + " TEXT," +
                        "" + PROFILE_COLUMN_USER_END_DATE + " TEXT," +
                        "" + PROFILE_COLUMN_PASSWORD_LAST_CHANGED_DATE + " TEXT," +
                        "" + PROFILE_COLUMN_COMPANY_ADDRESS + " TEXT," +
                        "" + PROFILE_COLUMN_EMAIL + " TEXT," +
                        "" + PROFILE_COLUMN_LAST_UPDATED_DATE + " TEXT) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + TASK_TABLE_NAME +
                        "(" + TASK_COLUMN_ID + " INTEGER PRIMARY KEY, " +
                        "" + TASK_COLUMN_TASK_ID + " INTEGER, " +
                        "" + TASK_COLUMN_TITLE + " TEXT, " +
                        "" + TASK_COLUMN_DESCRIPTION + " TEXT," +
                        "" + TASK_COLUMN_DATE + " TEXT," +
                        "" + TASK_COLUMN_DUE_DATE + " TEXT," +
                        "" + TASK_COLUMN_LOCTION + " TEXT," +
                        "" + TASK_COLUMN_MAP + " TEXT," +
                        "" + TASK_COLUMN_PRIORITY + " TEXT," +
                        "" + TASK_COLUMN_FILE + " TEXT," +
                        "" + TASK_COLUMN_ASSIGNER_PHOTO + " TEXT," +
                        "" + TASK_COLUMN_ASSIGNER_ID + " TEXT," +
                        "" + TASK_COLUMN_ASSIGNER_NAME + " TEXT," +
                        "" + TASK_COLUMN_CREATE_DATE + " TEXT," +
                        "" + TASK_COLUMN_COMPLETION_STATUS + " TEXT," +
                        "" + TASK_COLUMN_UPDATE_BY + " TEXT," +
                        "" + TASK_COLUMN_TYPE + " TEXT," +
                        "" + TASK_COLUMN_STORED_LOCATION + " TEXT," +
                        "" + TASK_COLUMN_LAST_CHECK_STATUS + " INTEGER," +
                        "" + TASK_COLUMN_UPDATE_DATE + " TEXT)"
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + MESSAGE_TABLE_NAME +
                        "(" + MESSAGE_COLUMN_ID + " integer primary key," +
                        "" + MESSAGE_COLUMN_CODE + " text," +
                        "" + MESSAGE_COLUMN_MESSAGE + " text," +
                        "" + MESSAGE_COLUMN_STATUS + " text," +
                        "" + MESSAGE_COLUMN_CREATED_DATE + " text," +
                        "" + MESSAGE_COLUMN_UPDATED_DATE + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + TOKEN_TABLE_NAME +
                        "(" + TOKEN_COLUMN_ID + " integer primary key, " +
                        "" + TOKEN_COLUMN_ACCESS_TOKEN + " text, " +
                        "" + TOKEN_COLUMN_USER_ID + " text, " +
                        "" + TOKEN_COLUMN_EXPIRED_DATE + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + ASSIGNEELIST_TABLE_NAME +
                        "(" + ASSIGNEELIST_COLUMN_ID + " integer primary key, " +
                        "" + ASSIGNEELIST_COLUMN_USER_ID + " text, " +
                        "" + ASSIGNEELIST_COLUMN_USER_NAME + " text, " +
                        "" + ASSIGNEELIST_COLUMN_TASK_ID + " integer, " +
                        "" + ASSIGNEELIST_COLUMN_TASK_TYPE + " text, " +
                        "" + ASSIGNEELIST_COLUMN_CREATE_DATE + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + USER_TABLE_NAME +
                        "(" + USER_COLUMN_ID + " INTEGER PRIMARY KEY, " +
                        "" + USER_COLUMN_USER_ID + " TEXT," +
                        "" + USER_COLUMN_NAME + " TEXT," +
                        "" + USER_COLUMN_COMPANY_NAME + " TEXT," +
                        "" + USER_COLUMN_COMPANY_ID + " TEXT," +
                        "" + USER_COLUMN_COMPANY_ADDRESS + " TEXT," +
                        "" + USER_COLUMN_CONTACT_NO + " TEXT," +
                        "" + USER_COLUMN_MOBILE_NO + " TEXT," +
                        "" + USER_COLUMN_DESIGNATION + " TEXT," +
                        "" + USER_COLUMN_DEPARTMENT_ID + " TEXT," +
                        "" + USER_COLUMN_DEPARTMENT_NAME + " TEXT," +
                        "" + USER_COLUMN_PROFILE_IMAGE + " TEXT," +
                        "" + USER_COLUMN_USER_START_DATE + " TEXT," +
                        "" + USER_COLUMN_USER_END_DATE + " TEXT," +
                        "" + USER_COLUMN_PASSWORD_LAST_CHANGED_DATE + " TEXT," +
                        "" + USER_COLUMN_LAST_UPDATED_DATE + " TEXT) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + CHECK_TABLE_NAME +
                        "(" + CHECK_COLUMN_ID + " integer primary key, " +
                        "" + CHECK_COLUMN_USER_ID + " text, " +
                        "" + CHECK_COLUMN_TASK_ID + " integer, " +
                        "" + CHECK_COLUMN_USER_NAME + " text, " +
                        "" + CHECK_COLUMN_USER_PHOTO + " text, " +
                        "" + CHECK_COLUMN_STATUS + " integer, " +
                        "" + CHECK_COLUMN_NETWORK_LAT + " text, " +
                        "" + CHECK_COLUMN_NETWORK_LONG + " text, " +
                        "" + CHECK_COLUMN_DEVICE_LAT + " text, " +
                        "" + CHECK_COLUMN_DEVICE_LONG + " text, " +
                        "" + CHECK_COLUMN_CREATE_DATE + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + CHECK_TABLE_NAME +
                        "(" + CHECK_COLUMN_ID + " integer primary key, " +
                        "" + CHECK_COLUMN_TASK_ID + " integer, " +
                        "" + CHECK_COLUMN_USER_ID + " text, " +
                        "" + CHECK_COLUMN_USER_NAME + " text, " +
                        "" + CHECK_COLUMN_USER_PHOTO + " text, " +
                        "" + CHECK_COLUMN_STATUS + " integer, " +
                        "" + CHECK_COLUMN_NETWORK_LAT + " text, " +
                        "" + CHECK_COLUMN_NETWORK_LONG + " text, " +
                        "" + CHECK_COLUMN_DEVICE_LAT + " text, " +
                        "" + CHECK_COLUMN_DEVICE_LONG + " text, " +
                        "" + CHECK_COLUMN_CREATE_DATE + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + TASK_MESSAGE_TABLE_NAME +
                        "(" + TASK_MESSAGE_COLUMN_ID + " integer primary key, " +
                        "" + TASK_MESSAGE_COLUMN_TASK_ID + " text, " +
                        "" + TASK_MESSAGE_COLUMN_FROM_ID + " text, " +
                        "" + TASK_MESSAGE_COLUMN_FROM_NAME + " text, " +
                        "" + TASK_MESSAGE_COLUMN_FROM_PHOTO + " text, " +
                        "" + TASK_MESSAGE_COLUMN_MESSAGE + " text, " +
                        "" + TASK_MESSAGE_COLUMN_FILE + " text, " +
                        "" + TASK_MESSAGE_COLUMN_STORE_LOCATION + " text, " +
                        "" + TASK_MESSAGE_COLUMN_CREATE_DATE + " text) "
        );

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS " + COUNT_TABLE_NAME +
                        "(" + COUNT_COLUMN_ID + " integer primary key, " +
                        "" + COUNT_CHECKING_COUNT + " text, " +
                        "" + COUNT_COLUMN_TASK_ID + " text, " +
                        "" + COUNT_MESSAGE_COUNT + " text) "
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);
    }
}
