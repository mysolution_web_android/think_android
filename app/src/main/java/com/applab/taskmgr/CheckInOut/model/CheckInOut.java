package com.applab.taskmgr.CheckInOut.model;

/**
 * Created by user on 01-Mar-16.
 */

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.applab.taskmgr.Utilities.DBHelper;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class CheckInOut implements Parcelable {

    public CheckInOut() {
    }

    public CheckInOut(Parcel in) {
        readFromParcel(in);
    }

    public void readFromParcel(Parcel in) {
        this.UserId = in.readString();
        this.UserName = in.readString();
        this.UserPhoto = in.readString();
        this.Check = in.readInt();
        this.GpsLat = in.readString();
        this.GpsLong = in.readString();
        this.InternetGpsLat = in.readString();
        this.InternetGpsLong = in.readString();
        this.CreateDate = in.readString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.UserId);
        dest.writeString(this.UserName);
        dest.writeString(this.UserPhoto);
        dest.writeInt(this.Check);
        dest.writeString(this.GpsLat);
        dest.writeString(this.GpsLong);
        dest.writeString(this.InternetGpsLat);
        dest.writeString(this.InternetGpsLong);
        dest.writeString(this.CreateDate);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @SuppressWarnings("unchecked")
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public CheckInOut createFromParcel(Parcel in) {
            return new CheckInOut(in);
        }

        @Override
        public CheckInOut[] newArray(int size) {
            return new CheckInOut[size];
        }
    };

    @SerializedName("UserId")
    @Expose
    private String UserId;
    @SerializedName("UserName")
    @Expose
    private String UserName;
    @SerializedName("UserPhoto")
    @Expose
    private String UserPhoto;
    @SerializedName("Check")
    @Expose
    private Integer Check;
    @SerializedName("GpsLat")
    @Expose
    private String GpsLat;
    @SerializedName("GpsLong")
    @Expose
    private String GpsLong;
    @SerializedName("InternetGpsLat")
    @Expose
    private String InternetGpsLat;
    @SerializedName("InternetGpsLong")
    @Expose
    private String InternetGpsLong;
    @SerializedName("CreateDate")
    @Expose
    private String CreateDate;

    private String taskId;

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    /**
     * @return The UserId
     */
    public String getUserId() {
        return UserId;
    }

    /**
     * @param UserId The UserId
     */
    public void setUserId(String UserId) {
        this.UserId = UserId;
    }

    /**
     * @return The UserName
     */
    public String getUserName() {
        return UserName;
    }

    /**
     * @param UserName The UserName
     */
    public void setUserName(String UserName) {
        this.UserName = UserName;
    }

    /**
     * @return The UserPhoto
     */
    public String getUserPhoto() {
        return UserPhoto;
    }

    /**
     * @param UserPhoto The UserPhoto
     */
    public void setUserPhoto(String UserPhoto) {
        this.UserPhoto = UserPhoto;
    }

    /**
     * @return The Check
     */
    public Integer getCheck() {
        return Check;
    }

    /**
     * @param Check The Check
     */
    public void setCheck(Integer Check) {
        this.Check = Check;
    }

    /**
     * @return The GpsLat
     */
    public String getGpsLat() {
        return GpsLat;
    }

    /**
     * @param GpsLat The GpsLat
     */
    public void setGpsLat(String GpsLat) {
        this.GpsLat = GpsLat;
    }

    /**
     * @return The GpsLong
     */
    public String getGpsLong() {
        return GpsLong;
    }

    /**
     * @param GpsLong The GpsLong
     */
    public void setGpsLong(String GpsLong) {
        this.GpsLong = GpsLong;
    }

    /**
     * @return The InternetGpsLat
     */
    public String getInternetGpsLat() {
        return InternetGpsLat;
    }

    /**
     * @param InternetGpsLat The InternetGpsLat
     */
    public void setInternetGpsLat(String InternetGpsLat) {
        this.InternetGpsLat = InternetGpsLat;
    }

    /**
     * @return The InternetGpsLong
     */
    public String getInternetGpsLong() {
        return InternetGpsLong;
    }

    /**
     * @param InternetGpsLong The InternetGpsLong
     */
    public void setInternetGpsLong(String InternetGpsLong) {
        this.InternetGpsLong = InternetGpsLong;
    }

    /**
     * @return The CreateDate
     */
    public String getCreateDate() {
        return CreateDate;
    }

    /**
     * @param CreateDate The CreateDate
     */
    public void setCreateDate(String CreateDate) {
        this.CreateDate = CreateDate;
    }

    public static CheckInOut getCheckInOut(Cursor cursor, int position) {
        cursor.moveToPosition(position);
        CheckInOut checkInOut = new CheckInOut();
        checkInOut.setUserId(cursor.getString(cursor.getColumnIndex(DBHelper.CHECK_COLUMN_USER_ID)));
        checkInOut.setUserName(cursor.getString(cursor.getColumnIndex(DBHelper.CHECK_COLUMN_USER_NAME)));
        checkInOut.setUserPhoto(cursor.getString(cursor.getColumnIndex(DBHelper.CHECK_COLUMN_USER_PHOTO)));
        checkInOut.setCheck(cursor.getInt(cursor.getColumnIndex(DBHelper.CHECK_COLUMN_STATUS)));
        checkInOut.setInternetGpsLat(cursor.getString(cursor.getColumnIndex(DBHelper.CHECK_COLUMN_NETWORK_LAT)));
        checkInOut.setInternetGpsLong(cursor.getString(cursor.getColumnIndex(DBHelper.CHECK_COLUMN_NETWORK_LONG)));
        checkInOut.setGpsLat(cursor.getString(cursor.getColumnIndex(DBHelper.CHECK_COLUMN_DEVICE_LAT)));
        checkInOut.setGpsLong(cursor.getString(cursor.getColumnIndex(DBHelper.CHECK_COLUMN_DEVICE_LONG)));
        checkInOut.setCreateDate(cursor.getString(cursor.getColumnIndex(DBHelper.CHECK_COLUMN_CREATE_DATE)));
        checkInOut.setCreateDate(cursor.getString(cursor.getColumnIndex(DBHelper.CHECK_COLUMN_CREATE_DATE)));
        checkInOut.setTaskId(cursor.getString(cursor.getColumnIndex(DBHelper.CHECK_COLUMN_TASK_ID)));
        return checkInOut;
    }

}
