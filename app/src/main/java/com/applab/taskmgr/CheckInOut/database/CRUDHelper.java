package com.applab.taskmgr.CheckInOut.database;

import android.content.ContentValues;
import android.content.Context;
import android.util.Log;
import com.applab.taskmgr.CheckInOut.model.CheckInOut;
import com.applab.taskmgr.CheckInOut.provider.CheckInOutProvider;
import com.applab.taskmgr.Task.model.Task;
import com.applab.taskmgr.Utilities.DBHelper;

/**
 * -- =============================================
 * -- Author     : Lau Pong
 * -- Create date: 02/03/2016
 * -- Description: CRUDHelper.java
 * -- =============================================
 * HISTORY OF UPDATE
 * <p/>
 * NO     DEVELOPER         DATETIME                      DESCRIPTION
 * *******************************************************************************
 * 1
 * 2
 */
public class CRUDHelper {
    public static void insertCheckInOut(Context context, String TAG, CheckInOut[] checkInOuts, Task task) {
        DBHelper helper = new DBHelper(context);
        try {
            ContentValues[] contentValueses = new ContentValues[checkInOuts.length];
            for (int i = 0; i < checkInOuts.length; i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.CHECK_COLUMN_CREATE_DATE, checkInOuts[i].getCreateDate());
                contentValues.put(DBHelper.CHECK_COLUMN_DEVICE_LAT, checkInOuts[i].getGpsLat());
                contentValues.put(DBHelper.CHECK_COLUMN_DEVICE_LONG, checkInOuts[i].getGpsLong());
                contentValues.put(DBHelper.CHECK_COLUMN_NETWORK_LAT, checkInOuts[i].getInternetGpsLat());
                contentValues.put(DBHelper.CHECK_COLUMN_NETWORK_LONG, checkInOuts[i].getInternetGpsLong());
                contentValues.put(DBHelper.CHECK_COLUMN_TASK_ID, task.getTaskId());
                contentValues.put(DBHelper.CHECK_COLUMN_USER_ID, checkInOuts[i].getUserId());
                contentValues.put(DBHelper.CHECK_COLUMN_USER_NAME, checkInOuts[i].getUserName());
                contentValues.put(DBHelper.CHECK_COLUMN_USER_PHOTO, checkInOuts[i].getUserPhoto());
                contentValues.put(DBHelper.CHECK_COLUMN_STATUS, checkInOuts[i].getCheck());
                contentValueses[i] = contentValues;
            }

            boolean isEmpty = true;
            for (ContentValues contentValues : contentValueses) {
                if (contentValues != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                context.getContentResolver().bulkInsert(CheckInOutProvider.CONTENT_URI, contentValueses);
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Check Error :" + ex.toString());
        }
    }
}
