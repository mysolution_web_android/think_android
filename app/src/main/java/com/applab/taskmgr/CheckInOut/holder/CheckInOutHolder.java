package com.applab.taskmgr.CheckInOut.holder;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.applab.taskmgr.R;

/**
 * -- =============================================
 * -- Author     : Vicknesh Balasubramaniam
 * -- Create date: 4/2/2016
 * -- Description: AssigneeList .java
 * -- =============================================
 * HISTORY OF UPDATE
 * <p/>
 * NO     DEVELOPER         DATETIME                      DESCRIPTION
 * *******************************************************************************
 * 1
 * 2
 */
public class CheckInOutHolder extends RecyclerView.ViewHolder {
    private TextView mTxtCheckInDate, mTxtNetworkGPS, mTxtDeviceGPS,mTxtStatus;
    private LinearLayout mBtnNetworkGPS;
    private LinearLayout mBtnDeviceGPS;
    private ImageView mImgProfile;

    public CheckInOutHolder(View itemView) {
        super(itemView);
        mTxtCheckInDate = (TextView) itemView.findViewById(R.id.txtCheckInDate);
        mTxtNetworkGPS = (TextView) itemView.findViewById(R.id.txtNetworkGPS);
        mTxtDeviceGPS = (TextView) itemView.findViewById(R.id.txtDeviceGPS);
        mTxtStatus = (TextView) itemView.findViewById(R.id.txtStatus);
        mBtnDeviceGPS = (LinearLayout) itemView.findViewById(R.id.btnDeviceGPS);
        mBtnNetworkGPS = (LinearLayout) itemView.findViewById(R.id.btnNetworkGPS);
        mImgProfile = (ImageView) itemView.findViewById(R.id.imgProfile);
    }

    public TextView getmTxtStatus() {
        return mTxtStatus;
    }

    public TextView getmTxtCheckInDate() {
        return mTxtCheckInDate;
    }

    public TextView getmTxtNetworkGPS() {
        return mTxtNetworkGPS;
    }

    public TextView getmTxtDeviceGPS() {
        return mTxtDeviceGPS;
    }

    public LinearLayout getmBtnNetworkGPS() {
        return mBtnNetworkGPS;
    }

    public LinearLayout getmBtnDeviceGPS() {
        return mBtnDeviceGPS;
    }

    public ImageView getmImgProfile() {
        return mImgProfile;
    }
}
