package com.applab.taskmgr.CheckInOut.activity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import com.applab.taskmgr.CheckInOut.adapter.CheckInOutAdapter;
import com.applab.taskmgr.CheckInOut.provider.CheckInOutProvider;
import com.applab.taskmgr.CheckInOut.webapi.HttpHelper;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Task.activity.TaskManagerActivity;
import com.applab.taskmgr.Task.model.Task;
import com.applab.taskmgr.Utilities.DBHelper;

/**
 * -- =============================================
 * -- Author     : Lau Pong
 * -- Create date: 4/2/2016
 * -- Description: CheckInOutActivity.java
 * -- =============================================
 * HISTORY OF UPDATE
 * <p/>
 * NO     DEVELOPER         DATETIME                      DESCRIPTION
 * *******************************************************************************
 * 1
 * 2
 */
public class CheckInOutActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private CheckInOutAdapter mAdapter;
    private Toolbar mToolbar;
    private TextView mTxtToolbarTitle;
    private Cursor mCursor;
    private Task mTask;
    private String TAG = CheckInOutActivity.class.getSimpleName();
    private int mLoaderId = 56;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_in_out);

        mToolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(mToolbar);
        mTxtToolbarTitle = (TextView) mToolbar.findViewById(R.id.txtTitle);
        mTxtToolbarTitle.setText(getString(R.string.title_activity_check_in_out));

        mToolbar.setNavigationIcon(R.mipmap.back);
        mToolbar.setNavigationOnClickListener(mToolbarOnClickListener);

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);

        mAdapter = new CheckInOutAdapter(this, mCursor);
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.setLayoutManager(mLayoutManager);
        setIntent(getIntent());
    }

    public void setIntent(Intent intent) {
        if (intent.getParcelableExtra("Task") != null) {
            mTask = intent.getParcelableExtra("Task");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getSupportLoaderManager().initLoader(mLoaderId, null, this);
        HttpHelper.getCheckInOut(CheckInOutActivity.this, TAG, mTask);
    }

    private View.OnClickListener mToolbarOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_created_task, menu);
        return true;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (mLoaderId == id) {
            return new CursorLoader(CheckInOutActivity.this, CheckInOutProvider.CONTENT_URI, null,
                    DBHelper.CHECK_COLUMN_TASK_ID + "=?", new String[]{mTask.getTaskId().toString()},
                    DBHelper.CHECK_COLUMN_CREATE_DATE + " DESC ");
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (mLoaderId == loader.getId()) {
            if (data != null) {
                mCursor = data;
                mAdapter.swapCursor(mCursor);
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
