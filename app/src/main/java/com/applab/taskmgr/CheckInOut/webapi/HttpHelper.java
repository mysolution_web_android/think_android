package com.applab.taskmgr.CheckInOut.webapi;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.applab.taskmgr.CheckInOut.model.CheckInOut;
import com.applab.taskmgr.CheckInOut.provider.CheckInOutProvider;
import com.applab.taskmgr.Login.database.CRUDHelper;
import com.applab.taskmgr.Login.model.Token;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Task.model.Task;
import com.applab.taskmgr.Utilities.AppController;
import com.applab.taskmgr.Utilities.DBHelper;
import com.applab.taskmgr.Utilities.GsonRequest;
import com.applab.taskmgr.Utilities.Utilities;

import java.util.HashMap;
import java.util.Map;

/**
 * -- =============================================
 * -- Author     : Lau Pong
 * -- Create date: 0203/2016
 * -- Description: HttpHelper.java
 * -- =============================================
 * HISTORY OF UPDATE
 * <p/>
 * NO     DEVELOPER         DATETIME                      DESCRIPTION
 * *******************************************************************************
 * 1
 * 2
 */
public class HttpHelper {
    public static void getCheckInOut(Context context, String TAG, Task task) {
        final Token token = CRUDHelper.getToken(context);
        if (token != null) {
            if (token.getToken() != null) {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", token.getToken());
                GsonRequest<CheckInOut[]> mGsonRequest = new GsonRequest<CheckInOut[]>(
                        Request.Method.GET,
                        context.getString(R.string.base_url) + "Task/GetCheckInOut?task_id=" +
                                Utilities.encode(task.getTaskId()),
                        CheckInOut[].class,
                        params,
                        responseCheckInOutListener(context, TAG, task),
                        errorCheckInOutListener(context, TAG)) {
                };

                mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utilities.TIMEOUT
                        , DefaultRetryPolicy.DEFAULT_MAX_RETRIES
                        , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
            }
        }
    }

    public static Response.Listener<CheckInOut[]> responseCheckInOutListener(final Context context, final String TAG, final Task task) {
        return new Response.Listener<CheckInOut[]>() {
            @Override
            public void onResponse(CheckInOut[] response) {
                Log.i(TAG, "Successfully Entered Data " + response);
                context.getContentResolver().delete(CheckInOutProvider.CONTENT_URI, DBHelper.CHECK_COLUMN_TASK_ID + "=?", new String[]{task.getTaskId().toString()});
                com.applab.taskmgr.CheckInOut.database.CRUDHelper.insertCheckInOut(context, TAG, response, task);
            }
        };
    }

    public static Response.ErrorListener errorCheckInOutListener(final Context context, final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utilities.serverHandlingError(context, error);
            }
        };
    }
}
