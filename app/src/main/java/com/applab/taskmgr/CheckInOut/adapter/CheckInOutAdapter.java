package com.applab.taskmgr.CheckInOut.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.applab.taskmgr.CheckInOut.holder.CheckInOutHolder;
import com.applab.taskmgr.CheckInOut.model.CheckInOut;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Utilities.CircleTransform;
import com.applab.taskmgr.Utilities.Utilities;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

/**
 * -- =============================================
 * -- Author     : Lau Pong
 * -- Create date: 02/03/2016
 * -- Description: CheckInOutAdapter.java
 * -- =============================================
 * HISTORY OF UPDATE
 * <p/>
 * NO     DEVELOPER         DATETIME                      DESCRIPTION
 * *******************************************************************************
 * 1
 * 2
 */

public class CheckInOutAdapter extends RecyclerView.Adapter<CheckInOutHolder> {
    private Context mContext;
    private Cursor mCursor;
    private LayoutInflater mLayoutInflater;

    public CheckInOutAdapter(Context context, Cursor cursor) {
        mContext = context;
        mCursor = cursor;
        mLayoutInflater = LayoutInflater.from(mContext);
    }

    @Override
    public void onBindViewHolder(CheckInOutHolder holder, int position) {
        if (mCursor != null) {
            CheckInOut checkInOut = CheckInOut.getCheckInOut(mCursor, position);
            holder.getmTxtCheckInDate().setText(Utilities.setToLocalDateTime(Utilities.DATE_FORMAT,
                    "dd MMM yyyy h:mm a", checkInOut.getCreateDate()));
            String deviceGps = String.format("%.5f", Double.valueOf(checkInOut.getGpsLat())) + ", " + String.format("%.5f", Double.valueOf(checkInOut.getGpsLong()));
            String networkGps = String.format("%.5f", Double.valueOf(checkInOut.getInternetGpsLat())) + ", " + String.format("%.5f", Double.valueOf(checkInOut.getInternetGpsLong()));
            holder.getmTxtDeviceGPS().setText(deviceGps);
            holder.getmTxtNetworkGPS().setText(networkGps);
            holder.getmTxtStatus().setText(checkInOut.getCheck() == 1 ? mContext.getString(R.string.check_in) :
                    mContext.getString(R.string.check_out));
            Glide.with(mContext)
                    .load(checkInOut.getUserPhoto())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.mipmap.ic_action_person_light)
                    .transform(new CircleTransform(mContext))
                    .into(holder.getmImgProfile());
            holder.getmBtnDeviceGPS().setTag(checkInOut);
            holder.getmBtnNetworkGPS().setTag(checkInOut);
            holder.getmBtnDeviceGPS().setOnClickListener(mBtnDeviceGPSOnClickListenener);
            holder.getmBtnNetworkGPS().setOnClickListener(mBtnNetworkGPSOnClickListenener);
        }
    }

    private View.OnClickListener mBtnDeviceGPSOnClickListenener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            LinearLayout lv = (LinearLayout) v.findViewById(R.id.btnDeviceGPS);
        }
    };

    private View.OnClickListener mBtnNetworkGPSOnClickListenener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            LinearLayout lv = (LinearLayout) v.findViewById(R.id.btnNetworkGPS);
        }
    };

    @Override
    public CheckInOutHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.custom_listing_row, parent, false);
        return new CheckInOutHolder(view);
    }

    @Override
    public int getItemCount() {
        return mCursor != null ? mCursor.getCount() : 0;
    }

    public Cursor swapCursor(Cursor cursor) {
        if (this.mCursor == cursor) {
            return null;
        }
        Cursor oldCursor = this.mCursor;
        this.mCursor = cursor;
        if (cursor != null) {
            notifyDataSetChanged();
        }
        return oldCursor;
    }
}
