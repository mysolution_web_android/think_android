package com.applab.taskmgr.MyTask.webapi;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.applab.taskmgr.MyTask.database.CRUDHelper;
import com.applab.taskmgr.MyTask.model.Count;
import com.applab.taskmgr.MyTask.provider.CountProvider;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Task.model.Task;
import com.applab.taskmgr.Task.provider.TaskProvider;
import com.applab.taskmgr.Utilities.AppController;
import com.applab.taskmgr.Utilities.DBHelper;
import com.applab.taskmgr.Utilities.GsonRequest;
import com.applab.taskmgr.Utilities.Utilities;

import java.util.HashMap;

/**
 * Created by user on 01-Mar-16.
 */
public class HttpHelper {
    public static void putCompletedTask(Context context, String TAG, int taskId) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", com.applab.taskmgr.Login.database.CRUDHelper.getToken(context).getToken());
        GsonRequest<String> mGsonRequest = new GsonRequest<String>(
                Request.Method.PUT,
                context.getString(R.string.base_url) + "Task/Complete?task_id=" + taskId,
                String.class,
                headers,
                responseNewProfileListener(context, TAG, taskId),
                errorNewProfileListener(context, TAG)) {

        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utilities.TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<String> responseNewProfileListener(final Context context, final String TAG, final int taskId) {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i(TAG, "Success Retrieved Data - " + response);
                context.getContentResolver().delete(TaskProvider.CONTENT_URI, DBHelper.TASK_COLUMN_TASK_ID + "=?", new String[]{String.valueOf(taskId)});
                Utilities.showError(context, Utilities.CODE_SUCCESS_UPDATED, Utilities.SUCCESS_UPDATED);
                ((Activity) context).finish();
            }
        };
    }

    public static Response.ErrorListener errorNewProfileListener(final Context context, final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Intent intent = new Intent(TAG);
                intent.putExtra("failed", true);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                Utilities.serverHandlingError(context, error);
            }
        };
    }

    //Check In Out
    public static void putCheckInOut(Context context, String TAG, int taskId, final int check
            , final double deviceLatitude, final double deviceLongtitude, final double networkLatitude, final double networkLongtitude) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", com.applab.taskmgr.Login.database.CRUDHelper.getToken(context).getToken());
        GsonRequest<String> mGsonRequest = new GsonRequest<String>(
                Request.Method.POST,
                context.getString(R.string.base_url) + "Task/CreateCheckInOut?task_id=" + taskId,
                String.class,
                headers,
                responseCheckProfileListener(context, TAG, taskId),
                errorCheckProfileListener(context, TAG)) {

            @Override
            public byte[] getBody() throws AuthFailureError {
                String httpbody = "Check=" + check + "&GpsLat=" + deviceLatitude + "&GpsLong=" + deviceLongtitude
                        + "&InternetGpsLat=" + networkLatitude + "&InternetGpsLong=" + networkLongtitude;
                return httpbody.getBytes();
            }

        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utilities.TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<String> responseCheckProfileListener(final Context context, final String TAG, final int taskId) {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i(TAG, "Success Retrieved Data - " + response);
                Utilities.showError(context, Utilities.CODE_SUCCESS_UPDATED, Utilities.SUCCESS_UPDATED);
                HttpHelper.getLastCheckIn(context, TAG, taskId, false);
                ((Activity) context).finish();
            }
        };
    }

    public static Response.ErrorListener errorCheckProfileListener(final Context context, final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Intent intent = new Intent(TAG);
                intent.putExtra("failed", true);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                Utilities.serverHandlingError(context, error);
            }
        };
    }

    //Cancel Task
    public static void putCancelTask(Context context, String TAG, int taskId) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", com.applab.taskmgr.Login.database.CRUDHelper.getToken(context).getToken());
        GsonRequest<String> mGsonRequest = new GsonRequest<String>(
                Request.Method.PUT,
                context.getString(R.string.base_url) + "Task/Reject?task_id=" + taskId,
                String.class,
                headers,
                responseCancelProfileListener(context, TAG, taskId),
                errorCancelProfileListener(context, TAG)) {

        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utilities.TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<String> responseCancelProfileListener(final Context context, final String TAG, final int taskId) {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i(TAG, "Success Retrieved Data - " + response);
                context.getContentResolver().delete(TaskProvider.CONTENT_URI, DBHelper.TASK_COLUMN_TASK_ID + "=?", new String[]{String.valueOf(taskId)});
                Utilities.showError(context, Utilities.CODE_SUCCESS_UPDATED, Utilities.SUCCESS_UPDATED);
                ((Activity) context).finish();
            }
        };
    }

    public static Response.ErrorListener errorCancelProfileListener(final Context context, final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Intent intent = new Intent(TAG);
                intent.putExtra("failed", true);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                Utilities.serverHandlingError(context, error);
            }
        };
    }

    //GET API COUNT
    public static void getCount(Context context, String TAG, int taskId) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", com.applab.taskmgr.Login.database.CRUDHelper.getToken(context).getToken());
        GsonRequest<Count> mGsonRequest = new GsonRequest<Count>(
                Request.Method.GET,
                context.getString(R.string.base_url) + "Count/GetMessageCheckCount?task_id=" + taskId,
                Count.class,
                headers,
                responseCountProfileListener(context, TAG, taskId),
                errorCountProfileListener(context, TAG)) {

        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utilities.TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<Count> responseCountProfileListener(final Context context, final String TAG, final int taskId) {
        return new Response.Listener<Count>() {
            @Override
            public void onResponse(Count response) {
                Log.i(TAG, "Success Retrieved Data - " + response);
                context.getContentResolver().delete(CountProvider.CONTENT_URI, DBHelper.COUNT_COLUMN_TASK_ID + "=?",
                        new String[]{String.valueOf(taskId)});
                CRUDHelper.insertCount(response, context, TAG, taskId);
            }
        };
    }

    public static Response.ErrorListener errorCountProfileListener(final Context context, final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Intent intent = new Intent(TAG);
                intent.putExtra("Error", Utilities.PROCEED_FAILED);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                Utilities.serverHandlingError(context, error);
            }
        };
    }

    public static void getLastCheckIn(Context context, String TAG, int taskId, boolean isClicked) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", com.applab.taskmgr.Login.database.CRUDHelper.getToken(context).getToken());
        GsonRequest<Task> mGsonRequest = new GsonRequest<Task>(
                Request.Method.GET,
                context.getString(R.string.base_url) + "Task/GetLastCheck?task_id=" + Utilities.encode(taskId),
                Task.class,
                headers,
                responseGetLastCheckIn(context, TAG, taskId, isClicked),
                errorGetLastCheckIn(context, TAG)) {

        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utilities.TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<Task> responseGetLastCheckIn(final Context context, final String TAG, final int taskId, final boolean isClicked) {
        return new Response.Listener<Task>() {
            @Override
            public void onResponse(Task response) {
                Log.i(TAG, "Success Retrieved Data - " + response);
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.TASK_COLUMN_LAST_CHECK_STATUS, response.getCheck());
                context.getContentResolver().update(TaskProvider.CONTENT_URI, contentValues,
                        DBHelper.TASK_COLUMN_TASK_ID + "=?", new String[]{String.valueOf(taskId)});
                if (isClicked) {
                    Intent intent = new Intent(TAG);
                    intent.putExtra("isCheck", true);
                    intent.putExtra("isCheckOut", response.getCheck() == 0);
                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                }
            }
        };
    }

    public static Response.ErrorListener errorGetLastCheckIn(final Context context, final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Intent intent = new Intent(TAG);
                intent.putExtra("failed", true);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                Utilities.serverHandlingError(context, error);
            }
        };
    }

    //Accept Task
    public static void putAcceptTask(Context context, String TAG, int taskId) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", com.applab.taskmgr.Login.database.CRUDHelper.getToken(context).getToken());
        GsonRequest<String> mGsonRequest = new GsonRequest<String>(
                Request.Method.PUT,
                context.getString(R.string.base_url) + "Task/Accept?task_id=" + taskId,
                String.class,
                headers,
                responseAcceptProfileListener(context, TAG, taskId),
                errorAcceptProfileListener(context, TAG)) {

        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utilities.TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<String> responseAcceptProfileListener(final Context context, final String TAG, final int taskId) {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i(TAG, "Success Retrieved Data - " + response);
                context.getContentResolver().delete(TaskProvider.CONTENT_URI, DBHelper.TASK_COLUMN_TASK_ID + "=?", new String[]{String.valueOf(taskId)});
                Utilities.showError(context, Utilities.CODE_SUCCESS_UPDATED, Utilities.SUCCESS_UPDATED);
                ((Activity) context).finish();
            }
        };
    }

    public static Response.ErrorListener errorAcceptProfileListener(final Context context, final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Intent intent = new Intent(TAG);
                intent.putExtra("failed", true);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                Utilities.serverHandlingError(context, error);
            }
        };
    }
}
