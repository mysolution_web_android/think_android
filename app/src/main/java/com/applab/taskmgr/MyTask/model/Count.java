package com.applab.taskmgr.MyTask.model;

/**
 * Created by user on 02-Mar-16.
 */

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.applab.taskmgr.Utilities.DBHelper;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Count implements Parcelable {

    public Count() {

    }

    public Count(Parcel in) {
        readFromParcel(in);
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.CheckCount);
        dest.writeInt(this.MessageCount);
    }

    public void readFromParcel(Parcel in) {
        this.CheckCount = in.readInt();
        this.MessageCount = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @SuppressWarnings("unchecked")
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public Count createFromParcel(Parcel in) {
            return new Count(in);
        }

        @Override
        public Count[] newArray(int size) {
            return new Count[size];
        }
    };

    @SerializedName("MessageCount")
    @Expose
    private Integer MessageCount;
    @SerializedName("CheckCount")
    @Expose
    private Integer CheckCount;

    /**
     * @return The MessageCount
     */
    public Integer getMessageCount() {
        return MessageCount;
    }

    /**
     * @param MessageCount The MessageCount
     */
    public void setMessageCount(Integer MessageCount) {
        this.MessageCount = MessageCount;
    }

    /**
     * @return The CheckCount
     */
    public Integer getCheckCount() {
        return CheckCount;
    }

    /**
     * @param CheckCount The CheckCount
     */
    public void setCheckCount(Integer CheckCount) {
        this.CheckCount = CheckCount;
    }

    public static Count getCount(Cursor cursor, int position) {
        cursor.moveToPosition(position);
        Count count = new Count();
        count.setCheckCount(cursor.getInt(cursor.getColumnIndex(DBHelper.COUNT_CHECKING_COUNT)));
        count.setMessageCount(cursor.getInt(cursor.getColumnIndex(DBHelper.COUNT_MESSAGE_COUNT)));
        return count;
    }

}
