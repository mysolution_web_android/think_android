package com.applab.taskmgr.MyTask.database;

import android.content.ContentValues;
import android.content.Context;
import android.util.Log;

import com.applab.taskmgr.MyTask.model.Count;
import com.applab.taskmgr.MyTask.provider.CountProvider;
import com.applab.taskmgr.Utilities.DBHelper;

/**
 * Created by user on 02-Mar-16.
 */
public class CRUDHelper {
    public static void insertCount(Count count, Context context, String TAG, int taskId) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(DBHelper.COUNT_CHECKING_COUNT, count.getCheckCount());
            contentValues.put(DBHelper.COUNT_COLUMN_TASK_ID, taskId);
            contentValues.put(DBHelper.COUNT_MESSAGE_COUNT, count.getMessageCount());
            context.getContentResolver().insert(CountProvider.CONTENT_URI, contentValues);
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Download Error :" + ex.toString());
        }
    }
}
