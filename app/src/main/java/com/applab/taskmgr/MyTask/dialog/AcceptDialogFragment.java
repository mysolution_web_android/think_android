package com.applab.taskmgr.MyTask.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.applab.taskmgr.R;

/**
 * Created by user on 04-Mar-16.
 */
public class AcceptDialogFragment extends DialogFragment {
    private String mUrl, mFolderName;
    private TextView mTxtTitle, mTxtMessage;
    private ImageView mBtnCancel;
    private LinearLayout mBtnYes, mBtnNo;
    private static AlertDialog.Builder builder;
    private static Context mContext;
    private String mMessage;

    private String TAG = AcceptDialogFragment.class.getSimpleName();

    public static AcceptDialogFragment newInstance(final Context context, final String message, final String TAG) {
        AcceptDialogFragment frag = new AcceptDialogFragment();
        Bundle args = new Bundle();
        args.putString("message", message);
        args.putString("TAG", TAG);
        mContext = context;
        frag.setArguments(args);
        builder = new AlertDialog.Builder(context);
        mContext = context;
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mMessage = getArguments().getString("message");
        TAG = getArguments().getString("TAG");
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.window_dialog, null);
        mTxtTitle = (TextView) v.findViewById(R.id.txtTitle);
        mTxtMessage = (TextView) v.findViewById(R.id.txtMessage);
        mTxtTitle.setText(getResources().getString(R.string.warning));
        mTxtMessage.setText(mMessage);
        mBtnCancel = (ImageView) v.findViewById(R.id.btnCancel);
        mBtnYes = (LinearLayout) v.findViewById(R.id.btnYes);
        mBtnNo = (LinearLayout) v.findViewById(R.id.btnNo);
        mBtnCancel.setOnClickListener(btnCancelOnClickListener);
        mBtnYes.setOnClickListener(btnYesOnClickListener);
        mBtnNo.setOnClickListener(btnNoOnClickListener);

        // Build the dialog
        builder.setView(v);
        Dialog d = builder.create();
        d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return d;
    }

    private View.OnClickListener btnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AcceptDialogFragment.this.getDialog().cancel();
        }
    };

    private View.OnClickListener btnYesOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(TAG);
            intent.putExtra("isConfirmAccept", true);
            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
            AcceptDialogFragment.this.getDialog().cancel();
        }
    };

    private View.OnClickListener btnNoOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AcceptDialogFragment.this.getDialog().cancel();
        }
    };


}
