package com.applab.taskmgr.MyTask.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.applab.taskmgr.AllMessage.activity.AllMessageActivitiy;
import com.applab.taskmgr.CheckInOut.activity.CheckInOutActivity;
import com.applab.taskmgr.CreateNewTask.activity.CreateNewTaskActivity;
import com.applab.taskmgr.MyTask.dialog.AcceptDialogFragment;
import com.applab.taskmgr.MyTask.dialog.DownloadDialogFragment;
import com.applab.taskmgr.MyTask.dialog.RejectDialogFragment;
import com.applab.taskmgr.MyTask.gpsnetworkloaction.AppLocationService;
import com.applab.taskmgr.MyTask.model.Count;
import com.applab.taskmgr.MyTask.provider.CountProvider;
import com.applab.taskmgr.MyTask.webapi.HttpHelper;
import com.applab.taskmgr.PostMessage.activity.activity.PostMessageActivity;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Task.model.AsigneeList;
import com.applab.taskmgr.Task.model.Task;
import com.applab.taskmgr.Task.provider.AssigneeProvider;
import com.applab.taskmgr.Task.provider.TaskProvider;
import com.applab.taskmgr.Utilities.DBHelper;
import com.applab.taskmgr.Utilities.Utilities;

import java.util.ArrayList;

/**
 * -- =============================================
 * -- Author     : Lau Pong
 * -- Create date: 4/2/2016
 * -- Description: AssigneeList .java
 * -- =============================================
 * HISTORY OF UPDATE
 * <p/>
 * NO     DEVELOPER         DATETIME                      DESCRIPTION
 * *******************************************************************************
 * 1
 * 2
 */
public class MyTaskActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    private Toolbar mToolbar;
    private TextView mTxtToolbarTitle;
    private int page = 0;

    private Task mTask = new Task();
    private AsigneeList mAsigneeList = new AsigneeList();
    private Count mCount = new Count();
    private int mLoaderId = 1234;
    private String TAG = MyTaskActivity.class.getSimpleName();
    private int mLoaderAssigneeId = 2142;
    private int mLoaderCountId = 2231;
    String mPriority = null;

    private TextView mTxtTaskTitle;
    private TextView mTxtLocation;
    private TextView mTxtDateTime;
    private TextView mTxtAssignTo;
    private TextView mTxtAssignBy;
    private TextView mTxtPriority;
    private TextView mTxtAttachement;
    private TextView mTxtRemarks;
    private TextView mTxtCheckInOut;
    private TextView mTxtMessageCount;
    private TextView mTxtCheckCount;

    private LinearLayout mBtnDone;
    private LinearLayout mBtnCheckIn;
    private LinearLayout mBtnCancel;
    private LinearLayout mBtnShowAll;
    private LinearLayout mBtnPostMessage;
    private LinearLayout mBtnShowAllMessage;
    private LinearLayout mBtnNewTask;
    private LinearLayout mBtnCreatedTask;
    private LinearLayout mBtnMyTask;
    private LinearLayout mBtnAccept;
    private LinearLayout mBtnDecline;
    private LinearLayout mBtnEdit;
    private LinearLayout mBtnOpenFile;
    private LinearLayout mLvCheck, mLvMessage;

    private AppLocationService appLocationService;

    private RelativeLayout mRl;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_task);

        appLocationService = new AppLocationService(MyTaskActivity.this);

        mToolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(mToolbar);
        mTxtToolbarTitle = (TextView) mToolbar.findViewById(R.id.txtTitle);
        mTxtToolbarTitle.setText(getString(R.string.title_activity_my_task));

        mToolbar.setNavigationIcon(R.mipmap.back);
        mToolbar.setNavigationOnClickListener(mToolbarOnClickListener);

        mTxtTaskTitle = (TextView) findViewById(R.id.txtTaskTitle);
        mTxtLocation = (TextView) findViewById(R.id.txtLocation);
        mTxtDateTime = (TextView) findViewById(R.id.txtDateTime);
        mTxtAssignTo = (TextView) findViewById(R.id.txtAssignTo);
        mTxtAttachement = (TextView) findViewById(R.id.attachement);
        mTxtPriority = (TextView) findViewById(R.id.txtPriority);
        mTxtAssignBy = (TextView) findViewById(R.id.txtAssignedBy);
        mTxtRemarks = (TextView) findViewById(R.id.message);
        mTxtCheckInOut = (TextView) findViewById(R.id.txtCheckInOut);
        mTxtCheckCount = (TextView) findViewById(R.id.txtCheckCount);
        mTxtMessageCount = (TextView) findViewById(R.id.txtMessageCount);

        mBtnDone = (LinearLayout) findViewById(R.id.btnDone);
        mBtnCheckIn = (LinearLayout) findViewById(R.id.btnCheckIn);
        mBtnCancel = (LinearLayout) findViewById(R.id.btnCancel);
        mBtnPostMessage = (LinearLayout) findViewById(R.id.btn_PostMessage);
        mBtnShowAll = (LinearLayout) findViewById(R.id.btn_showAll);
        mBtnShowAllMessage = (LinearLayout) findViewById(R.id.btn_showMessage);
        mBtnAccept = (LinearLayout) findViewById(R.id.btnAccept);
        mBtnDecline = (LinearLayout) findViewById(R.id.btnDecline);
        mBtnEdit = (LinearLayout) findViewById(R.id.btnEdit);
        mBtnNewTask = (LinearLayout) findViewById(R.id.btnNewTask);
        mBtnCreatedTask = (LinearLayout) findViewById(R.id.btnCreatedTask);
        mBtnMyTask = (LinearLayout) findViewById(R.id.btnMyTask);
        mBtnOpenFile = (LinearLayout) findViewById(R.id.btnOpenFile);
        mLvCheck = (LinearLayout) findViewById(R.id.lvCheck);
        mLvMessage = (LinearLayout) findViewById(R.id.lvMessage);

        mRl = (RelativeLayout) findViewById(R.id.fadeRL);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);

        mBtnDone.setOnClickListener(btnDoneClickListener);
        mBtnCheckIn.setOnClickListener(btnCheckInOutOnClickListener);
        mBtnCancel.setOnClickListener(btnCancelOnClickListener);
        mBtnPostMessage.setOnClickListener(postMessageClickListener);
        mBtnShowAll.setOnClickListener(showAllClickListener);
        mBtnShowAllMessage.setOnClickListener(showAllMessageClickListener);

        mBtnAccept.setOnClickListener(mBtnAcceptOnClickListener);
        mBtnDecline.setOnClickListener(mBtnDeclineOnClickListener);
        mBtnEdit.setOnClickListener(mBtnEditOnClickListener);
        mBtnOpenFile.setOnClickListener(mBtnOpenFileOnClickListener);

        setIntent(getIntent());
    }

    public void setIntent(Intent intent) {
        if (intent.getParcelableExtra("Task") != null) {
            mTask = intent.getParcelableExtra("Task");
            String type = mTask.getType();
            if (type.equals(getString(R.string.cancelled_task))) {
                mBtnCreatedTask.setVisibility(View.GONE);
                mBtnMyTask.setVisibility(View.GONE);
                mBtnNewTask.setVisibility(View.GONE);
                mTxtToolbarTitle.setText(R.string.title_activity_canceled_task);
                mLvMessage.setVisibility(View.GONE);
                mLvCheck.setVisibility(View.GONE);
            } else if (type.equals(getString(R.string.new_tasks))) {
                mBtnCreatedTask.setVisibility(View.GONE);
                mBtnMyTask.setVisibility(View.GONE);
                mBtnNewTask.setVisibility(View.VISIBLE);
                mTxtToolbarTitle.setText(R.string.title_new_task);
                mLvMessage.setVisibility(View.GONE);
                mLvCheck.setVisibility(View.GONE);
            } else if (type.equals(getString(R.string.created_task))) {
                mBtnCreatedTask.setVisibility(View.VISIBLE);
                mBtnMyTask.setVisibility(View.GONE);
                mBtnNewTask.setVisibility(View.GONE);
                mTxtToolbarTitle.setText(R.string.title_activity_created_task);
                mLvMessage.setVisibility(View.GONE);
                mLvCheck.setVisibility(View.GONE);
            } else if (type.equals(getString(R.string.my_pending_task))) {
                mBtnCreatedTask.setVisibility(View.GONE);
                mBtnMyTask.setVisibility(View.VISIBLE);
                mBtnNewTask.setVisibility(View.GONE);
                HttpHelper.getLastCheckIn(MyTaskActivity.this, TAG, mTask.getTaskId(), false);
                mTxtToolbarTitle.setText(R.string.title_activity_my_task);
            } else if (type.equals(getString(R.string.completed_task))) {
                mBtnCreatedTask.setVisibility(View.GONE);
                mBtnMyTask.setVisibility(View.GONE);
                mBtnNewTask.setVisibility(View.GONE);
                mTxtToolbarTitle.setText(R.string.title_activity_completed_task);
            }
        }
    }

    private View.OnClickListener mBtnOpenFileOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mTask.getFile() != null) {
                if (Utilities.localFileExists(mTask.getStoredLocation())) {
                    Utilities.openDocument(mTask.getStoredLocation(), MyTaskActivity.this);
                } else {
                    String folderName = MyTaskActivity.this.getString(R.string.task_attachment) + "/" + mTask.getTaskId().toString();
                    DownloadDialogFragment downloadDialogFragment = DownloadDialogFragment.newInstance(MyTaskActivity.this, mTask.getFile(), folderName);
                    downloadDialogFragment.show(getSupportFragmentManager(), "");
                }
            }
        }
    };

    private View.OnClickListener mBtnAcceptOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AcceptDialogFragment acceptDialogFragment = AcceptDialogFragment.newInstance(MyTaskActivity.this, getString(R.string.accept_mesage), TAG);
            acceptDialogFragment.show(getSupportFragmentManager(), "");
        }
    };

    private View.OnClickListener mBtnDeclineOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            RejectDialogFragment rejectDialogFragment = RejectDialogFragment.newInstance(MyTaskActivity.this, getString(R.string.reject_mesage), TAG);
            rejectDialogFragment.show(getSupportFragmentManager(), "");
        }
    };

    private View.OnClickListener mBtnEditOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MyTaskActivity.this, CreateNewTaskActivity.class);
            intent.putExtra("Task", mTask);
            startActivity(intent);
        }
    };

    private View.OnClickListener showAllClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MyTaskActivity.this, CheckInOutActivity.class);
            intent.putExtra("Task", mTask);
            startActivity(intent);
        }
    };

    private View.OnClickListener showAllMessageClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MyTaskActivity.this, AllMessageActivitiy.class);
            intent.putExtra("Task", mTask);
            startActivity(intent);
        }
    };

    private View.OnClickListener postMessageClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MyTaskActivity.this, PostMessageActivity.class);
            intent.putExtra("Task", mTask);
            startActivity(intent);
        }
    };

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(TAG)) {
                if (intent.getBooleanExtra("isCheck", false)) {
                    int check = intent.getBooleanExtra("isCheckOut", false) ? 0 : 1;
                    checkInOut(check);
                } else if (intent.getBooleanExtra("failed", false)) {
                    Utilities.setFadeProgressBarVisibility(false, mRl, mProgressBar);
                } else if (intent.getBooleanExtra("isConfirmReject", false)) {
                    Utilities.setFadeProgressBarVisibility(true, mRl, mProgressBar);
                    HttpHelper.putCancelTask(MyTaskActivity.this, TAG, mTask.getTaskId());
                } else if (intent.getBooleanExtra("isConfirmAccept", false)) {
                    Utilities.setFadeProgressBarVisibility(true, mRl, mProgressBar);
                    HttpHelper.putAcceptTask(MyTaskActivity.this, TAG, mTask.getTaskId());
                }
            }
        }
    };

    private void checkInOut(int check) {
        Location gpsLocation = appLocationService.getLocation(LocationManager.GPS_PROVIDER);
        Location networkLocation = appLocationService.getLocation(LocationManager.NETWORK_PROVIDER);
        if (gpsLocation != null && networkLocation != null) {
            double latitudeGPS = gpsLocation.getLatitude();
            double longitudeGPS = gpsLocation.getLongitude();
            double latitudeNetwork = gpsLocation.getLatitude();
            double longitudeNetwork = gpsLocation.getLongitude();
            HttpHelper.putCheckInOut(MyTaskActivity.this, TAG, mTask.getTaskId(), check,
                    latitudeGPS, longitudeGPS, latitudeNetwork, longitudeNetwork);
        } else {
            Toast.makeText(MyTaskActivity.this, getString(R.string.no_gps_wifi), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter iff = new IntentFilter(TAG);
        LocalBroadcastManager.getInstance(getBaseContext()).registerReceiver(broadcastReceiver, iff);
        getSupportLoaderManager().initLoader(mLoaderId, null, this);
        getSupportLoaderManager().initLoader(mLoaderAssigneeId, null, this);
        getSupportLoaderManager().initLoader(mLoaderCountId, null, this);
        HttpHelper.getCount(MyTaskActivity.this, TAG, mTask.getTaskId());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_completed_task, menu);
        return true;
    }

    private View.OnClickListener mToolbarOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getBaseContext()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (mLoaderId == id) {
            return new CursorLoader(MyTaskActivity.this, TaskProvider.CONTENT_URI, null, DBHelper.TASK_COLUMN_TASK_ID + "=?"
                    , new String[]{mTask.getTaskId().toString()}, null);
        } else if (mLoaderAssigneeId == id) {
            return new CursorLoader(MyTaskActivity.this, AssigneeProvider.CONTENT_URI, null, DBHelper.ASSIGNEELIST_COLUMN_TASK_ID + "=?"
                    , new String[]{mTask.getTaskId().toString()}, null);
        } else if (mLoaderCountId == id) {
            return new CursorLoader(MyTaskActivity.this, CountProvider.CONTENT_URI, null, DBHelper.COUNT_COLUMN_TASK_ID + "=?", new String[]{mTask.getTaskId().toString()}, null);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (mLoaderId == loader.getId()) {
            if (data != null) {
                if (data.getCount() > 0) {
                    mTask = Task.getTask(data, 0);
                    String mDate = Utilities.setToLocalDateTime(Utilities.DATE_FORMAT, "EEEE, dd MMMM yyyy", mTask.getTaskDate()) +
                            " - " + Utilities.setToLocalDateTime(Utilities.DATE_FORMAT, "EEEE, dd MMM yyyy", mTask.getTaskDueDate());

                    String mAssignedBy = getString(R.string.assigned_by) + " " + mTask.getAssignerName() + " " + getString(R.string.on) + " "
                            + Utilities.setToLocalDateTime(Utilities.DATE_FORMAT, "EEEE dd MMM yyyy", mTask.getCreateDate());

                    if (mTask.getPriority() != 1) {
                        mPriority = getString(R.string.low);
                    } else {
                        mPriority = getString(R.string.high);
                    }

                    String mRemarks = null;

                    if (mTask.getTaskDescription() != null) {
                        if (mTask.getTaskDescription().length() > 0) {
                            mRemarks = mTask.getTaskDescription();
                        } else {
                            mRemarks = getString(R.string.no_remarks);
                        }
                    } else {
                        mRemarks = getString(R.string.no_remarks);
                    }

                    mTxtTaskTitle.setText(mTask.getTaskTitle());
                    mTxtLocation.setText(mTask.getLocation());
                    mTxtDateTime.setText(mDate);
                    mTxtAssignBy.setText(mAssignedBy);
                    mTxtPriority.setText(mPriority);
                    mTxtRemarks.setText(mRemarks);
                    mTxtCheckInOut.setText(mTask.getCheck() == 0 ? getString(R.string.check_in) : getString(R.string.check_out));

                    if (mTask.getFile() != null) {
                        String url = mTask.getFile().split("/")[mTask.getFile().split("/").length - 1];
                        mTxtAttachement.setText(url);
                    } else {
                        mTxtAttachement.setText(getString(R.string.no_file));
                    }
                }
            }
        } else if (mLoaderAssigneeId == loader.getId()) {
            if (data != null) {
                if (data.getCount() > 0) {
                    ArrayList<AsigneeList> asigneeLists = new ArrayList<>();
                    String name = "";
                    for (int i = 0; i < data.getCount(); i++) {
                        mAsigneeList = AsigneeList.getAssigneeList(data, i);
                        asigneeLists.add(mAsigneeList);
                        if (i == data.getCount() - 1) {
                            name += mAsigneeList.getUserName();
                        } else {
                            name += mAsigneeList.getUserName() + " , ";
                        }

                    }
                    mTxtAssignTo.setText(name);
                    mTask.setAssigneeList(asigneeLists);
                }
            }
        } else if (mLoaderCountId == loader.getId()) {
            if (data != null) {
                if (data.getCount() > 0) {
                    data.moveToPosition(0);
                    mCount.setCheckCount(data.getInt(data.getColumnIndex(DBHelper.COUNT_CHECKING_COUNT)));
                    mCount.setMessageCount(data.getInt(data.getColumnIndex(DBHelper.COUNT_MESSAGE_COUNT)));
                    mTxtCheckCount.setText(getString(R.string.check_in_out) + " ( " + mCount.getCheckCount() + " )");
                    mTxtMessageCount.setText(getString(R.string.message) + " ( " + mCount.getMessageCount() + " )");
                } else {
                    mTxtCheckCount.setText(getString(R.string.check_in_out) + " ( " + 0 + " )");
                    mTxtMessageCount.setText(getString(R.string.message) + " ( " + 0 + " )");
                }
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    private View.OnClickListener btnDoneClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AcceptDialogFragment acceptDialogFragment = AcceptDialogFragment.newInstance(MyTaskActivity.this, getString(R.string.accept_mesage), TAG);
            acceptDialogFragment.show(getSupportFragmentManager(), "");
        }
    };

    private View.OnClickListener btnCheckInOutOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Utilities.setFadeProgressBarVisibility(true, mRl, mProgressBar);
            HttpHelper.getLastCheckIn(MyTaskActivity.this, TAG, mTask.getTaskId(), true);
        }
    };

    private View.OnClickListener btnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            RejectDialogFragment rejectDialogFragment = RejectDialogFragment.newInstance(MyTaskActivity.this, getString(R.string.reject_mesage), TAG);
            rejectDialogFragment.show(getSupportFragmentManager(), "");
        }
    };
}
