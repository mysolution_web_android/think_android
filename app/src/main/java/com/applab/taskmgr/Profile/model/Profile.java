package com.applab.taskmgr.Profile.model;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.applab.taskmgr.Utilities.DBHelper;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * -- =============================================
 * -- Author     : Vicknesh Balasubramaniam
 * -- Create date: 19/1/2016
 * -- Description: AssigneeList .java
 * -- =============================================
 * HISTORY OF UPDATE
 * <p/>
 * NO     DEVELOPER         DATETIME                      DESCRIPTION
 * *******************************************************************************
 * 1
 * 2
 */


public class Profile implements Parcelable {

    @SerializedName("UserId")
    @Expose
    private String UserId;
    @SerializedName("CompanyId")
    @Expose
    private Integer CompanyId;
    @SerializedName("CompanyName")
    @Expose
    private String CompanyName;
    @SerializedName("Name")
    @Expose
    private String Name;
    @SerializedName("ContactNo")
    @Expose
    private String ContactNo;
    @SerializedName("Designation")
    @Expose
    private String Designation;
    @SerializedName("Email")
    @Expose
    private String Email;
    @SerializedName("CompanyAddress")
    @Expose
    private String CompanyAddress;
    @SerializedName("DepartmentId")
    @Expose
    private String DepartmentId;
    @SerializedName("DepartmentName")
    @Expose
    private String DepartmentName;
    @SerializedName("ProfileImage")
    @Expose
    private String ProfileImage;
    @SerializedName("UserStartDate")
    @Expose
    private String UserStartDate;
    @SerializedName("UserEndDate")
    @Expose
    private String UserEndDate;
    @SerializedName("PasswordLastChangeDate")
    @Expose
    private String PasswordLastChangeDate;
    @SerializedName("MobileNo")
    @Expose
    private String MobileNo;
    @SerializedName("LastUpdateDate")
    @Expose
    private String LastUpdateDate;
    private int id;

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String mobileNo) {
        MobileNo = mobileNo;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getCompanyAddress() {
        return CompanyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        CompanyAddress = companyAddress;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return The UserId
     */
    public String getUserId() {
        return UserId;
    }

    /**
     * @param UserId The UserId
     */
    public void setUserId(String UserId) {
        this.UserId = UserId;
    }

    /**
     * @return The CompanyId
     */
    public Integer getCompanyId() {
        return CompanyId;
    }

    /**
     * @param CompanyId The CompanyId
     */
    public void setCompanyId(Integer CompanyId) {
        this.CompanyId = CompanyId;
    }

    /**
     * @return The CompanyName
     */
    public String getCompanyName() {
        return CompanyName;
    }

    /**
     * @param CompanyName The CompanyName
     */
    public void setCompanyName(String CompanyName) {
        this.CompanyName = CompanyName;
    }

    /**
     * @return The Name
     */
    public String getName() {
        return Name;
    }

    /**
     * @param Name The Name
     */
    public void setName(String Name) {
        this.Name = Name;
    }

    /**
     * @return The ContactNo
     */
    public String getContactNo() {
        return ContactNo;
    }

    /**
     * @param ContactNo The ContactNo
     */
    public void setContactNo(String ContactNo) {
        this.ContactNo = ContactNo;
    }

    /**
     * @return The Designation
     */
    public String getDesignation() {
        return Designation;
    }

    /**
     * @param Designation The Designation
     */
    public void setDesignation(String Designation) {
        this.Designation = Designation;
    }

    /**
     * @return The DepartmentId
     */
    public String getDepartmentId() {
        return DepartmentId;
    }

    /**
     * @param DepartmentId The DepartmentId
     */
    public void setDepartmentId(String DepartmentId) {
        this.DepartmentId = DepartmentId;
    }

    /**
     * @return The DepartmentName
     */
    public String getDepartmentName() {
        return DepartmentName;
    }

    /**
     * @param DepartmentName The DepartmentName
     */
    public void setDepartmentName(String DepartmentName) {
        this.DepartmentName = DepartmentName;
    }

    /**
     * @return The ProfileImage
     */
    public String getProfileImage() {
        return ProfileImage;
    }

    /**
     * @param ProfileImage The ProfileImage
     */
    public void setProfileImage(String ProfileImage) {
        this.ProfileImage = ProfileImage;
    }

    /**
     * @return The UserStartDate
     */
    public String getUserStartDate() {
        return UserStartDate;
    }

    /**
     * @param UserStartDate The UserStartDate
     */
    public void setUserStartDate(String UserStartDate) {
        this.UserStartDate = UserStartDate;
    }

    /**
     * @return The UserEndDate
     */
    public String getUserEndDate() {
        return UserEndDate;
    }

    /**
     * @param UserEndDate The UserEndDate
     */
    public void setUserEndDate(String UserEndDate) {
        this.UserEndDate = UserEndDate;
    }

    /**
     * @return The PasswordLastChangeDate
     */
    public String getPasswordLastChangeDate() {
        return PasswordLastChangeDate;
    }

    /**
     * @param PasswordLastChangeDate The PasswordLastChangeDate
     */
    public void setPasswordLastChangeDate(String PasswordLastChangeDate) {
        this.PasswordLastChangeDate = PasswordLastChangeDate;
    }

    /**
     * @return The LastUpdateDate
     */
    public String getLastUpdateDate() {
        return LastUpdateDate;
    }

    /**
     * @param LastUpdateDate The LastUpdateDate
     */
    public void setLastUpdateDate(String LastUpdateDate) {
        this.LastUpdateDate = LastUpdateDate;
    }

    public static Parcelable.Creator getCREATOR() {
        return CREATOR;
    }

    public Profile() {
    }

    public Profile(Parcel in) {
        readFromParcel(in);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.UserId);
        dest.writeInt(this.id);
        dest.writeInt(this.CompanyId);
        dest.writeString(this.CompanyName);
        dest.writeString(this.Name);
        dest.writeString(this.ContactNo);
        dest.writeString(this.ContactNo);
        dest.writeString(this.Designation);
        dest.writeString(this.DepartmentId);
        dest.writeString(this.DepartmentName);
        dest.writeString(this.ProfileImage);
        dest.writeString(this.UserStartDate);
        dest.writeString(this.UserEndDate);
        dest.writeString(this.PasswordLastChangeDate);
        dest.writeString(this.LastUpdateDate);
        dest.writeString(this.CompanyAddress);
        dest.writeString(this.Email);
        dest.writeString(this.MobileNo);
    }

    public void readFromParcel(Parcel in) {
        this.UserId = in.readString();
        this.id = in.readInt();
        this.CompanyId = in.readInt();
        this.CompanyName = in.readString();
        this.Name = in.readString();
        this.ContactNo = in.readString();
        this.Designation = in.readString();
        this.DepartmentId = in.readString();
        this.DepartmentName = in.readString();
        this.ProfileImage = in.readString();
        this.UserStartDate = in.readString();
        this.UserEndDate = in.readString();
        this.PasswordLastChangeDate = in.readString();
        this.LastUpdateDate = in.readString();
        this.CompanyAddress = in.readString();
        this.Email = in.readString();
        this.MobileNo = in.readString();
    }

    @SuppressWarnings("unchecked")
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public Profile createFromParcel(Parcel in) {
            return new Profile(in);
        }

        @Override
        public Profile[] newArray(int size) {
            return new Profile[size];
        }
    };

    public static Profile getProfile(Cursor cursor, int position) {
        cursor.moveToPosition(position);
        Profile profile = new Profile();
        profile.setName(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_NAME)));
        profile.setCompanyId(cursor.getInt(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_COMPANY_ID)));
        profile.setCompanyName(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_COMPANY_NAME)));
        profile.setContactNo(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_CONTACT_NO)));
        profile.setDepartmentId(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_DEPARTMENT_ID)));
        profile.setDepartmentName(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_DEPARTMENT_NAME)));
        profile.setDesignation(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_DESIGNATION)));
        profile.setLastUpdateDate(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_LAST_UPDATED_DATE)));
        profile.setPasswordLastChangeDate(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_PASSWORD_LAST_CHANGED_DATE)));
        profile.setProfileImage(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_PROFILE_IMAGE)));
        profile.setUserEndDate(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_USER_END_DATE)));
        profile.setUserStartDate(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_USER_START_DATE)));
        profile.setUserId(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_USER_ID)));
        profile.setId(cursor.getInt(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_ID)));
        profile.setCompanyAddress(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_COMPANY_ADDRESS)));
        profile.setEmail(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_EMAIL)));
        profile.setContactNo(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_CONTACT_NO)));
        profile.setMobileNo(cursor.getString(cursor.getColumnIndex(DBHelper.PROFILE_COLUMN_MOBILE)));
        return profile;
    }

}