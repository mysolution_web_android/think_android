package com.applab.taskmgr.Profile.service;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.applab.taskmgr.Profile.webapi.HttpHelper;
import com.applab.taskmgr.Profile.webapi.MultiPartRequest;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Utilities.Utilities;
import java.io.File;

public class ProfileUploadService extends IntentService implements MultiPartRequest.MultipartProgressListener {
    private String mMobileNo;
    private int mId = 1511;
    private String TAG = ProfileUploadService.class.getSimpleName();
    private File mFile;

    public ProfileUploadService() {
        super(ProfileUploadService.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        mMobileNo = intent.getStringExtra("mobileNo");
        mFile = (File) intent.getSerializableExtra("file");
        if (mFile != null) {
            if (Utilities.getUsableSpace() < mFile.length()) {
                Utilities.showError(getBaseContext(), Utilities.CODE_LOCAL_SIZE_EXCEEDED, Utilities.LOCAL_SIZE_EXCEED);
            } else {
                if (Utilities.isValidImage(mFile, getBaseContext())) {
                    HttpHelper.putProfileImage(mFile, mMobileNo, ProfileUploadService.this, TAG, ProfileUploadService.this);
                }
            }
        } else {
            HttpHelper.putProfileImage(null, mMobileNo, ProfileUploadService.this, TAG, ProfileUploadService.this);
        }
    }

    @Override
    public void transferred(long transferred, long progress) {
        final NotificationManager mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setContentTitle(mFile.getName())
                .setContentText(Utilities.UPLOAD_PROGRESS)
                .setSmallIcon(R.mipmap.ic_launcher);

        // Start a lengthy operation in a background thread
        mBuilder.setProgress(100, (int) progress, false);
        // Displays the progress bar for the first time.
        mNotifyManager.notify(mId, mBuilder.build());
        // When the loop is finished, updates the notification
        if (100 == progress) {
            mBuilder.setContentText(Utilities.UPLOAD_COMPLETE)
                    // Removes the progress bar
                    .setProgress(0, 0, false);
            mNotifyManager.notify(mId, mBuilder.build());
        }
    }
}
