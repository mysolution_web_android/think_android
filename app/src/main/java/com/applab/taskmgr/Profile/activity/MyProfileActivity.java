package com.applab.taskmgr.Profile.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applab.taskmgr.Menu.fragment.NavigationDrawerFragment;
import com.applab.taskmgr.Profile.dialog.ProfileDialogFragment;
import com.applab.taskmgr.Profile.model.Profile;
import com.applab.taskmgr.Profile.provider.ProfileProvider;
import com.applab.taskmgr.Profile.webapi.HttpHelper;
import com.applab.taskmgr.Profile.service.ProfileUploadService;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Utilities.CircleTransform;
import com.applab.taskmgr.Utilities.Utilities;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.File;

/**
 * -- =============================================
 * -- Author     : Vicknesh Balasubramaniam
 * -- Create date: 19/1/2016
 * -- Description: AssigneeList .java
 * -- =============================================
 * HISTORY OF UPDATE
 * <p/>
 * NO     DEVELOPER         DATETIME                      DESCRIPTION
 * *******************************************************************************
 * 1
 * 2
 */

public class MyProfileActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private ImageView mImgProfile, imgEdit;

    private Toolbar mToolbar;
    private TextView mTxtToolbarTitle;

    private NavigationDrawerFragment mDrawerFragment;
    public static String TAG = MyProfileActivity.class.getSimpleName();
    private int mLoaderId = 1598;

    private TextView mTxtName, mTxtAddress, mTxtCompanyName, mTxtDesignation,
            mTxtDepartment, mTxtEmail, mTxtError;
    private EditText mEdiMobile, mEdiTel;
    private Profile mProfile;
    private LinearLayout mLvProfile, mBtnSave, mBtnCancel, mBtn;
    private Uri mFileUri;
    private File mFile;
    private ProgressBar mProgressBar;
    private RelativeLayout mRl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);

        mToolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(mToolbar);
        mToolbar.setFitsSystemWindows(false);
        mToolbar.setPadding(0, 0, 0, 0);
        mTxtToolbarTitle = (TextView) mToolbar.findViewById(R.id.txtTitle);
        mTxtToolbarTitle.setText(getString(R.string.title_activity_my_profile));

        mDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        mDrawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        mDrawerFragment.setSelectedPosition(5);
        mDrawerFragment.mDrawerToggle.setDrawerIndicatorEnabled(false);
        mDrawerFragment.setPaddingRelativeLayout();

        mToolbar.setNavigationIcon(R.mipmap.menu);
        mToolbar.setNavigationOnClickListener(mToolbarOnClickListener);

        mImgProfile = (ImageView) findViewById(R.id.imageProfile);
        mImgProfile.setOnClickListener(mImgProfileOnClickListener);
        mTxtName = (TextView) findViewById(R.id.txtName);
        mEdiTel = (EditText) findViewById(R.id.ediTel);
        mTxtAddress = (TextView) findViewById(R.id.txtAddress);
        mTxtCompanyName = (TextView) findViewById(R.id.txtCompanyName);
        mTxtDesignation = (TextView) findViewById(R.id.txtDesignation);
        mTxtDepartment = (TextView) findViewById(R.id.txtDepartment);
        mTxtEmail = (TextView) findViewById(R.id.txtEmail);
        mTxtError = (TextView) findViewById(R.id.txtError);
        mEdiMobile = (EditText) findViewById(R.id.ediMobile);
        mLvProfile = (LinearLayout) findViewById(R.id.lvProfile);
        mBtnCancel = (LinearLayout) findViewById(R.id.btnCancel);
        mBtn = (LinearLayout) findViewById(R.id.btn);
        mBtnSave = (LinearLayout) findViewById(R.id.btnSave);
        imgEdit = (ImageView) findViewById(R.id.imgEdit);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mRl = (RelativeLayout) findViewById(R.id.fadeRL);

        mBtnSave.setOnClickListener(mBtnSaveOnClickListener);
        mBtnCancel.setOnClickListener(mBtnCancelOnClickListener);
        imgEdit.setOnClickListener(imgEditOnClickListener);
        mEdiMobile.setOnClickListener(mEditMobileOnClickListener);
        mEdiMobile.setOnEditorActionListener(mEditTextOnEditorActionListener);
    }


    private TextView.OnEditorActionListener mEditTextOnEditorActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                InputMethodManager imm = (InputMethodManager) MyProfileActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                return true;
            }
            return false;
        }
    };

    private View.OnClickListener mEditMobileOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mBtn.getVisibility() == View.VISIBLE) {
                ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE))
                        .showSoftInput(mEdiMobile, InputMethodManager.SHOW_FORCED);
            }
        }
    };


    private View.OnClickListener mImgProfileOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mBtn.getVisibility() == View.VISIBLE) {
                ProfileDialogFragment downloadDialogFragment = ProfileDialogFragment.newInstance(mProfile, MyProfileActivity.this);
                downloadDialogFragment.show(getSupportFragmentManager(), "");
            }
        }
    };

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(TAG)) {
                if (intent.getBooleanExtra("isCamera", false)) {
                    if (Utilities.isDeviceSupportCamera(MyProfileActivity.this)) {
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        mFileUri = Utilities.getOutputMediaFileUri(MyProfileActivity.this, true, null);
                        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, mFileUri);
                        startActivityForResult(cameraIntent, Utilities.CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
                    } else {
                        Utilities.showError(MyProfileActivity.this, Utilities.CODE_CAMERA_SUPPORT, Utilities.CAMERA_SUPPORT);
                    }
                } else if (intent.getBooleanExtra("isGallery", false)) {
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    startActivityForResult(photoPickerIntent, Utilities.SELECT_PHOTO_REQUEST_CODE);
                } else if (intent.getBooleanExtra("success", false)) {
                    HttpHelper.profileApi(MyProfileActivity.this, TAG, mTxtError);
                    Utilities.setFadeProgressBarVisibility(false, mRl, mProgressBar);
                } else if (intent.getBooleanExtra("failed", false)) {
                    getSupportLoaderManager().restartLoader(mLoaderId, null, MyProfileActivity.this);
                    Utilities.setFadeProgressBarVisibility(false, mRl, mProgressBar);
                }
            }
        }
    };

    private View.OnClickListener mBtnSaveOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MyProfileActivity.this, ProfileUploadService.class);
            String mobile = mEdiMobile.getText().toString();
            intent.putExtra("file", mFile);
            intent.putExtra("mobileNo", mobile);
            startService(intent);
            Utilities.setFadeProgressBarVisibility(true, mRl, mProgressBar);
            setVisibility(false);
            Utilities.hideKeyboard(MyProfileActivity.this);
        }
    };

    private View.OnClickListener mBtnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            setVisibility(false);
            Utilities.hideKeyboard(MyProfileActivity.this);
        }
    };

    private View.OnClickListener imgEditOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ProfileDialogFragment downloadDialogFragment = ProfileDialogFragment.newInstance(mProfile, MyProfileActivity.this);
            downloadDialogFragment.show(getSupportFragmentManager(), "");
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case Utilities.SELECT_PHOTO_REQUEST_CODE:
                mFile = Utilities.previewSelectedImage(this, data.getData(), mImgProfile, false, 0, true, null, Utilities.SIZE_LIMIT, true);
                break;
            case Utilities.CAMERA_CAPTURE_IMAGE_REQUEST_CODE:
                mFile = Utilities.previewCapturedImage(this, mFileUri, mImgProfile, false, 0, true, null, Utilities.SIZE_LIMIT, true);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Utilities.setIsActivityOpen(true, MyProfileActivity.this);
        getSupportLoaderManager().initLoader(mLoaderId, null, this);
        HttpHelper.profileApi(this, TAG, mTxtError);
        IntentFilter iff = new IntentFilter(TAG);
        LocalBroadcastManager.getInstance(getBaseContext()).registerReceiver(broadcastReceiver, iff);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Utilities.hideKeyboard(MyProfileActivity.this);
        LocalBroadcastManager.getInstance(getBaseContext()).unregisterReceiver(broadcastReceiver);
        Utilities.setIsActivityOpen(false, MyProfileActivity.this);
    }

    @Override
    public void onBackPressed() {
        mDrawerFragment.setCloseDrawer(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return true;
    }

    private void setVisibility(boolean isVisible) {
        if (isVisible) {
            imgEdit.setVisibility(View.VISIBLE);
            mBtn.setVisibility(View.VISIBLE);
            mEdiMobile.setEnabled(true);
            mTxtName.setTextColor(ContextCompat.getColor(MyProfileActivity.this, R.color.color_highlight));
            mTxtAddress.setTextColor(ContextCompat.getColor(MyProfileActivity.this, R.color.color_highlight));
            mTxtCompanyName.setTextColor(ContextCompat.getColor(MyProfileActivity.this, R.color.color_highlight));
            mTxtDesignation.setTextColor(ContextCompat.getColor(MyProfileActivity.this, R.color.color_highlight));
            mTxtDepartment.setTextColor(ContextCompat.getColor(MyProfileActivity.this, R.color.color_highlight));
            mTxtEmail.setTextColor(ContextCompat.getColor(MyProfileActivity.this, R.color.color_highlight));
            mEdiTel.setTextColor(ContextCompat.getColor(MyProfileActivity.this, R.color.color_highlight));
        } else {
            imgEdit.setVisibility(View.GONE);
            mBtn.setVisibility(View.GONE);
            mEdiMobile.setEnabled(false);
            mTxtName.setTextColor(ContextCompat.getColor(MyProfileActivity.this, R.color.color_primary_text));
            mTxtAddress.setTextColor(ContextCompat.getColor(MyProfileActivity.this, R.color.color_primary_text));
            mTxtCompanyName.setTextColor(ContextCompat.getColor(MyProfileActivity.this, R.color.color_primary_text));
            mTxtDesignation.setTextColor(ContextCompat.getColor(MyProfileActivity.this, R.color.color_primary_text));
            mTxtDepartment.setTextColor(ContextCompat.getColor(MyProfileActivity.this, R.color.color_primary_text));
            mTxtEmail.setTextColor(ContextCompat.getColor(MyProfileActivity.this, R.color.color_primary_text));
            mEdiMobile.setTextColor(ContextCompat.getColor(MyProfileActivity.this, R.color.color_primary_text));
            mEdiTel.setTextColor(ContextCompat.getColor(MyProfileActivity.this, R.color.color_primary_text));
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_edit) {
            setVisibility(true);
            return true;
        } else {
            return mDrawerFragment.mDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
        }
    }

    private View.OnClickListener mToolbarOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mDrawerFragment.setOpenCloseDrawer();
        }
    };

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (mLoaderId == id) {
            return new CursorLoader(MyProfileActivity.this, ProfileProvider.CONTENT_URI, null, null, null, null);
        }
        return null;
    }

    private void setData(Cursor data) {
        mProfile = Profile.getProfile(data, 0);
        mTxtName.setText(mProfile.getName());
        mTxtCompanyName.setText(mProfile.getCompanyName());
        mTxtDesignation.setText(mProfile.getDesignation());
        mTxtDepartment.setText(mProfile.getDepartmentName());
        mTxtAddress.setText(mProfile.getCompanyAddress());
        mTxtEmail.setText(mProfile.getEmail());
        mEdiMobile.setText(mProfile.getMobileNo());
        mEdiTel.setText(mProfile.getContactNo());
        Handler handler1 = new Handler();
        Runnable r1 = new Runnable() {
            public void run() {
                Glide.with(MyProfileActivity.this)
                        .load(mProfile.getProfileImage())
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .transform(new CircleTransform(MyProfileActivity.this))
                        .placeholder(R.mipmap.ic_action_person_light)
                        .into(mImgProfile);
            }
        };
        handler1.postDelayed(r1, 10);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == mLoaderId) {
            if (data != null) {
                if (data.getCount() > 0) {
                    setData(data);
                }
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
