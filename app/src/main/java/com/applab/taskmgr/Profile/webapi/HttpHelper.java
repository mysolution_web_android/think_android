package com.applab.taskmgr.Profile.webapi;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.applab.taskmgr.Login.model.Token;
import com.applab.taskmgr.Profile.activity.MyProfileActivity;
import com.applab.taskmgr.Profile.database.CRUDHelper;
import com.applab.taskmgr.Profile.model.Profile;
import com.applab.taskmgr.Profile.provider.ProfileProvider;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Utilities.AppController;
import com.applab.taskmgr.Utilities.GsonRequest;
import com.applab.taskmgr.Utilities.Utilities;
import com.google.gson.JsonObject;

import java.io.File;
import java.util.HashMap;

/**
 * -- =============================================
 * -- Author     : Vicknesh Balasubramaniam
 * -- Create date: 19/1/2016
 * -- Description: AssigneeList .java
 * -- =============================================
 * HISTORY OF UPDATE
 * <p/>
 * NO     DEVELOPER         DATETIME                      DESCRIPTION
 * *******************************************************************************
 * 1
 * 2
 */
public class HttpHelper {
    //region profile region
    public static void profileApi(Context context, String TAG, TextView txtError) {
        Token token = com.applab.taskmgr.Login.database.CRUDHelper.getToken(context);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", token.getToken());
        Log.i(TAG, "Token: " + token);
        GsonRequest<Profile> mGsonRequest = new GsonRequest<Profile>(
                Request.Method.GET,
                context.getString(R.string.base_url) + "User/SingleUserById?id=" + Utilities.encode(token.getUserId()),
                Profile.class,
                headers,
                responseProfileListener(context, txtError, TAG),
                errorProfileListener(context, txtError)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utilities.TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<Profile> responseProfileListener(final Context context, final TextView txtError, final String TAG) {
        return new Response.Listener<Profile>() {
            @Override
            public void onResponse(Profile response) {
                if (txtError != null) {
                    txtError.setVisibility(View.GONE);
                }
                Log.i(TAG, "Success Retrieved Data - " + response);
                context.getContentResolver().delete(ProfileProvider.CONTENT_URI, null, null);
                CRUDHelper.insertProfile(response, context, TAG);
            }
        };
    }

    public static Response.ErrorListener errorProfileListener(final Context context, final TextView txtError) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                if (txtError != null) {
                    txtError.setVisibility(View.VISIBLE);
                    Utilities.serverHandlingError(context, error, txtError);
                } else {
                    Utilities.serverHandlingError(context, error);
                }
            }
        };
    }
    //endregion

    public static void putProfileImage(File file, String contactNo, Context context, String TAG, MultiPartRequest.MultipartProgressListener multipartProgressListener) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", com.applab.taskmgr.Login.database.CRUDHelper.getToken(context).getToken());
        MultiPartRequest<String> mGsonRequest = new MultiPartRequest<String>(
                Request.Method.PUT,
                context.getString(R.string.base_url) + "Account/EditProfile",
                String.class,
                headers,
                responsePutImageListener(context, TAG),
                errorPutImageListener(context),
                file,
                multipartProgressListener, contactNo) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utilities.TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }


    private static Response.Listener<String> responsePutImageListener(final Context context, final String TAG) {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Utilities.showError(context, Utilities.CODE_SUCCESS_UPDATED, Utilities.SUCCESS_UPDATED);
                Intent intent = new Intent(MyProfileActivity.TAG);
                intent.putExtra("success", true);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }
        };
    }

    private static Response.ErrorListener errorPutImageListener(final Context context) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Intent intent = new Intent(MyProfileActivity.TAG);
                intent.putExtra("failed", true);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                Utilities.serverHandlingError(context, error);
            }
        };
    }


}
