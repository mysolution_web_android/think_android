package com.applab.taskmgr.Profile.webapi;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.applab.taskmgr.Login.database.CRUDHelper;
import com.applab.taskmgr.Profile.activity.MyProfileActivity;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Utilities.AppController;
import com.applab.taskmgr.Utilities.Utilities;
import com.google.gson.JsonObject;

import java.io.File;
import java.util.HashMap;

public class ProfileUploadService extends IntentService implements MultiPartRequest.MultipartProgressListener {
    private String mFilePath;
    private String mMobileNo;
    private int id;
    private String TAG = "EDIT PROFILE";
    private File mFile;

    public ProfileUploadService() {
        super("ProfileUploadService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle bundle = intent.getExtras();
        mFilePath = bundle.getString("filePath");
        mMobileNo = bundle.getString("mobileNo");
        if (mFilePath != null) {
            mFile = new File(mFilePath);
        }

        if (mFile != null) {
            if (Utilities.getUsableSpace() < new File(mFilePath).length()) {
                Utilities.showError(getBaseContext(), Utilities.CODE_LOCAL_SIZE_EXCEEDED, Utilities.LOCAL_SIZE_EXCEED);
            } else {
                putProfileImage(mFile, mMobileNo);
            }
        } else {
            putProfileImage(null, mMobileNo);
        }

    }

    @Override
    public void transferred(long transferred, long progress) {
        id = 1511;
        final NotificationManager mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setContentTitle(mFile.getName())
                .setContentText(Utilities.UPLOAD_PROGRESS)
                .setSmallIcon(R.mipmap.ic_launcher);

        // Start a lengthy operation in a background thread
        mBuilder.setProgress(100, (int) progress, false);
        // Displays the progress bar for the first time.
        mNotifyManager.notify(id, mBuilder.build());
        // When the loop is finished, updates the notification
        if (100 == progress) {
            mBuilder.setContentText(Utilities.UPLOAD_COMPLETE)
                    // Removes the progress bar
                    .setProgress(0, 0, false);
            mNotifyManager.notify(id, mBuilder.build());
        }
    }

    public void putProfileImage(File file, String contactNo) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + CRUDHelper.getToken(getBaseContext()).getToken());
        MultiPartRequest<JsonObject> mGsonRequest = new MultiPartRequest<JsonObject>(
                Request.Method.PUT,
                getBaseContext().getString(R.string.base_url) + "Account/EditProfile",
                JsonObject.class,
                headers,
                responseListener(),
                errorListener(),
                file,
                ProfileUploadService.this, contactNo) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utilities.TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }


    private Response.Listener<JsonObject> responseListener() {
        return new Response.Listener<JsonObject>() {
            @Override
            public void onResponse(JsonObject response) {
                Utilities.showError(getBaseContext(), Utilities.CODE_SUCCESS_UPDATED, Utilities.SUCCESS_UPDATED);
                Intent intent = new Intent(MyProfileActivity.TAG);
                intent.putExtra("success", true);
                LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(intent);
            }
        };
    }

    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Utilities.serverHandlingError(getBaseContext(), error);
            }
        };
    }
}
