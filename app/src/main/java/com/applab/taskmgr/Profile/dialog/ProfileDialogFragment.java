package com.applab.taskmgr.Profile.dialog;

/**
 * Created by user on 18/2/2016.
 */

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.applab.taskmgr.Profile.activity.MyProfileActivity;
import com.applab.taskmgr.Profile.model.Profile;
import com.applab.taskmgr.R;

/**
 * Created by user on 27/10/2015.
 */
public class ProfileDialogFragment extends DialogFragment {
    private Profile mProfile;
    private LinearLayout mLvCamera, mLvGallery;
    private ImageView mBtnCancel;
    private static AlertDialog.Builder builder;
    private static Context mContext;

    /**
     * Public factory method to create new RenameFileDialogFragment instances.
     *
     * @return Dialog ready to show.
     */
    public static ProfileDialogFragment newInstance(Profile profile, Context context) {
        ProfileDialogFragment frag = new ProfileDialogFragment();
        Bundle args = new Bundle();
        args.putParcelable("profile", profile);
        mContext = context;
        frag.setArguments(args);
        builder = new AlertDialog.Builder(context);
        mContext = context;
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mProfile = getArguments().getParcelable("profile");
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.window_profile_image, null);
        mBtnCancel = (ImageView) v.findViewById(R.id.btnCancel);
        mLvCamera = (LinearLayout) v.findViewById(R.id.lvCamera);
        mLvGallery = (LinearLayout) v.findViewById(R.id.lvGallery);
        mBtnCancel.setOnClickListener(btnCancelOnClickListener);
        mLvGallery.setOnClickListener(btnGalleryOnClickListener);
        mLvCamera.setOnClickListener(btnCameraOnClickListener);

        // Build the dialog
        builder.setView(v);
        Dialog d = builder.create();
        d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return d;
    }

    private View.OnClickListener btnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ProfileDialogFragment.this.getDialog().cancel();
        }
    };
    private View.OnClickListener btnCameraOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MyProfileActivity.TAG);
            intent.putExtra("isCamera", true);
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
            ProfileDialogFragment.this.getDialog().cancel();
        }
    };
    private View.OnClickListener btnGalleryOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MyProfileActivity.TAG);
            intent.putExtra("isGallery", true);
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
            ProfileDialogFragment.this.getDialog().cancel();
        }
    };
}

