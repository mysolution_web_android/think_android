package com.applab.taskmgr.Profile.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.applab.taskmgr.Profile.provider.ProfileProvider;
import com.applab.taskmgr.Profile.model.Profile;
import com.applab.taskmgr.Utilities.DBHelper;

/**
 * -- =============================================
 * -- Author     : Vicknesh Balasubramaniam
 * -- Create date: 19/1/2016
 * -- Description: AssigneeList .java
 * -- =============================================
 * HISTORY OF UPDATE
 * <p/>
 * NO     DEVELOPER         DATETIME                      DESCRIPTION
 * *******************************************************************************
 * 1
 * 2
 */
public class CRUDHelper {
    public static Profile getProfile(Context context) {
        Profile profile = null;
        Uri uri = ProfileProvider.CONTENT_URI;
        String sorting = DBHelper.PROFILE_COLUMN_ID + " DESC LIMIT 1 ";
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(uri, null, null, null, sorting);
            if (cursor != null && cursor.moveToFirst()) {
                profile = new Profile();
                profile = Profile.getProfile(cursor, 0);
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return profile;
    }

    public static void insertProfile(Profile profile, Context context, String TAG) {
        DBHelper helper = new DBHelper(context);
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(DBHelper.PROFILE_COLUMN_CONTACT_NO, profile.getContactNo());
            contentValues.put(DBHelper.PROFILE_COLUMN_COMPANY_ID, profile.getCompanyId());
            contentValues.put(DBHelper.PROFILE_COLUMN_COMPANY_NAME, profile.getCompanyName());
            contentValues.put(DBHelper.PROFILE_COLUMN_DEPARTMENT_ID, profile.getDepartmentId());
            contentValues.put(DBHelper.PROFILE_COLUMN_DEPARTMENT_NAME, profile.getDepartmentName());
            contentValues.put(DBHelper.PROFILE_COLUMN_DESIGNATION, profile.getDesignation());
            contentValues.put(DBHelper.PROFILE_COLUMN_LAST_UPDATED_DATE, profile.getLastUpdateDate());
            contentValues.put(DBHelper.PROFILE_COLUMN_PROFILE_IMAGE, profile.getProfileImage());
            contentValues.put(DBHelper.PROFILE_COLUMN_EMAIL, profile.getEmail());
            contentValues.put(DBHelper.PROFILE_COLUMN_COMPANY_ADDRESS, profile.getCompanyAddress());
            contentValues.put(DBHelper.PROFILE_COLUMN_MOBILE, profile.getMobileNo());
            contentValues.put(DBHelper.PROFILE_COLUMN_NAME, profile.getName());
            contentValues.put(DBHelper.PROFILE_COLUMN_PASSWORD_LAST_CHANGED_DATE, profile.getPasswordLastChangeDate());
            contentValues.put(DBHelper.PROFILE_COLUMN_USER_END_DATE, profile.getUserEndDate());
            contentValues.put(DBHelper.PROFILE_COLUMN_USER_START_DATE, profile.getUserStartDate());
            contentValues.put(DBHelper.PROFILE_COLUMN_USER_ID, profile.getUserId());
            context.getContentResolver().insert(ProfileProvider.CONTENT_URI, contentValues);
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Download Error :" + ex.toString());
        }
    }
}
