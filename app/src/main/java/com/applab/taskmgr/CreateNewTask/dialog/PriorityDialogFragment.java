package com.applab.taskmgr.CreateNewTask.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.applab.taskmgr.CreateNewTask.activity.CreateNewTaskActivity;
import com.applab.taskmgr.R;

/**
 * Created by user on 23-Feb-16.
 */
public class PriorityDialogFragment extends DialogFragment {
    private TextView mTxtTitle;
    private ImageView mBtnCancel;
    private LinearLayout mBtnHigh;
    private LinearLayout mBtnLow;
    private static AlertDialog.Builder builder;
    private static Context mContext;
    private String TAG = PriorityDialogFragment.class.getSimpleName();

    public static PriorityDialogFragment newInstance(final Context context) {
        PriorityDialogFragment frag = new PriorityDialogFragment();
        Bundle args = new Bundle();
        mContext = context;
        builder = new AlertDialog.Builder(context);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.window_priority, null);

        mTxtTitle = (TextView) v.findViewById(R.id.txtTitle);
        mBtnCancel = (ImageView) v.findViewById(R.id.btnCancel);
        mBtnHigh = (LinearLayout) v.findViewById(R.id.btn_high);
        mBtnLow = (LinearLayout) v.findViewById(R.id.btn_low);

        mBtnCancel.setOnClickListener(btnCancelOnClickListener);
        mBtnLow.setOnClickListener(btnLowOnHighClickListener);
        mBtnHigh.setOnClickListener(btnHighOnClickListener);

        mTxtTitle.setText(getResources().getString(R.string.priority));

        //Builder dialog
        builder.setView(v);
        Dialog d = builder.create();
        d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return d;
    }

    private View.OnClickListener btnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            PriorityDialogFragment.this.getDialog().cancel();
        }
    };

    private View.OnClickListener btnHighOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(CreateNewTaskActivity.class.getSimpleName());
            intent.putExtra("Priority",mContext.getString(R.string.high));
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
            PriorityDialogFragment.this.getDialog().cancel();
        }
    };

    private View.OnClickListener btnLowOnHighClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(CreateNewTaskActivity.class.getSimpleName());
            intent.putExtra("Priority",mContext.getString(R.string.low));
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
            PriorityDialogFragment.this.getDialog().cancel();
        }
    };
}
