package com.applab.taskmgr.CreateNewTask.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applab.taskmgr.AssignTo.activity.AssignToActivity;
import com.applab.taskmgr.CreateNewTask.dialog.PriorityDialogFragment;
import com.applab.taskmgr.CreateNewTask.modal.CreateTask;
import com.applab.taskmgr.CreateNewTask.webapi.HttpHelperCreate;
import com.applab.taskmgr.FileExplorer.activity.FileExplorerActivity;
import com.applab.taskmgr.Menu.fragment.NavigationDrawerFragment;
import com.applab.taskmgr.Profile.database.CRUDHelper;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Task.model.AsigneeList;
import com.applab.taskmgr.Task.model.Task;
import com.applab.taskmgr.Task.provider.AssigneeProvider;
import com.applab.taskmgr.Task.provider.TaskProvider;
import com.applab.taskmgr.Utilities.DBHelper;
import com.applab.taskmgr.Utilities.Utilities;
import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.sleepbot.datetimepicker.time.RadialPickerLayout;
import com.sleepbot.datetimepicker.time.TimePickerDialog;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * -- =============================================
 * -- Author     : Vicknesh Balasubramaniam
 * -- Create date: 4/2/2016
 * -- Description: AssigneeList .java
 * -- =============================================
 * HISTORY OF UPDATE
 * <p/>
 * NO     DEVELOPER         DATETIME                      DESCRIPTION
 * *******************************************************************************
 * 1
 * 2
 */

public class CreateNewTaskActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener, LoaderManager.LoaderCallbacks<Cursor> {

    private Toolbar mToolbar;
    private TextView mTxtToolbarTitle;

    private NavigationDrawerFragment mDrawerFragment;

    private TextView mTxtStartDate, mTxtEndDate, mTxtPriority, mTxtAttachment, mTxtAssignedBy, mTxtAssignedTo, mTxtSave;

    private EditText mEdiName;
    private EditText mEdiVenue;
    private EditText mEdiMessage;

    private LinearLayout mBtnStartDate;
    private LinearLayout mBtnEndDate;
    private LinearLayout mBtnAssignedTo;
    private LinearLayout mBtnPriority;
    private LinearLayout mBtnSave;
    private LinearLayout mBtnCancel;
    private LinearLayout mRlAttachmentFile;

    private RelativeLayout mBtnAttachment, mRlAttachment;

    public static final String DATEPICKER_TAG = "datepicker";
    public static final String TIMEPICKER_TAG = "timepicker";

    private DatePickerDialog mDatePickerDialog;
    private TimePickerDialog mTimePickerDialog;

    private String TAG = CreateNewTaskActivity.class.getSimpleName();
    private int mPressed = 0;
    private String mStart, mEnd;
    private ArrayList<String> mAssigneeId = new ArrayList<>();
    private ArrayList<String> mAssigneeName = new ArrayList<>();
    private String mDateTimeFormat = "dd MMM yyyy h:mm a";
    private String mDateTimeFormatFromCreate = "dd MMM yyyy h:00 a";
    private File mAttachment;
    private int mRequestCode = 567;
    private ProgressBar mProgressBar;
    private RelativeLayout mRl;
    private Task mTask = null;
    private int mLoaderTask = 567;
    private int mLoaderAssignee = 67;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_task);
        mAttachment = null;
        mToolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(mToolbar);
        mTxtToolbarTitle = (TextView) mToolbar.findViewById(R.id.txtTitle);
        mToolbar.setFitsSystemWindows(false);
        mToolbar.setPadding(0, 0, 0, 0);
        mTxtToolbarTitle.setText(getString(R.string.title_create_new_task));

        mDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        mDrawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        mDrawerFragment.mDrawerToggle.setDrawerIndicatorEnabled(false);
        mDrawerFragment.setPaddingRelativeLayout();

        mToolbar.setNavigationIcon(R.mipmap.menu);
        mToolbar.setNavigationOnClickListener(mToolbarOnClickListener);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mRl = (RelativeLayout) findViewById(R.id.fadeRL);
        mTxtStartDate = (TextView) findViewById(R.id.txtStartDate);
        mTxtEndDate = (TextView) findViewById(R.id.txtEndDate);
        mTxtPriority = (TextView) findViewById(R.id.txtPriority);
        mTxtAttachment = (TextView) findViewById(R.id.txtAttachment);
        mTxtAssignedBy = (TextView) findViewById(R.id.txtAssignedBy);
        mTxtAssignedTo = (TextView) findViewById(R.id.txtAssignedTo);
        mTxtSave = (TextView) findViewById(R.id.txtSave);

        mTxtAssignedBy.setText(CRUDHelper.getProfile(this).getName());

        mStart = Utilities.setCalendarDate(mDateTimeFormatFromCreate, new Date(new Date().getTime() + 3600 * 1000));
        mEnd = Utilities.setCalendarDate(mDateTimeFormatFromCreate, new Date(new Date().getTime() + 2 * 3600 * 1000));
        mTxtStartDate.setText(mStart);
        mTxtEndDate.setText(mEnd);

        mEdiName = (EditText) findViewById(R.id.ediName);
        mEdiVenue = (EditText) findViewById(R.id.ediVenue);
        mEdiMessage = (EditText) findViewById(R.id.ediMessage);

        mBtnPriority = (LinearLayout) findViewById(R.id.btnPriority);
        mBtnStartDate = (LinearLayout) findViewById(R.id.btnStartDate);
        mBtnEndDate = (LinearLayout) findViewById(R.id.btnEndDate);
        mBtnSave = (LinearLayout) findViewById(R.id.btnSave);
        mBtnCancel = (LinearLayout) findViewById(R.id.btnCancel);
        mBtnAssignedTo = (LinearLayout) findViewById(R.id.btnAssignedTo);
        mBtnAttachment = (RelativeLayout) findViewById(R.id.btnAttachment);
        mRlAttachment = (RelativeLayout) findViewById(R.id.rlAttachment);
        mRlAttachmentFile = (LinearLayout) findViewById(R.id.rlAttachmentFile);

        Calendar calendar = Calendar.getInstance();

        mDatePickerDialog = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        mTimePickerDialog = TimePickerDialog.newInstance(this, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), false, false);

        mBtnPriority.setOnClickListener(mBtnPriorityOnClickListener);
        mBtnAssignedTo.setOnClickListener(mBtnAssignedToOnClickListener);
        mBtnStartDate.setOnClickListener(mBtnStartDateOnClickListener);
        mBtnEndDate.setOnClickListener(mBtnEndDateOnClickListener);
        mBtnSave.setOnClickListener(mBtnSaveOnClickListener);
        mBtnCancel.setOnClickListener(mBtnCancelOnClickListener);
        mBtnAttachment.setOnClickListener(mBtnAttachmentToOnClickListener);
        mRlAttachment.setOnClickListener(mRlAttachmentOnClickListener);
        setIntent(getIntent());
    }

    private View.OnClickListener mRlAttachmentOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mRlAttachmentFile.getVisibility() == View.GONE) {
                startActivityForResult(new Intent(CreateNewTaskActivity.this, FileExplorerActivity.class), mRequestCode);
            } else {
                mAttachment = null;
                mRlAttachmentFile.setVisibility(View.GONE);
            }
        }
    };

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(TAG)) {
                if (intent.getBooleanExtra("failed", false)) {
                    Utilities.setFadeProgressBarVisibility(false, mRl, mProgressBar);
                } else if (intent.getStringExtra("Priority") != null) {
                    mTxtPriority.setText(intent.getStringExtra("Priority"));
                }
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == mRequestCode) {
            setIntent(data);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter iff = new IntentFilter(TAG);
        LocalBroadcastManager.getInstance(getBaseContext()).registerReceiver(broadcastReceiver, iff);
    }

    public void setIntent(Intent intent) {
        if (intent.getStringArrayListExtra("arrayUserId") != null) {
            mAssigneeId = intent.getStringArrayListExtra("arrayUserId");
            mAssigneeName = intent.getStringArrayListExtra("arrayUserName");
            String assigneeName = "";
            for (int i = 0; i < mAssigneeId.size(); i++) {
                if (i == mAssigneeId.size() - 1) {
                    assigneeName += mAssigneeName.get(i);
                } else {
                    assigneeName += mAssigneeName.get(i) + ", ";
                }

            }
            if (assigneeName.length() > 0) {
                mTxtAssignedTo.setText(assigneeName);
            }
        }
        if (intent.getSerializableExtra("Attachment") != null) {
            mAttachment = (File) intent.getSerializableExtra("Attachment");
            mRlAttachmentFile.setVisibility(View.VISIBLE);
            mTxtAttachment.setText(mAttachment.getName());
        }
        if (intent.getParcelableExtra("Task") != null) {
            mTask = intent.getParcelableExtra("Task");
            mTxtSave.setText(getString(R.string.save));
            mTxtToolbarTitle.setText(getString(R.string.title_activity_edit_task));
            getSupportLoaderManager().initLoader(mLoaderTask, null, this);
            getSupportLoaderManager().initLoader(mLoaderAssignee, null, this);
        }
    }

    private View.OnClickListener mBtnAttachmentToOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivityForResult(new Intent(CreateNewTaskActivity.this, FileExplorerActivity.class), mRequestCode);
        }
    };

    private View.OnClickListener mBtnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    private View.OnClickListener mBtnPriorityOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            PriorityDialogFragment priority = PriorityDialogFragment.newInstance(CreateNewTaskActivity.this);
            priority.show(getFragmentManager(), TAG);
        }
    };

    private View.OnClickListener mBtnAssignedToOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(CreateNewTaskActivity.this, AssignToActivity.class);
            intent.putExtra("arrayUserId", mAssigneeId);
            intent.putExtra("arrayUserName", mAssigneeName);
            startActivityForResult(intent, mRequestCode);
        }
    };

    private View.OnClickListener mBtnStartDateOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mPressed = 0;
            mTimePickerDialog.show(getSupportFragmentManager(), TIMEPICKER_TAG);
            mDatePickerDialog.setYearRange(1985, 2028);
            mDatePickerDialog.show(getSupportFragmentManager(), DATEPICKER_TAG);
        }
    };

    private View.OnClickListener mBtnEndDateOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mPressed = 1;
            mTimePickerDialog.show(getSupportFragmentManager(), TIMEPICKER_TAG);
            mDatePickerDialog.setYearRange(1985, 2028);
            mDatePickerDialog.show(getSupportFragmentManager(), DATEPICKER_TAG);
        }
    };

    private View.OnClickListener mBtnSaveOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (Utilities.setCalendarDate(mDateTimeFormat, mTxtEndDate.getText().toString()).before(Utilities.setCalendarDate(mDateTimeFormat, mTxtStartDate.getText().toString()))) {
                String startDate = mTxtStartDate.getText().toString();
                String error = Utilities.getDialogMessage(CreateNewTaskActivity.this, Utilities.CODE_END_AFTER_START_DATE, Utilities.END_AFTER_START_DATE);
                Utilities.showError(CreateNewTaskActivity.this, "", error + startDate);
            } else {
                Utilities.setFadeProgressBarVisibility(true, mRl, mProgressBar);
                Utilities.hideKeyboard(CreateNewTaskActivity.this);
                CreateTask current = new CreateTask();
                current.setLocation(Utilities.encode(mEdiVenue.getText().toString()));
                current.setTaskTitle(Utilities.encode(mEdiName.getText().toString()));
                current.setPriority(Utilities.encode(mTxtPriority.getText().toString()));
                current.setAssigneeIdList(mAssigneeId);
                current.setTaskDescription(Utilities.encode(mEdiMessage.getText().toString()));
                current.setTaskDate(Utilities.encode(Utilities.setToUTCDate(mDateTimeFormat, Utilities.DATE_FORMAT, mTxtStartDate.getText().toString())));
                current.setTaskDueDate(Utilities.encode(Utilities.setToUTCDate(mDateTimeFormat, Utilities.DATE_FORMAT, mTxtEndDate.getText().toString())));
                if (mTask == null) {
                    HttpHelperCreate.createApi(CreateNewTaskActivity.this, TAG, current, mAttachment);
                } else {
                    HttpHelperCreate.updateApi(CreateNewTaskActivity.this, TAG, current, mAttachment, mTask);
                }
            }
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
        Utilities.hideKeyboard(CreateNewTaskActivity.this);
        LocalBroadcastManager.getInstance(getBaseContext()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onBackPressed() {
        mDrawerFragment.setCloseDrawer(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_new_task, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return mDrawerFragment.mDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

    private View.OnClickListener mToolbarOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mDrawerFragment.setOpenCloseDrawer();
        }
    };

    @Override
    public void onDateSet(DatePickerDialog datepickerdialog, int year, int month, int day) {
        String[] m = getResources().getStringArray(R.array.month);
        String strMonth = String.valueOf(m[month]);
        String strDay = "";
        if (day < 10) {
            strDay = "0" + String.valueOf(day);
        } else {
            strDay = String.valueOf(day);
        }
        if (mPressed == 0) {
            mStart = strDay + " " + strMonth + " " + year;
            mTxtStartDate.setText(mStart);
        }

        if (mPressed == 1) {
            mEnd = strDay + " " + strMonth + " " + year;
            mTxtEndDate.setText(mEnd);
        }
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
        String strMinute = "";
        String strHourOfDay = "";
        String str = "";
        if (hourOfDay >= 12) {
            if (hourOfDay != 12) {
                hourOfDay = hourOfDay - 12;
            }
            strHourOfDay = "" + hourOfDay;
            str = "PM";
        } else {
            if (hourOfDay == 0) {
                strHourOfDay = "" + 12;
            } else {
                strHourOfDay = "" + hourOfDay;
            }
            str = "AM";
        }
        if (minute < 10) {
            strMinute = "0" + minute;
        } else {
            strMinute = "" + minute;
        }

        if (mPressed == 0) {
            mStart += " " + strHourOfDay + ":" + strMinute + " " + str;
            mTxtStartDate.setText(mStart);
        }

        if (mPressed == 1) {
            mEnd += " " + strHourOfDay + ":" + strMinute + " " + str;
            mTxtEndDate.setText(mEnd);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (mLoaderTask == id) {
            return new CursorLoader(CreateNewTaskActivity.this, TaskProvider.CONTENT_URI, null, DBHelper.TASK_COLUMN_TASK_ID + "=?", new String[]{mTask.getTaskId().toString()}, null);
        } else if (mLoaderAssignee == id) {
            return new CursorLoader(CreateNewTaskActivity.this, AssigneeProvider.CONTENT_URI, null, DBHelper.TASK_COLUMN_TASK_ID + "=?", new String[]{mTask.getTaskId().toString()}, null);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (mLoaderTask == loader.getId()) {
            if (data != null) {
                mTask = Task.getTask(data, 0);
                String endDate = Utilities.setToLocalDateTime(Utilities.DATE_FORMAT, mDateTimeFormat, mTask.getTaskDueDate());
                String startDate = Utilities.setToLocalDateTime(Utilities.DATE_FORMAT, mDateTimeFormat, mTask.getTaskDate());
                mTxtEndDate.setText(endDate);
                mTxtStartDate.setText(startDate);
                mTxtAssignedBy.setText(CRUDHelper.getProfile(CreateNewTaskActivity.this).getName());
                mTxtPriority.setText(mTask.getPriority() == 0 ? getString(R.string.low) : getString(R.string.high));
                mEdiName.setText(mTask.getTaskTitle());
                mEdiVenue.setText(mTask.getLocation());
                mEdiMessage.setText(mTask.getTaskDescription());
                if (mTask.getFile() != null) {
                    String url = mTask.getFile().split("/")[mTask.getFile().split("/").length - 1];
                    if (url != null) {
                        mTxtAttachment.setText(url);
                        mRlAttachment.setVisibility(View.VISIBLE);
                    }
                }
            }
        } else if (mLoaderAssignee == loader.getId()) {
            if (data != null) {
                if (data.getCount() > 0) {
                    ArrayList<AsigneeList> asigneeLists = new ArrayList<>();
                    String assignedTo = "";
                    mAssigneeId.clear();
                    mAssigneeName.clear();
                    for (int i = 0; i < data.getCount(); i++) {
                        AsigneeList asigneeList = AsigneeList.getAssigneeList(data, i);
                        asigneeLists.add(asigneeList);
                        if (i != data.getCount() - 1) {
                            assignedTo += asigneeList.getUserName() + ", ";
                        } else {
                            assignedTo += asigneeList.getUserName();
                        }
                        mAssigneeId.add(asigneeList.getUserId());
                        mAssigneeName.add(asigneeList.getUserName());
                    }
                    mTxtAssignedTo.setText(assignedTo);
                    mTask.setAssigneeList(asigneeLists);
                }
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
