package com.applab.taskmgr.CreateNewTask.webapi;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.applab.taskmgr.CreateNewTask.activity.CreateNewTaskActivity;
import com.applab.taskmgr.CreateNewTask.modal.CreateTask;
import com.applab.taskmgr.CreateNewTask.service.CreateTaskUploadService;
import com.applab.taskmgr.Login.database.CRUDHelper;
import com.applab.taskmgr.Login.model.Token;
import com.applab.taskmgr.Login.provider.TokenProvider;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Task.model.Task;
import com.applab.taskmgr.Task.webapi.HttpHelperTask;
import com.applab.taskmgr.Utilities.AppController;
import com.applab.taskmgr.Utilities.GsonRequest;
import com.applab.taskmgr.Utilities.Utilities;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.http.entity.ContentType;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import eu.janmuller.android.simplecropimage.Util;

/**
 * -- =============================================
 * -- Author     : Lau Pong
 * -- Create date: 18/2/2016
 * -- Description: HttpHelperCreate.java
 * -- =============================================
 * HISTORY OF UPDATE
 * <p/>
 * NO     DEVELOPER         DATETIME                      DESCRIPTION
 * *******************************************************************************
 * 1
 * 2
 */
public class HttpHelperCreate {
    public static void createApi(final Context context, String TAG, CreateTask createTask, File attachment) {
        Gson gson = new GsonBuilder().serializeNulls().create();
        final String json = gson.toJson(createTask);
        Map<String, String> params = new HashMap<String, String>();
        params.put("Authorization", CRUDHelper.getToken(context).getToken());
        GsonRequest<String> mGsonRequest = new GsonRequest<String>(
                Request.Method.POST,
                context.getString(R.string.base_url) + "Task/Create",
                String.class,
                params,
                responseCreateTaskListener(context, TAG, attachment),
                errorCreateTaskListener(context, TAG)) {

            @Override
            public String getBodyContentType() {
                return ContentType.APPLICATION_JSON.toString();
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return json.getBytes();
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utilities.TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<String> responseCreateTaskListener(final Context context, final String TAG, final File attachment) {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i(TAG, "Data Successfully Entered - " + response);
                Utilities.showError(context, "", Utilities.SUCCESS_CREATED);
                if (attachment != null) {
                    Intent intent = new Intent(context, CreateTaskUploadService.class);
                    intent.putExtra("TaskId", response);
                    intent.putExtra("file", attachment);
                    context.startService(intent);
                }
                Utilities.showError(context, "", Utilities.SUCCESS_CREATED);
                ((Activity) context).finish();
            }
        };
    }

    public static Response.ErrorListener errorCreateTaskListener(final Context context, final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Intent intent = new Intent(TAG);
                intent.putExtra("failed", true);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                Utilities.serverHandlingError(context, error);
            }
        };
    }

    public static void updateFile(final Context context, String TAG, File attachment, String taskId, MultiPartRequest.MultipartProgressListener multipartProgressListener) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", com.applab.taskmgr.Login.database.CRUDHelper.getToken(context).getToken());
        MultiPartRequest<String> mGsonRequest = new MultiPartRequest<String>(
                Request.Method.PUT,
                context.getString(R.string.base_url) + "Task/UpdateFile?task_id=" + Utilities.encode(taskId),
                String.class,
                headers,
                responseUpdateFileListener(context, TAG, taskId),
                errorUpdateFileListenr(context, TAG),
                attachment,
                multipartProgressListener) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utilities.TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<String> responseUpdateFileListener(final Context context, final String TAG, final String taskId) {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i(TAG, "Data Succesfully Enterd - " + response);
                HttpHelperTask.getSingleTask(context, TAG, taskId, context.getString(R.string.created_task));
            }
        };
    }

    public static Response.ErrorListener errorUpdateFileListenr(final Context context, final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utilities.serverHandlingError(context, error);
            }
        };
    }

    public static void updateApi(final Context context, String TAG, CreateTask createTask, File attachment, Task task) {
        Gson gson = new GsonBuilder().serializeNulls().create();
        final String json = gson.toJson(createTask);
        Map<String, String> params = new HashMap<String, String>();
        params.put("Authorization", CRUDHelper.getToken(context).getToken());
        GsonRequest<String> mGsonRequest = new GsonRequest<String>(
                Request.Method.PUT,
                context.getString(R.string.base_url) + "Task/Update?task_id=" + task.getTaskId().toString(),
                String.class,
                params,
                responseUpdateTaskListener(context, TAG, attachment),
                errorUpdateTaskListener(context, TAG)) {

            @Override
            public String getBodyContentType() {
                return ContentType.APPLICATION_JSON.toString();
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return json.getBytes();
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utilities.TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<String> responseUpdateTaskListener(final Context context, final String TAG, final File attachment) {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i(TAG, "Data Successfully Entered - " + response);
                Utilities.showError(context, "", Utilities.SUCCESS_UPDATED);
                if (attachment != null) {
                    Intent intent = new Intent(context, CreateTaskUploadService.class);
                    intent.putExtra("TaskId", response);
                    intent.putExtra("file", attachment);
                    context.startService(intent);
                } else {
                    HttpHelperTask.getSingleTask(context, TAG, response, context.getString(R.string.created_task));
                }
                ((Activity) context).finish();
            }
        };
    }

    public static Response.ErrorListener errorUpdateTaskListener(final Context context, final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Intent intent = new Intent(TAG);
                intent.putExtra("failed", true);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                Utilities.serverHandlingError(context, error);
            }
        };
    }
}
