package com.applab.taskmgr.CreateNewTask.service;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.applab.taskmgr.CreateNewTask.webapi.HttpHelperCreate;
import com.applab.taskmgr.CreateNewTask.webapi.MultiPartRequest;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Utilities.Utilities;

import java.io.File;

/**
 * Created by user on 26/2/2016.
 */
public class CreateTaskUploadService extends IntentService implements MultiPartRequest.MultipartProgressListener {
    private int mId = 151;
    private String mTaskId;
    private String TAG = CreateTaskUploadService.class.getSimpleName();
    private File mFile;

    public CreateTaskUploadService() {
        super(CreateTaskUploadService.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        mTaskId = intent.getStringExtra("TaskId");
        mFile = (File) intent.getSerializableExtra("file");
        if (mFile != null) {
            HttpHelperCreate.updateFile(CreateTaskUploadService.this, TAG, mFile, mTaskId, CreateTaskUploadService.this);
        }
    }

    @Override
    public void transferred(long transferred, long progress) {
        final NotificationManager mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setContentTitle(mFile.getName())
                .setContentText(Utilities.UPLOAD_PROGRESS)
                .setSmallIcon(R.mipmap.ic_launcher);

        // Start a lengthy operation in a background thread
        mBuilder.setProgress(100, (int) progress, false);
        // Displays the progress bar for the first time.
        mNotifyManager.notify(mId, mBuilder.build());
        // When the loop is finished, updates the notification
        if (100 == progress) {
            mBuilder.setContentText(Utilities.UPLOAD_COMPLETE)
                    // Removes the progress bar
                    .setProgress(0, 0, false);
            mNotifyManager.notify(mId, mBuilder.build());
        }
    }
}
