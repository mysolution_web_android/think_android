package com.applab.taskmgr.CreateNewTask.modal;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateTask {

    @SerializedName("TaskTitle")
    @Expose
    private String TaskTitle;
    @SerializedName("TaskDescription")
    @Expose
    private String TaskDescription;
    @SerializedName("TaskDate")
    @Expose
    private String TaskDate;
    @SerializedName("TaskDueDate")
    @Expose
    private String TaskDueDate;
    @SerializedName("Location")
    @Expose
    private String Location;
    @SerializedName("Map")
    @Expose
    private String Map;
    @SerializedName("Priority")
    @Expose
    private String Priority;
    @SerializedName("AssignerIp")
    @Expose
    private String AssignerIp;
    @SerializedName("AssignerOs")
    @Expose
    private String AssignerOs;
    @SerializedName("AssignerOsVer")
    @Expose
    private String AssignerOsVer;
    @SerializedName("AssignerAppVer")
    @Expose
    private String AssignerAppVer;
    @SerializedName("AssigneeIdList")
    @Expose
    private List<String> AssigneeIdList = new ArrayList<String>();

    /**
     * @return The TaskTitle
     */
    public String getTaskTitle() {
        return TaskTitle;
    }

    /**
     * @param TaskTitle The TaskTitle
     */
    public void setTaskTitle(String TaskTitle) {
        this.TaskTitle = TaskTitle;
    }

    /**
     * @return The TaskDescription
     */
    public String getTaskDescription() {
        return TaskDescription;
    }

    /**
     * @param TaskDescription The TaskDescription
     */
    public void setTaskDescription(String TaskDescription) {
        this.TaskDescription = TaskDescription;
    }

    /**
     * @return The TaskDate
     */
    public String getTaskDate() {
        return TaskDate;
    }

    /**
     * @param TaskDate The TaskDate
     */
    public void setTaskDate(String TaskDate) {
        this.TaskDate = TaskDate;
    }

    /**
     * @return The TaskDueDate
     */
    public String getTaskDueDate() {
        return TaskDueDate;
    }

    /**
     * @param TaskDueDate The TaskDueDate
     */
    public void setTaskDueDate(String TaskDueDate) {
        this.TaskDueDate = TaskDueDate;
    }

    /**
     * @return The Location
     */
    public String getLocation() {
        return Location;
    }

    /**
     * @param Location The Location
     */
    public void setLocation(String Location) {
        this.Location = Location;
    }

    /**
     * @return The Map
     */
    public String getMap() {
        return Map;
    }

    /**
     * @param Map The Map
     */
    public void setMap(String Map) {
        this.Map = Map;
    }

    /**
     * @return The Priority
     */
    public String getPriority() {
        return Priority;
    }

    /**
     * @param Priority The Priority
     */
    public void setPriority(String Priority) {
        this.Priority = Priority;
    }

    /**
     * @return The AssignerIp
     */
    public String getAssignerIp() {
        return AssignerIp;
    }

    /**
     * @param AssignerIp The AssignerIp
     */
    public void setAssignerIp(String AssignerIp) {
        this.AssignerIp = AssignerIp;
    }

    /**
     * @return The AssignerOs
     */
    public String getAssignerOs() {
        return AssignerOs;
    }

    /**
     * @param AssignerOs The AssignerOs
     */
    public void setAssignerOs(String AssignerOs) {
        this.AssignerOs = AssignerOs;
    }

    /**
     * @return The AssignerOsVer
     */
    public String getAssignerOsVer() {
        return AssignerOsVer;
    }

    /**
     * @param AssignerOsVer The AssignerOsVer
     */
    public void setAssignerOsVer(String AssignerOsVer) {
        this.AssignerOsVer = AssignerOsVer;
    }

    /**
     * @return The AssignerAppVer
     */
    public String getAssignerAppVer() {
        return AssignerAppVer;
    }

    /**
     * @param AssignerAppVer The AssignerAppVer
     */
    public void setAssignerAppVer(String AssignerAppVer) {
        this.AssignerAppVer = AssignerAppVer;
    }

    /**
     * @return The AssigneeIdList
     */
    public List<String> getAssigneeIdList() {
        return AssigneeIdList;
    }

    /**
     * @param AssigneeIdList The AssigneeIdList
     */
    public void setAssigneeIdList(List<String> AssigneeIdList) {
        this.AssigneeIdList = AssigneeIdList;
    }

}