package com.applab.taskmgr.FileExplorer.adapter;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applab.taskmgr.FileExplorer.holder.FileExplorerViewHolder;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Utilities.Utilities;

import java.io.File;
import java.util.ArrayList;

/**
 * -- =============================================
 * -- Author     : Lau Pong
 * -- Create date: 29/2/2016
 * -- Description: FileExplorerAdapter.java
 * -- =============================================
 * HISTORY OF UPDATE
 * <p/>
 * NO     DEVELOPER         DATETIME                      DESCRIPTION
 * *******************************************************************************
 * 1
 * 2
 */

public class FileExplorerAdapter extends RecyclerView.Adapter<FileExplorerViewHolder> {
    private LayoutInflater mInflater;
    private Context mContext;
    private ArrayList<File> mCurrent;
    private String TAG = FileExplorerAdapter.class.getSimpleName();

    public FileExplorerAdapter(Context mContext, ArrayList<File> current) {
        this.mInflater = LayoutInflater.from(mContext);
        this.mContext = mContext;
        this.mCurrent = current;
    }

    @Override
    public FileExplorerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i("FileListViewHolder", "onCreateViewHolder called");
        View view = mInflater.inflate(R.layout.custom_file_list_row, parent, false);
        return new FileExplorerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FileExplorerViewHolder holder, int position) {
        Log.i("onBindViewHolder", "onBindViewHolder " + position);
        File current = mCurrent.get(position);
        holder.getTitle().setText(current.getName());
        holder.getTitle().setTag(current.getAbsoluteFile());
        if (current.isFile()) {
            holder.getDate().setText(Utilities.setCalendarDate("d MMMM yyyy", current.lastModified()));
            String[] result = current.getAbsolutePath().split("\\.");
            String type = result[result.length - 1];
            holder.getIcon().setImageResource(Utilities.getFileType(type));
            holder.getCapacity().setText(Utilities.bytesIntoHumanReadable(current.length()));
            holder.getCapacity().setVisibility(View.VISIBLE);
            holder.getDate().setVisibility(View.VISIBLE);
            holder.getDivider().setVisibility(View.VISIBLE);
        } else {
            holder.getCapacity().setVisibility(View.GONE);
            holder.getDate().setVisibility(View.GONE);
            holder.getDivider().setVisibility(View.GONE);
            holder.getIcon().setImageResource(R.mipmap.info_folder);
        }
    }

    @Override
    public int getItemCount() {
        return mCurrent == null ? 0 : mCurrent.size();
    }

    public ArrayList<File> swapFile(ArrayList<File> current) {
        if (this.mCurrent == current) {
            return null;
        }
        ArrayList<File> oldFile = this.mCurrent;
        this.mCurrent = current;
        android.os.Message msg = handler.obtainMessage();
        handler.handleMessage(msg);
        return oldFile;
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            FileExplorerAdapter.this.notifyDataSetChanged();
        }
    };
}
