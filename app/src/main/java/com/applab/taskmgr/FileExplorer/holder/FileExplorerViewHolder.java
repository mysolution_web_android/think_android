package com.applab.taskmgr.FileExplorer.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.applab.taskmgr.R;

/**
 * -- =============================================
 * -- Author     : Lau Pong
 * -- Create date: 29/2/2016
 * -- Description: FileExplorerViewHolder.java
 * -- =============================================
 * HISTORY OF UPDATE
 * <p/>
 * NO     DEVELOPER         DATETIME                      DESCRIPTION
 * *******************************************************************************
 * 1
 * 2
 */
public class FileExplorerViewHolder extends RecyclerView.ViewHolder {
    TextView title;
    TextView capacity;
    TextView date;
    ImageView icon;
    LinearLayout lv;
    LinearLayout lvText;
    View divider;
    ImageView imgStatus;

    public FileExplorerViewHolder(View itemView) {
        super(itemView);
        title = (TextView) itemView.findViewById(R.id.txtTitle);
        capacity = (TextView) itemView.findViewById(R.id.txtCapacity);
        date = (TextView) itemView.findViewById(R.id.txtDate);
        icon = (ImageView) itemView.findViewById(R.id.imgIcon);
        lv = (LinearLayout) itemView.findViewById(R.id.lv);
        lvText = (LinearLayout) itemView.findViewById(R.id.lvText);
        divider = (View) itemView.findViewById(R.id.divider);
        imgStatus = (ImageView) itemView.findViewById(R.id.imgStatus);
    }

    public TextView getTitle() {
        return title;
    }

    public TextView getCapacity() {
        return capacity;
    }

    public TextView getDate() {
        return date;
    }

    public ImageView getIcon() {
        return icon;
    }

    public LinearLayout getLv() {
        return lv;
    }

    public LinearLayout getLvText() {
        return lvText;
    }

    public View getDivider() {
        return divider;
    }

    public ImageView getImgStatus() {
        return imgStatus;
    }
}
