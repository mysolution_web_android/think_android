package com.applab.taskmgr.FileExplorer.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import com.applab.taskmgr.FileExplorer.adapter.FileExplorerAdapter;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Utilities.ItemClickSupport;
import com.applab.taskmgr.Utilities.Utilities;
import java.io.File;
import java.util.ArrayList;

/**
 * -- =============================================
 * -- Author     : Lau Pong
 * -- Create date: 29/2/2016
 * -- Description: FileExplorerActivity.java
 * -- =============================================
 * HISTORY OF UPDATE
 * <p/>
 * NO     DEVELOPER         DATETIME                      DESCRIPTION
 * *******************************************************************************
 * 1
 * 2
 */


public class FileExplorerActivity extends AppCompatActivity {
    private Toolbar mToolbar;
    private TextView mTxtToolbarTitle;
    private RecyclerView mRecyclerView;
    private FileExplorerAdapter mAdapter;
    private File file = Environment.getExternalStorageDirectory();
    private ArrayList<File> mFile = new ArrayList<>();
    private LinearLayoutManager mLinearLayoutManager;
    private int mFileLevel = 0;
    private ArrayList<File> mFileParentLevel = new ArrayList<>();
    private String mPathStorage = "storage";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_list);
        mToolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(mToolbar);
        mTxtToolbarTitle = (TextView) mToolbar.findViewById(R.id.txtTitle);
        mTxtToolbarTitle.setText(getResources().getString(R.string.title_activity_file_list));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_action_previous_item_dark);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        setFileParent();
        if (Utilities.localFileExists(file.getAbsolutePath())) {
            mFile = rearrangeFile(file);
            mAdapter = new FileExplorerAdapter(getBaseContext(), mFile);
            mRecyclerView.setAdapter(mAdapter);
            mFileParentLevel.add(file);
        }
        mLinearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        setTouchListener();
    }

    private void setFileParent() {
        while (file != null && file.getParentFile() != null) {
            if (file.getParentFile().getName().equals(mPathStorage)) {
                file = file.getParentFile();
                break;
            } else {
                file = file.getParentFile();
            }
        }
    }

    private ArrayList<File> rearrangeFile(File current) {
        File[] fileList = current.listFiles();
        ArrayList<File> folder = new ArrayList<>();
        ArrayList<File> file = new ArrayList<>();
        ArrayList<File> arrFile = new ArrayList<>();
        for (int i = 0; i < fileList.length; i++) {
            if (!fileList[i].isFile()) {
                folder.add(fileList[i]);
            } else {
                file.add(fileList[i]);
            }
        }
        for (int j = 0; j < folder.size(); j++) {
            arrFile.add(folder.get(j));
        }
        for (int k = 0; k < file.size(); k++) {
            arrFile.add(file.get(k));
        }
        return arrFile;
    }

    @Override
    public void onBackPressed() {
        if (mFileLevel > 0) {
            mFileParentLevel.remove(mFileLevel);
            mFileLevel--;
        } else {
            finish();
        }
        mFile = mAdapter.swapFile(rearrangeFile(mFileParentLevel.get(mFileLevel)));
    }

    private void setTouchListener() {
        ItemClickSupport.addTo(mRecyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView mRecyclerView, int position, View v) {
                TextView txtTitle = (TextView) v.findViewById(R.id.txtTitle);
                final File current = (File) txtTitle.getTag();
                if (current.isFile()) {
                    Intent intent = new Intent();
                    intent.putExtra("Attachment", current);
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    File[] list = current.listFiles();
                    if (list != null && list.length > 0) {
                        mFileLevel++;
                        mFileParentLevel.add(current);
                        mFile = mAdapter.swapFile(rearrangeFile(current));
                    } else {
                        mFileLevel++;
                        mFileParentLevel.add(null);
                        mAdapter.swapFile(null);
                    }
                }
            }
        });

        ItemClickSupport.addTo(mRecyclerView).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClicked(RecyclerView mRecyclerView, int position, View v) {
                return false;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_file_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            if (mFileLevel > 0) {
                mFileParentLevel.remove(mFileLevel);
                mFileLevel--;
            } else {
                File current = mFileParentLevel.get(mFileLevel);
                mFileParentLevel.clear();
                mFileParentLevel.add(current);
                mFileLevel = 0;
                finish();
            }
            mFile = mAdapter.swapFile(rearrangeFile(mFileParentLevel.get(mFileLevel)));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
