package com.applab.taskmgr.Image.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.applab.taskmgr.Image.adapter.ImageSlidingAdapter;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Utilities.Utilities;
import com.readystatesoftware.systembartint.SystemBarTintManager;

import java.io.File;
import java.util.ArrayList;

public class ImageSlidingActivity extends AppCompatActivity {
    private ImageSlidingAdapter adapter;
    private ViewPager mViewPager;
    private int mPosition;
    private TextView mTxtTitle;
    private LinearLayout mBtnSave;
    private int mSelectedPage;
    private ArrayList<String> mArrUrl = new ArrayList<>();
    private boolean mIsDownload = false;
    private String mStoreFolderLocation;
    private String mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_sliding);
        mTxtTitle = (TextView) findViewById(R.id.txtTitle);
        mBtnSave = (LinearLayout) findViewById(R.id.btnSave);
        mBtnSave.setVisibility(View.GONE);
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mPosition = getIntent().getIntExtra("position", 0);
        mArrUrl = getIntent().getStringArrayListExtra("url");
        mStoreFolderLocation = getIntent().getStringExtra("storeFolderLocation");
        mIsDownload = getIntent().getBooleanExtra("isDownload", false);
        adapter = new ImageSlidingAdapter(getSupportFragmentManager(), ImageSlidingActivity.this, mArrUrl);
        mViewPager.setAdapter(adapter);

        String[] result = mArrUrl.get(mPosition).split("/");
        mTxtTitle.setText(result[result.length - 1]);
        mBtnSave.setTag(mArrUrl.get(mPosition));

        mViewPager.setCurrentItem(mPosition);
        mSelectedPage = mPosition;
        mViewPager.addOnPageChangeListener(mViewPagerOnPageChangeListener);
        mBtnSave.setOnClickListener(mBtnSaveOnClickListener);
        SystemBarTintManager tintManager = new SystemBarTintManager(this);
        // enable status bar tint
        tintManager.setStatusBarTintEnabled(true);
        // enable navigation bar tint
        tintManager.setNavigationBarTintEnabled(true);
        // set a custom tint color for all system bars
        tintManager.setTintColor(Color.BLACK);
    }

    private View.OnClickListener mBtnSaveOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (Utilities.isConnectingToInternet(getBaseContext())) {
                Utilities.downloadFile(mBtnSave.getTag().toString(), ImageSlidingActivity.this, mStoreFolderLocation);
            } else {
                Utilities.showError(ImageSlidingActivity.this, Utilities.CODE_CONNECTION, Utilities.CONNECTION);
            }
        }
    };

    private ViewPager.OnPageChangeListener mViewPagerOnPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i2) {
        }

        @Override
        public void onPageSelected(int i) {
            String[] result = mArrUrl.get(i).split("/");
            mTitle = result[result.length - 1];
            mTxtTitle.setText(result[result.length - 1]);
            mBtnSave.setTag(mArrUrl.get(i));
            mSelectedPage = i;
        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
    };

    @Override
    public void onBackPressed() {
        finish();
    }

    public void handleAction(View v) {
        if (mTxtTitle.getVisibility() == View.GONE) {
            mTxtTitle.setVisibility(View.VISIBLE);
            mTxtTitle.setText(mTitle);
        } else {
            mTxtTitle.setVisibility(View.GONE);
        }
        if (mIsDownload) {
            if (mBtnSave.getVisibility() == View.GONE) {
                Utilities.expand(mBtnSave);
            } else {
                Utilities.collapse(mBtnSave);
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
