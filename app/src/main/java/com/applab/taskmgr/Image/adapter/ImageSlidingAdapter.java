package com.applab.taskmgr.Image.adapter;

/**
 * Created by user on 1/3/2016.
 */

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;

import com.applab.taskmgr.Image.fragment.ImageSlidingListFragment;
import com.applab.taskmgr.R;
import com.gesture_image_view.GestureImageView;

import java.util.ArrayList;

/**
 * Created by user on 6/10/2015.
 */
public class ImageSlidingAdapter extends FragmentStatePagerAdapter {
    private Activity mActivity;
    private ArrayList<String> mImagePaths = new ArrayList<>();

    // constructor
    public ImageSlidingAdapter(FragmentManager fm, Activity activity, ArrayList<String> imagePaths) {
        super(fm);
        mActivity = activity;
        mImagePaths = imagePaths;
    }

    @Override
    public Fragment getItem(int position) {
        return ImageSlidingListFragment.newInstance(mImagePaths.get(position));
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return this.mImagePaths.size();
    }
}
