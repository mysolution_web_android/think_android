package com.applab.taskmgr.AllMessage.adapter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.applab.taskmgr.AllMessage.holder.MessageHolder;
import com.applab.taskmgr.AllMessage.model.Message;
import com.applab.taskmgr.AssignTo.holder.AssignedToHolder;
import com.applab.taskmgr.Image.activity.ImageSlidingActivity;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Utilities.CircleTransform;
import com.applab.taskmgr.Utilities.Utilities;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

/**
 * Created by user on 1/3/2016.
 */
public class MessageAdapter extends RecyclerView.Adapter<MessageHolder> {
    private Context mContext;
    private LayoutInflater layoutInflater;
    private Cursor mCursor;

    public MessageAdapter(Context context, Cursor cursor) {
        this.mContext = context;
        this.mCursor = cursor;
        this.layoutInflater = LayoutInflater.from(mContext);
    }

    @Override
    public MessageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.custom_message_task_row, parent, false);
        return new MessageHolder(view);
    }

    @Override
    public void onBindViewHolder(final MessageHolder holder, final int position) {
        if (mCursor != null) {
            Message current = Message.getMessage(mCursor, position);

            Glide.with(mContext)
                    .load(current.getFromPhoto())
                    .placeholder(R.mipmap.ic_action_person_light)
                    .transform(new CircleTransform(mContext))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.getImgProfile());
            holder.getTxtDateTime().setText(Utilities.setToLocalDateTime(Utilities.DATE_FORMAT,
                    "dd MMM yyyy 'at' h:mm a", current.getCreateDate()));
            holder.getTxtMessage().setText(current.getMessage());
            holder.getTxtName().setText(current.getFromName());
            if (current.getFile() != null) {
                holder.getImgAttachement().setVisibility(View.VISIBLE);
                Glide.with(mContext)
                        .load(current.getFile())
                        .placeholder(R.drawable.empty_photo)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(holder.getImgAttachement());
            } else {
                holder.getImgAttachement().setVisibility(View.GONE);
            }

            holder.getRlProfile().setTag(current);
            holder.getRlAttachment().setTag(current);
            holder.getRlAttachment().setOnClickListener(mImgAttachmenrOnClickListener);
            holder.getRlProfile().setOnClickListener(mImgProfileOnClickListener);
        }
    }

    private View.OnClickListener mImgProfileOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            RelativeLayout img = (RelativeLayout) v.findViewById(R.id.rlProfile);
            Message message = (Message) img.getTag();
            Intent intent = new Intent(mContext, ImageSlidingActivity.class);
            intent.putExtra("position", 0);
            ArrayList<String> arr = new ArrayList<>();
            arr.add(message.getFromPhoto());
            intent.putExtra("url", arr);
            mContext.startActivity(intent);
        }
    };

    private View.OnClickListener mImgAttachmenrOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            RelativeLayout img = (RelativeLayout) v.findViewById(R.id.rlAttachment);
            Message message = (Message) img.getTag();
            Intent intent = new Intent(mContext, ImageSlidingActivity.class);
            intent.putExtra("position", 0);
            ArrayList<String> arr = new ArrayList<>();
            arr.add(message.getFile());
            intent.putExtra("url", arr);
            intent.putExtra("storeFolderLocation", mContext.getString(R.string.message_image) + "/" + message.getTaskId());
            mContext.startActivity(intent);
        }
    };

    @Override
    public int getItemCount() {
        return mCursor != null ? mCursor.getCount() : 0;
    }

    public Cursor swapCursor(Cursor cursor) {
        if (this.mCursor == cursor) {
            return null;
        }

        Cursor oldCursor = this.mCursor;
        this.mCursor = cursor;
        if (cursor != null) {
            notifyDataSetChanged();
        }

        return oldCursor;
    }
}
