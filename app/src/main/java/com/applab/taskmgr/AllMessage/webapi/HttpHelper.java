package com.applab.taskmgr.AllMessage.webapi;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.applab.taskmgr.AllMessage.model.Message;
import com.applab.taskmgr.AllMessage.provider.TaskMessageProvider;
import com.applab.taskmgr.Login.database.CRUDHelper;
import com.applab.taskmgr.Login.model.Token;
import com.applab.taskmgr.Login.provider.MessageProvider;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Task.model.Task;
import com.applab.taskmgr.Utilities.AppController;
import com.applab.taskmgr.Utilities.DBHelper;
import com.applab.taskmgr.Utilities.GsonRequest;
import com.applab.taskmgr.Utilities.Utilities;

import java.util.HashMap;

/**
 * Created by user on 1/3/2016.
 */
public class HttpHelper {
    public static void getMesssage(Context context, String TAG, Task task) {
        final Token token = CRUDHelper.getToken(context);
        if (token != null) {
            if (token.getToken() != null) {
                HashMap<String, String> header = new HashMap<String, String>();
                header.put("Authorization", token.getToken());
                GsonRequest<Message[]> mGsonRequest = new GsonRequest<Message[]>(
                        Request.Method.GET,
                        context.getString(R.string.base_url) + "Task/GetMessage?task_id=" + Utilities.encode(task.getTaskId()),
                        Message[].class,
                        header,
                        responseMessageListener(context, TAG, task),
                        errorMessageListener(context, TAG)
                ) {
                };

                mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utilities.TIMEOUT,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
            }
        }

    }

    public static Response.Listener<Message[]> responseMessageListener(final Context context, final String TAG, final Task task) {
        return new Response.Listener<Message[]>() {

            @Override
            public void onResponse(Message[] response) {
                if (response != null) {
                    if (response.length > 0) {
                        Log.i(TAG, "Succesfully entered data : " + response);
                        context.getContentResolver().delete(TaskMessageProvider.CONTENT_URI, DBHelper.TASK_MESSAGE_COLUMN_TASK_ID + "=?", new String[]{task.getTaskId().toString()});
                        com.applab.taskmgr.AllMessage.database.CRUDHelper.insertMessage(context, TAG, response, task);
                    }
                }
                Intent intent = new Intent(TAG);
                intent.putExtra("success", true);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }
        };
    }

    public static Response.ErrorListener errorMessageListener(final Context context, final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Utilities.serverHandlingError(context, error);
                Intent intent = new Intent(TAG);
                intent.putExtra("failed", true);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }
        };
    }
}
