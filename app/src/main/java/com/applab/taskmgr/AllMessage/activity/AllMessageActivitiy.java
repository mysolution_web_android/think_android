package com.applab.taskmgr.AllMessage.activity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import com.applab.taskmgr.AllMessage.adapter.MessageAdapter;
import com.applab.taskmgr.AllMessage.provider.TaskMessageProvider;
import com.applab.taskmgr.AllMessage.webapi.HttpHelper;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Task.model.Task;
import com.applab.taskmgr.Utilities.DBHelper;

public class AllMessageActivitiy extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    private MessageAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView mRecyclerView;
    private static final String TAG = AllMessageActivitiy.class.getSimpleName();
    private int mLoaderId = 3359;
    private Cursor mCursor;
    private Task mTask;
    private Toolbar mToolbar;
    private TextView mTxtToolbarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_message_activitiy);
        mToolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(mToolbar);
        mTxtToolbarTitle = (TextView) mToolbar.findViewById(R.id.txtTitle);
        mTxtToolbarTitle.setText(getString(R.string.title_activity_message));
        mToolbar.setNavigationIcon(R.mipmap.back);
        mToolbar.setNavigationOnClickListener(mToolbarOnClickListener);

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);

        mAdapter = new MessageAdapter(this, mCursor);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(mLayoutManager);
        setIntent(getIntent());
    }

    public void setIntent(Intent intent) {
        if (intent.getParcelableExtra("Task") != null) {
            mTask = intent.getParcelableExtra("Task");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getSupportLoaderManager().initLoader(mLoaderId, null, this);
        HttpHelper.getMesssage(AllMessageActivitiy.this, TAG, mTask);
    }

    private View.OnClickListener mToolbarOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_message, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (mLoaderId == id) {
            return new CursorLoader(AllMessageActivitiy.this, TaskMessageProvider.CONTENT_URI, null,
                    DBHelper.TASK_MESSAGE_COLUMN_TASK_ID + "=?", new String[]{mTask.getTaskId().toString()}, null);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (mLoaderId == loader.getId()) {
            if (data != null) {
                mCursor = data;
                mAdapter.swapCursor(data);
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
