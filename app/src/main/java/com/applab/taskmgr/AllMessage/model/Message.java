package com.applab.taskmgr.AllMessage.model;

/**
 * Created by user on 1/3/2016.
 */

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.applab.taskmgr.Utilities.DBHelper;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Message implements Parcelable {

    @SerializedName("FromId")
    @Expose
    private String FromId;
    @SerializedName("FromName")
    @Expose
    private String FromName;
    @SerializedName("FromPhoto")
    @Expose
    private String FromPhoto;
    @SerializedName("Message")
    @Expose
    private String Message;
    @SerializedName("File")
    @Expose
    private String File;
    @SerializedName("CreateDate")
    @Expose
    private String CreateDate;

    private int id;
    private String taskId;
    private String storeLocation;

    public String getStoreLocation() {
        return storeLocation;
    }

    public void setStoreLocation(String storeLocation) {
        this.storeLocation = storeLocation;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    /**
     * @return The FromId
     */
    public String getFromId() {
        return FromId;
    }

    /**
     * @param FromId The FromId
     */
    public void setFromId(String FromId) {
        this.FromId = FromId;
    }

    /**
     * @return The FromName
     */
    public String getFromName() {
        return FromName;
    }

    /**
     * @param FromName The FromName
     */
    public void setFromName(String FromName) {
        this.FromName = FromName;
    }

    /**
     * @return The FromPhoto
     */
    public String getFromPhoto() {
        return FromPhoto;
    }

    /**
     * @param FromPhoto The FromPhoto
     */
    public void setFromPhoto(String FromPhoto) {
        this.FromPhoto = FromPhoto;
    }

    /**
     * @return The Message
     */
    public String getMessage() {
        return Message;
    }

    /**
     * @param Message The Message
     */
    public void setMessage(String Message) {
        this.Message = Message;
    }

    /**
     * @return The File
     */
    public String getFile() {
        return File;
    }

    /**
     * @param File The File
     */
    public void setFile(String File) {
        this.File = File;
    }

    /**
     * @return The CreateDate
     */
    public String getCreateDate() {
        return CreateDate;
    }

    /**
     * @param CreateDate The CreateDate
     */
    public void setCreateDate(String CreateDate) {
        this.CreateDate = CreateDate;
    }

    public Message(Parcel in) {
        readFromParcel(in);
    }

    /**
     * Called from the constructor to create this
     * object from a parcel.
     *
     * @param in parcel from which to re-create object.
     */
    public void readFromParcel(Parcel in) {
        this.FromId = in.readString();
        this.FromName = in.readString();
        this.FromPhoto = in.readString();
        this.Message = in.readString();
        this.File = in.readString();
        this.CreateDate = in.readString();
        this.id = in.readInt();
        this.taskId = in.readString();
        this.storeLocation = in.readString();

    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.FromId);
        dest.writeString(this.FromName);
        dest.writeString(this.FromPhoto);
        dest.writeString(this.Message);
        dest.writeString(this.File);
        dest.writeString(this.CreateDate);
        dest.writeInt(this.id);
        dest.writeString(this.taskId);
        dest.writeString(this.storeLocation);

    }

    @SuppressWarnings("unchecked")
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public Message createFromParcel(Parcel in) {
            return new Message(in);
        }

        @Override
        public Message[] newArray(int size) {
            return new Message[size];
        }
    };

    public Message() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static Message getMessage(Cursor cursor, int position) {
        cursor.moveToPosition(position);
        Message message = new Message();
        message.setCreateDate(cursor.getString(cursor.getColumnIndex(DBHelper.TASK_MESSAGE_COLUMN_CREATE_DATE)));
        message.setFile(cursor.getString(cursor.getColumnIndex(DBHelper.TASK_MESSAGE_COLUMN_FILE)));
        message.setFromId(cursor.getString(cursor.getColumnIndex(DBHelper.TASK_MESSAGE_COLUMN_FROM_ID)));
        message.setFromName(cursor.getString(cursor.getColumnIndex(DBHelper.TASK_MESSAGE_COLUMN_FROM_NAME)));
        message.setFromPhoto(cursor.getString(cursor.getColumnIndex(DBHelper.TASK_MESSAGE_COLUMN_FROM_PHOTO)));
        message.setMessage(cursor.getString(cursor.getColumnIndex(DBHelper.TASK_MESSAGE_COLUMN_MESSAGE)));
        message.setId(cursor.getInt(cursor.getColumnIndex(DBHelper.TASK_MESSAGE_COLUMN_ID)));
        message.setTaskId(cursor.getString(cursor.getColumnIndex(DBHelper.TASK_MESSAGE_COLUMN_TASK_ID)));
        message.setStoreLocation(cursor.getString(cursor.getColumnIndex(DBHelper.TASK_MESSAGE_COLUMN_STORE_LOCATION)));
        return message;
    }


}