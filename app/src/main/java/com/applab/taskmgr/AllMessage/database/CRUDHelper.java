package com.applab.taskmgr.AllMessage.database;

import android.content.ContentValues;
import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.applab.taskmgr.AllMessage.model.Message;
import com.applab.taskmgr.AllMessage.provider.TaskMessageProvider;
import com.applab.taskmgr.Login.provider.MessageProvider;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Task.model.Task;
import com.applab.taskmgr.Utilities.DBHelper;

import java.io.File;

/**
 * Created by user on 1/3/2016.
 */
public class CRUDHelper {
    public static void insertMessage(Context context, String TAG, Message[] message, Task task) {
        DBHelper helper = new DBHelper(context);
        try {
            ContentValues[] contentValueses = new ContentValues[message.length];
            for (int i = 0; i < message.length; i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.TASK_MESSAGE_COLUMN_CREATE_DATE, message[i].getCreateDate());
                contentValues.put(DBHelper.TASK_MESSAGE_COLUMN_FILE, message[i].getFile());
                contentValues.put(DBHelper.TASK_MESSAGE_COLUMN_FROM_ID, message[i].getFromId());
                contentValues.put(DBHelper.TASK_MESSAGE_COLUMN_FROM_NAME, message[i].getFromName());
                contentValues.put(DBHelper.TASK_MESSAGE_COLUMN_FROM_PHOTO, message[i].getFromPhoto());
                contentValues.put(DBHelper.TASK_MESSAGE_COLUMN_MESSAGE, message[i].getMessage());
                contentValues.put(DBHelper.TASK_MESSAGE_COLUMN_TASK_ID, task.getTaskId());
                if (message[i].getFile() != null)
                    contentValues.put(DBHelper.TASK_MESSAGE_COLUMN_STORE_LOCATION, new File(Environment.getExternalStorageDirectory() + "/" + context.getResources().getString(R.string.folder_name) + "/" +
                            context.getString(R.string.message_image)).getAbsolutePath() + "/" + task.getTaskId().toString() + message[i].getFile().split("/")[message[i].getFile().split("/").length - 1]);
                contentValueses[i] = contentValues;
            }

            boolean isEmpty = true;
            for (ContentValues contentValues : contentValueses) {
                if (contentValues != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                context.getContentResolver().bulkInsert(TaskMessageProvider.CONTENT_URI, contentValueses);
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Message Error :" + ex.toString());
        }
    }
}
