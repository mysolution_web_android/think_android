package com.applab.taskmgr.AllMessage.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applab.taskmgr.R;

/**
 * Created by user on 26-Feb-16.
 */
public class MessageHolder extends RecyclerView.ViewHolder {

    private ImageView imgProfile;
    private ImageView imgAttachement;
    private TextView txtMessage;
    private TextView txtDateTime;
    private TextView txtName;

    private RelativeLayout rlProfile, rlAttachment;

    public MessageHolder(View itemView) {
        super(itemView);
        imgAttachement = (ImageView) itemView.findViewById(R.id.imgAttachement);
        imgProfile = (ImageView) itemView.findViewById(R.id.imgProfile);
        txtMessage = (TextView) itemView.findViewById(R.id.txtMessage);
        txtDateTime = (TextView) itemView.findViewById(R.id.txtDateTime);
        txtName = (TextView) itemView.findViewById(R.id.txtName);
        rlProfile = (RelativeLayout) itemView.findViewById(R.id.rlProfile);
        rlAttachment = (RelativeLayout) itemView.findViewById(R.id.rlAttachment);
    }

    public RelativeLayout getRlProfile() {
        return rlProfile;
    }

    public RelativeLayout getRlAttachment() {
        return rlAttachment;
    }

    public TextView getTxtName() {
        return txtName;
    }

    public TextView getTxtDateTime() {
        return txtDateTime;
    }

    public TextView getTxtMessage() {
        return txtMessage;
    }

    public ImageView getImgAttachement() {
        return imgAttachement;
    }

    public ImageView getImgProfile() {
        return imgProfile;
    }
}
