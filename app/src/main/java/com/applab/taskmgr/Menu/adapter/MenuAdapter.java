package com.applab.taskmgr.Menu.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.applab.taskmgr.ChangePassword.activity.ChangePasswordActivity;
import com.applab.taskmgr.Login.activity.login.LoginPage;
import com.applab.taskmgr.Menu.holder.MenuViewHolder;
import com.applab.taskmgr.Menu.model.Menu;
import com.applab.taskmgr.Profile.activity.MyProfileActivity;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Task.activity.TaskManagerActivity;

/**
 * -- =============================================
 * -- Author     : Vicknesh Balasubramaniam
 * -- Create date: 19/1/2016
 * -- Description: AssigneeList .java
 * -- =============================================
 * HISTORY OF UPDATE
 * <p/>
 * NO     DEVELOPER         DATETIME                      DESCRIPTION
 * *******************************************************************************
 * 1
 * 2
 */

public class MenuAdapter extends RecyclerView.Adapter<MenuViewHolder> {
    private LayoutInflater mInflater;
    private Cursor mCursor;
    private Context mContext;
    public DrawerLayout mDrawerLayout;
    public Menu menu;
    public int mSelectedPosition;
    private int page = 0;
    private int page2 = 1;
    private int page3 = 2;
    private int page4 = 3;
    private int page5 = 4;

    public MenuAdapter(Context context, Cursor cursor, int selectedPosition, DrawerLayout drawerLayout) {
        this.mInflater = LayoutInflater.from(context);
        this.mCursor = cursor;
        this.mContext = context;
        this.mSelectedPosition = selectedPosition;
        this.mDrawerLayout = drawerLayout;
    }

    @Override
    public MenuViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.custom_menu_row, parent, false);
        MenuViewHolder holder = new MenuViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MenuViewHolder holder, int position) {
        Menu current = Menu.getMenu(mCursor, position, mSelectedPosition);
        holder.getmTxtTitle().setText(current.getTitle());
        if (current.getTitle().equals(mContext.getString(R.string.title_activity_my_task))) {
            holder.getmImgIcon().setImageResource(R.mipmap.my_task);
            holder.getmTxtTitle().setText(R.string.title_activity_my_task);
            holder.getmTtxtNotification().setVisibility(View.GONE);
        } else if (current.getTitle().equals(mContext.getString(R.string.title_activity_new_task))) {
            holder.getmImgIcon().setImageResource(R.mipmap.new_task);
            holder.getmTxtTitle().setText(R.string.title_activity_new_task);
            holder.getmTtxtNotification().setVisibility(View.VISIBLE);
            if (current.getNo() == 0) {
                holder.getmTtxtNotification().setVisibility(View.GONE);
            } else if (current.getNo() > 9) {
                holder.getmTtxtNotification().setText("+9");
            } else {
                holder.getmTtxtNotification().setText(String.valueOf(current.getNo()));
            }
        } else if (current.getTitle().equals(mContext.getString(R.string.title_activity_completed_task))) {
            holder.getmImgIcon().setImageResource(R.mipmap.completed_task);
            holder.getmTxtTitle().setText(R.string.title_activity_completed_task);
            holder.getmTtxtNotification().setVisibility(View.GONE);
        } else if (current.getTitle().equals(mContext.getString(R.string.title_activity_canceled_task))) {
            holder.getmImgIcon().setImageResource(R.mipmap.cancelled_task);
            holder.getmTxtTitle().setText(R.string.title_activity_canceled_task);
            holder.getmTtxtNotification().setVisibility(View.GONE);
        } else if (current.getTitle().equals(mContext.getString(R.string.title_activity_created_task))) {
            holder.getmImgIcon().setImageResource(R.mipmap.created_task);
            holder.getmTxtTitle().setText(R.string.title_activity_created_task);
            holder.getmTtxtNotification().setVisibility(View.GONE);
        } else if (current.getTitle().equals(mContext.getString(R.string.title_activity_profile))) {
            holder.getmImgIcon().setImageResource(R.mipmap.profile);
            holder.getmTxtTitle().setText(R.string.title_activity_profile);
            holder.getmTtxtNotification().setVisibility(View.GONE);
        } else if (current.getTitle().equals(mContext.getString(R.string.title_activity_change_password))) {
            holder.getmImgIcon().setImageResource(R.mipmap.setting);
            holder.getmTxtTitle().setText(R.string.title_activity_change_password);
            holder.getmTtxtNotification().setVisibility(View.GONE);
        } else if (current.getTitle().equals(mContext.getString(R.string.title_activity_sign_out))) {
            holder.getmImgIcon().setImageResource(R.mipmap.sign_out);
            holder.getmTxtTitle().setText(R.string.title_activity_sign_out);
            holder.getmTtxtNotification().setVisibility(View.GONE);
        }
        if (current.getIsSelected()) {
            holder.getmRl().setBackgroundColor(ContextCompat.getColor(mContext, R.color.color_highlight));
        }
        /*if (current.getNo() > 0) {
            holder.getmTtxtNotification().setVisibility(View.VISIBLE);
            if (current.getNo() > 9) {
                holder.getmTtxtNotification().setText("+9");
            } else {
                holder.getmTtxtNotification().setText(String.valueOf(current.getNo()));
            }
        } else {
            holder.getmTtxtNotification().setVisibility(View.GONE);
        }*/
        setWholeOnClickListener(position, holder.getmRl());
    }

    public void menuNavigation(int position) {

        if (mSelectedPosition != position) {
            switch (position) {
                /*case 0:
                    NavUtils.navigateUpFromSameTask((Activity) mContext);
                    break;*/
                case 0:
                    if (!mContext.getClass().getSimpleName().equals(TaskManagerActivity.class.getSimpleName())) {
                        Intent intentSurvey = new Intent(mContext, TaskManagerActivity.class);
                        intentSurvey.putExtra("ARG", page);
                        mContext.startActivity(intentSurvey);
                        ((Activity) mContext).finish();
                    } else {
                        Intent intentSurvey = new Intent(TaskManagerActivity.class.getSimpleName());
                        intentSurvey.putExtra("ARG", page);
                        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intentSurvey);
                    }
                    break;
                case 1:
                    if (!mContext.getClass().getSimpleName().equals(TaskManagerActivity.class.getSimpleName())) {
                        Intent intentSurvey = new Intent(mContext, TaskManagerActivity.class);
                        intentSurvey.putExtra("ARG", page2);
                        mContext.startActivity(intentSurvey);
                        ((Activity) mContext).finish();
                    } else {
                        Intent intentSurvey = new Intent(TaskManagerActivity.class.getSimpleName());
                        intentSurvey.putExtra("ARG", page2);
                        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intentSurvey);
                    }
                    break;
                case 2:
                    if (!mContext.getClass().getSimpleName().equals(TaskManagerActivity.class.getSimpleName())) {
                        Intent intentSurvey = new Intent(mContext, TaskManagerActivity.class);
                        intentSurvey.putExtra("ARG", page3);
                        mContext.startActivity(intentSurvey);
                        ((Activity) mContext).finish();
                    } else {
                        Intent intentSurvey = new Intent(TaskManagerActivity.class.getSimpleName());
                        intentSurvey.putExtra("ARG", page3);
                        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intentSurvey);
                    }
                    break;
                case 3:
                    if (!mContext.getClass().getSimpleName().equals(TaskManagerActivity.class.getSimpleName())) {
                        Intent intentSurvey = new Intent(mContext, TaskManagerActivity.class);
                        intentSurvey.putExtra("ARG", page4);
                        mContext.startActivity(intentSurvey);
                        ((Activity) mContext).finish();
                    } else {
                        Intent intentSurvey = new Intent(TaskManagerActivity.class.getSimpleName());
                        intentSurvey.putExtra("ARG", page4);
                        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intentSurvey);
                    }
                    break;
                case 4:
                    if (!mContext.getClass().getSimpleName().equals(TaskManagerActivity.class.getSimpleName())) {
                        Intent intentSurvey = new Intent(mContext, TaskManagerActivity.class);
                        intentSurvey.putExtra("ARG", page5);
                        mContext.startActivity(intentSurvey);
                        ((Activity) mContext).finish();
                    } else {
                        Intent intentSurvey = new Intent(TaskManagerActivity.class.getSimpleName());
                        intentSurvey.putExtra("ARG", page5);
                        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intentSurvey);
                    }
                    break;
                case 5:
                    mContext.startActivity(new Intent(mContext, MyProfileActivity.class));
                    if (!mContext.getClass().getSimpleName().equals(TaskManagerActivity.class.getSimpleName())) {
                        ((Activity) mContext).finish();
                    }
                    break;
                case 6:
                    mContext.startActivity(new Intent(mContext, ChangePasswordActivity.class));
                    if (!mContext.getClass().getSimpleName().equals(TaskManagerActivity.class.getSimpleName())) {
                        ((Activity) mContext).finish();
                    }
                    break;
                case 7:
                    mContext.startActivity(new Intent(mContext, LoginPage.class));
                    Intent intent = new Intent(new Intent(mContext, LoginPage.class));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    mContext.startActivity(intent);
                    break;
                default:
                    Toast.makeText(mContext, "onClick " + position, Toast.LENGTH_SHORT).show();
            }
        }
        mDrawerLayout = (DrawerLayout) ((Activity) mContext).findViewById(R.id.drawer_layout);
        mDrawerLayout.closeDrawers();
    }

    private void setWholeOnClickListener(final int positon, final RelativeLayout whole) {
        whole.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuNavigation(positon);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mCursor == null ? 0 : mCursor.getCount();
    }

    public Cursor swapCursor(Cursor cursor) {
        if (this.mCursor == cursor) {
            return null;
        }
        Cursor oldCursor = this.mCursor;
        this.mCursor = cursor;
        if (cursor != null) {
            android.os.Message msg = mHandler.obtainMessage();
            mHandler.handleMessage(msg);
        }
        return oldCursor;
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            MenuAdapter.this.notifyDataSetChanged();
        }
    };
}
