package com.applab.taskmgr.Menu.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.applab.taskmgr.Menu.model.Menu;
import com.applab.taskmgr.Menu.provider.MenuProvider;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Utilities.DBHelper;

/**
 * -- =============================================
 * -- Author     : Vicknesh Balasubramaniam
 * -- Create date: 19/1/2016
 * -- Description: AssigneeList .java
 * -- =============================================
 * HISTORY OF UPDATE
 * <p/>
 * NO     DEVELOPER         DATETIME                      DESCRIPTION
 * *******************************************************************************
 * 1
 * 2
 */
public class MenuDBHelper {
    public static void insertDashboard(Context context) {
        if (getTotalDashboardRow(context) == 0) {
            ContentValues[] contentValueses = new ContentValues[9];
            String[] title = {
                    context.getString(R.string.title_activity_my_task),
                    context.getString(R.string.title_activity_new_task),
                    context.getString(R.string.title_activity_completed_task),
                    context.getString(R.string.title_activity_canceled_task),
                    context.getString(R.string.title_activity_created_task),
                    context.getString(R.string.title_activity_profile),
                    context.getString(R.string.title_activity_change_password),
                    context.getString(R.string.title_activity_sign_out)
            };
            for (int i = 0; i < 8; i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.MENU_COLUMN_TITLE, title[i]);
                contentValues.put(DBHelper.MENU_COLUMN_NO, 0);
                contentValueses[i] = contentValues;
            }
            context.getContentResolver().bulkInsert(MenuProvider.CONTENT_URI, contentValueses);
        }
    }

    public static int getTotalDashboardRow(Context context) {
        Cursor cursor = null;
        int total = 0;
        try {
            cursor = context.getContentResolver().query(MenuProvider.CONTENT_URI,
                    null, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                total = cursor.getCount();
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return total;
    }

    public static void insertTaskCount(Context context, String TAG, Menu menu) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(DBHelper.MENU_COLUMN_NO, menu.getNewTaskCount());
            context.getContentResolver().update(MenuProvider.CONTENT_URI, contentValues, DBHelper.MENU_COLUMN_TITLE + "=?", new String[]{context.getString(R.string.title_new_task)});
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Download Error: " + ex.toString());
        }
    }
}
