package com.applab.taskmgr.Menu.webapi;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.applab.taskmgr.Login.database.CRUDHelper;
import com.applab.taskmgr.Login.model.Token;
import com.applab.taskmgr.Menu.database.MenuDBHelper;
import com.applab.taskmgr.Menu.model.Menu;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Utilities.AppController;
import com.applab.taskmgr.Utilities.GsonRequest;
import com.applab.taskmgr.Utilities.Utilities;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by user on 11-Mar-16.
 */
public class HttpHelper {
    public static void getNewTaskCount(Context context, String TAG) {
        final Token token = CRUDHelper.getToken(context);
        if (token != null) {
            if (token.getToken() != null) {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", token.getToken());
                GsonRequest<Menu> mGsonRequest = new GsonRequest<Menu>(
                        Request.Method.GET,
                        context.getString(R.string.base_url) + "Count/GetNewTaskCount",
                        Menu.class,
                        params,
                        responseListener(context, TAG),
                        errorListener(context, TAG)) {
                };
                mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utilities.TIMEOUT,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
            }
        }
    }

    public static Response.Listener<Menu> responseListener(final Context context, final String TAG) {
        return new Response.Listener<Menu>() {
            @Override
            public void onResponse(Menu response) {
                if (response != null) {
                    MenuDBHelper.insertTaskCount(context, TAG, response);
                }
            }
        };
    }

    public static Response.ErrorListener errorListener(final Context context, final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Intent intent = new Intent(TAG);
                intent.putExtra("failed", true);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                Utilities.serverHandlingError(context, error);
            }
        };
    }
}
