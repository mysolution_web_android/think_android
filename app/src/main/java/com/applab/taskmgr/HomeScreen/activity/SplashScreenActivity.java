package com.applab.taskmgr.HomeScreen.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.applab.taskmgr.Login.activity.login.LoginPage;
import com.applab.taskmgr.Login.database.CRUDHelper;
import com.applab.taskmgr.Login.model.Token;
import com.applab.taskmgr.Login.provider.TokenProvider;
import com.applab.taskmgr.Profile.provider.ProfileProvider;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Task.activity.TaskManagerActivity;
import com.applab.taskmgr.Utilities.CustomPushBotHandler;
import com.applab.taskmgr.Utilities.Utilities;
import com.pushbots.push.Pushbots;

import java.util.Date;

public class SplashScreenActivity extends AppCompatActivity {

    Handler timeHandler = new Handler();
    private Token token;

    @Override
    protected void onResume() {
        super.onResume();
        Utilities.setIsActivityOpen(true, SplashScreenActivity.this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Utilities.setIsActivityOpen(false, SplashScreenActivity.this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (!Utilities.isNotPermitted(this)) {
            setUpHomeScreen();
        }
    }

    private void setUpHomeScreen(){
        Pushbots.sharedInstance().init(this);
        if (Pushbots.sharedInstance() != null) {
            if (Pushbots.sharedInstance().isInitialized()) {
                Pushbots.sharedInstance().setCustomHandler(CustomPushBotHandler.class);
            }
        }

        token = CRUDHelper.getToken(SplashScreenActivity.this);

        timeHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (token != null) {
                    if (new Date().before(Utilities.setCalendarDate(Utilities.DATE_FORMAT,
                            Utilities.setToLocalDateTime(Utilities.DATE_FORMAT, Utilities.DATE_FORMAT, token.getExpireDate())))) {
                        Intent intent = new Intent(SplashScreenActivity.this, TaskManagerActivity.class);
                        startActivity(intent);
                    } else {
                        getContentResolver().delete(ProfileProvider.CONTENT_URI, null, null);
                        getContentResolver().delete(TokenProvider.CONTENT_URI, null, null);
                        Intent intent = new Intent(SplashScreenActivity.this, LoginPage.class);
                        startActivity(intent);
                    }
                } else {
                    Intent intent = new Intent(SplashScreenActivity.this, LoginPage.class);
                    startActivity(intent);
                }
            }
        }, 1000);
    }
}
