package com.applab.taskmgr.Login.activity.login;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.applab.taskmgr.Dashboard.activity.DashboardActivity;
import com.applab.taskmgr.Login.activity.forgetpassword.ForgetPasswordActivity;
import com.applab.taskmgr.Login.webapi.HttpHelper;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Utilities.DBHelper;
import com.applab.taskmgr.Utilities.Utilities;

import java.util.HashMap;
import java.util.Map;


public class LoginPage extends AppCompatActivity {

    private Button btnForgetPassword;
    private Button btnLogin;
    private Button btnSignUp;
    private EditText editEmail;
    private EditText editPassword;
    private String TAG = LoginPage.class.getSimpleName();
    private ProgressBar mProgressBar;
    private RelativeLayout mRl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);

        btnForgetPassword = (Button) findViewById(R.id.forgetPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnSignUp = (Button) findViewById(R.id.btnSignUp);
        editEmail = (EditText) findViewById(R.id.editEmail);
        editPassword = (EditText) findViewById(R.id.editPassword);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mRl = (RelativeLayout) findViewById(R.id.fadeRL);

        btnForgetPassword.setOnClickListener(mBtnForgetPasswordOnClickListener);
        btnLogin.setOnClickListener(mBtnLoginOnClickListener);
        btnSignUp.setOnClickListener(mBtnSignUpOnClickListener);
    }

    private View.OnClickListener mBtnSignUpOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Utilities.openWebSite(LoginPage.this, getString(R.string.sign_up_url));
        }
    };

    private View.OnClickListener mBtnForgetPasswordOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(LoginPage.this, ForgetPasswordActivity.class);
            startActivity(intent);
        }
    };

    private View.OnClickListener mBtnLoginOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final String email = editEmail.getText().toString().trim();
            final String password = editPassword.getText().toString().trim();
            if (editEmail.getText().toString().trim().length() == 0) {
                Utilities.showError(LoginPage.this, Utilities.CODE_EMAIL_FIELD, Utilities.EMAIL_FIELD);
            } else if (editPassword.getText().toString().trim().length() == 0) {
                Utilities.showError(LoginPage.this, Utilities.CODE_PASSWORD_FIELD, Utilities.PASSWORD_FIELD);
            } else {
                HttpHelper.loginApi(LoginPage.this, TAG, email, password);
                Utilities.hideKeyboard(LoginPage.this);
                Utilities.setFadeProgressBarVisibility(true, mRl, mProgressBar);
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        DBHelper dbHelper = new DBHelper(this);
        dbHelper.clearData();
        IntentFilter iff = new IntentFilter(TAG);
        LocalBroadcastManager.getInstance(getBaseContext()).registerReceiver(broadcastReceiver, iff);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getBaseContext()).unregisterReceiver(broadcastReceiver);
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(TAG)) {
                if (intent.getBooleanExtra("failed", false)) {
                    Utilities.setFadeProgressBarVisibility(false, mRl, mProgressBar);
                }
            }
        }
    };

    @Override
    public void onBackPressed() {
        finish();
        moveTaskToBack(true);
    }
}
