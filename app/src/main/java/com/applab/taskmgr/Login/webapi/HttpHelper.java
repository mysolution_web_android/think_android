package com.applab.taskmgr.Login.webapi;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.applab.taskmgr.Login.activity.login.LoginPage;
import com.applab.taskmgr.Login.database.CRUDHelper;
import com.applab.taskmgr.Login.model.Token;
import com.applab.taskmgr.Login.provider.TokenProvider;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Task.activity.TaskManagerActivity;
import com.applab.taskmgr.Utilities.AppController;
import com.applab.taskmgr.Utilities.GsonRequest;
import com.applab.taskmgr.Utilities.Utilities;

/**
 * -- =============================================
 * -- Author     : Vicknesh Balasubramaniam
 * -- Create date: 5/2/2016
 * -- Description: AssigneeList .java
 * -- =============================================
 * HISTORY OF UPDATE
 * <p/>
 * NO     DEVELOPER         DATETIME                      DESCRIPTION
 * *******************************************************************************
 * 1
 * 2
 */
public class HttpHelper {
    //region task region
    public static void loginApi(Context context, String TAG, final String username, final String password) {
        GsonRequest<Token> mGsonRequest = new GsonRequest<Token>(
                Request.Method.POST,
                context.getString(R.string.base_url) + "Login",
                Token.class,
                null,
                responseLoginListener(context, TAG),
                errorLoginListener(context, TAG)) {


            @Override
            public byte[] getBody() throws AuthFailureError {
                String httpbody = "username=" + username + "&password=" + password;
                return httpbody.getBytes();
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utilities.TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<Token> responseLoginListener(final Context context, final String TAG) {
        return new Response.Listener<Token>() {
            @Override
            public void onResponse(Token response) {
                context.getContentResolver().delete(TokenProvider.CONTENT_URI, null, null);
                CRUDHelper.insertToken(context, response);
                com.applab.taskmgr.Profile.webapi.HttpHelper.profileApi(context, TAG, null);
                Intent intent = new Intent(context, TaskManagerActivity.class);
                context.startActivity(intent);
                ((Activity) context).finish();
            }
        };
    }

    public static Response.ErrorListener errorLoginListener(final Context context, final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                //Utilities.serverHandlingError(context, error);
                Intent intent = new Intent(TAG);
                intent.putExtra("failed", true);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                Utilities.serverHandlingError(context, error);
            }
        };
    }
    //endregion

    public static void putForgetPassword(Context context, String TAG, final String email) {
        GsonRequest<Boolean> mGsonRequest = new GsonRequest<Boolean>(
                Request.Method.PUT,
                context.getString(R.string.base_url) + "Account/ForgotPassword",
                Boolean.class,
                null,
                responseSubmitProfileListener(context, TAG),
                errorSubmitProfileListener(context, TAG)) {

            @Override
            public byte[] getBody() throws AuthFailureError {
                String httpbody = "Email=" + email;
                return httpbody.getBytes();
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utilities.TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<Boolean> responseSubmitProfileListener(final Context context, final String TAG) {
        return new Response.Listener<Boolean>() {
            @Override
            public void onResponse(Boolean response) {
                Log.i(TAG, "Success Retrieved Data - " + response);
                Utilities.showError(context,Utilities.CODE_SUCCESS_SENT, Utilities.SUCCESS_SENT);
                Intent intent = new Intent(context, LoginPage.class);
                context.startActivity(intent);
                ((Activity) context).finish();
            }
        };
    }

    public static Response.ErrorListener errorSubmitProfileListener(final Context context, final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Intent intent = new Intent(TAG);
                intent.putExtra("failed", true);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                Utilities.serverHandlingError(context, error);
            }
        };
    }
}
