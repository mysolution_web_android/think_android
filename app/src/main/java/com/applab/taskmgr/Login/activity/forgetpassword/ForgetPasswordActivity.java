package com.applab.taskmgr.Login.activity.forgetpassword;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.provider.MediaStore;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.applab.taskmgr.Login.activity.login.LoginPage;
import com.applab.taskmgr.Login.webapi.HttpHelper;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Utilities.Utilities;

/**
 * -- =============================================
 * -- Author     : Vicknesh Balasubramaniam
 * -- Create date: 20/1/2016
 * -- Description: AssigneeList .java
 * -- =============================================
 * HISTORY OF UPDATE
 * <p/>
 * NO     DEVELOPER         DATETIME                      DESCRIPTION
 * *******************************************************************************
 * 1
 * 2
 */

public class ForgetPasswordActivity extends AppCompatActivity {

    private Button btnCancel;
    private Button btnSubmit;
    private String TAG = ForgetPasswordActivity.class.getSimpleName();
    private EditText editEmail;
    private RelativeLayout mRl;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);

        btnCancel = (Button) findViewById(R.id.btnCancel);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        editEmail = (EditText) findViewById(R.id.editEmail);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mRl = (RelativeLayout) findViewById(R.id.fadeRL);
        btnSubmit.setOnClickListener(btnSubmitClickListener);
        btnCancel.setOnClickListener(btnCancelClickListener);
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(TAG)) {
                if (intent.getBooleanExtra("failed", false)) {
                    Utilities.setFadeProgressBarVisibility(false, mRl, mProgressBar);
                }
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter iff = new IntentFilter(TAG);
        LocalBroadcastManager.getInstance(getBaseContext()).registerReceiver(broadcastReceiver, iff);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Utilities.hideKeyboard(this);
        LocalBroadcastManager.getInstance(getBaseContext()).unregisterReceiver(broadcastReceiver);
    }

    private View.OnClickListener btnSubmitClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String email = editEmail.getText().toString().trim();
            if (email.length() > 0) {
                Utilities.setFadeProgressBarVisibility(true, mRl, mProgressBar);
                HttpHelper.putForgetPassword(ForgetPasswordActivity.this, TAG, email);
            } else {
                Utilities.showError(ForgetPasswordActivity.this, Utilities.CODE_EMAIL_FIELD, Utilities.EMAIL_FIELD);
            }
        }
    };

    private View.OnClickListener btnCancelClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    @Override
    public void onBackPressed() {
        finish();
    }
}
