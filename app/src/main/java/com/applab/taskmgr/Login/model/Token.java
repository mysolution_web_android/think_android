package com.applab.taskmgr.Login.model;


import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.applab.taskmgr.Utilities.DBHelper;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 -- =============================================
 -- Author     : Vicknesh Balasubramaniam
 -- Create date: 5/2/2016
 -- Description: AssigneeList .java
 -- =============================================
 HISTORY OF UPDATE

 NO     DEVELOPER         DATETIME                      DESCRIPTION
 ********************************************************************************
 1
 2
 */

public class Token implements Parcelable {


    @SerializedName("Token")
    @Expose
    private String Token;
    @SerializedName("UserId")
    @Expose
    private String UserId;
    @SerializedName("ExpireDate")
    @Expose
    private String ExpireDate;

    public Token(Parcel in) {
        readFromParcel(in);
    }

    public Token() {
    }

    /**
     * @return The Token
     */
    public String getToken() {
        return Token;
    }

    /**
     * @param Token The Token
     */
    public void setToken(String Token) {
        this.Token = Token;
    }

    /**
     * @return The UserId
     */
    public String getUserId() {
        return UserId;
    }

    /**
     * @param UserId The UserId
     */
    public void setUserId(String UserId) {
        this.UserId = UserId;
    }

    /**
     * @return The ExpireDate
     */
    public String getExpireDate() {
        return ExpireDate;
    }

    /**
     * @param ExpireDate The ExpireDate
     */
    public void setExpireDate(String ExpireDate) {
        this.ExpireDate = ExpireDate;
    }

    public static Creator getCreator() {
        return getCreator();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Token);
        dest.writeString(this.ExpireDate);
        dest.writeString(this.UserId);
    }

    /**
     * Called from the constructor to create this
     * object from a parcel.
     *
     * @param in parcel from which to re-create object.
     */
    public void readFromParcel(Parcel in) {
        this.Token = in.readString();
        this.UserId = in.readString();
        this.ExpireDate = in.readString();
    }

    @SuppressWarnings("unchecked")
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public Token createFromParcel(Parcel in) {
            return new Token(in);
        }

        @Override
        public Token[] newArray(int size) {
            return new Token[size];
        }
    };


    public static Token getToken(Cursor cursor, int position) {
        Token token = new Token();
        cursor.moveToPosition(position);
        token.setToken(cursor.getString(cursor.getColumnIndex(DBHelper.TOKEN_COLUMN_ACCESS_TOKEN)));
        token.setUserId(cursor.getString(cursor.getColumnIndex(DBHelper.TOKEN_COLUMN_USER_ID)));
        token.setExpireDate(cursor.getString(cursor.getColumnIndex(DBHelper.TOKEN_COLUMN_EXPIRED_DATE)));
        return token;
    }
}
