package com.applab.taskmgr.Login.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.applab.taskmgr.Login.model.Token;
import com.applab.taskmgr.Login.provider.TokenProvider;
import com.applab.taskmgr.Utilities.DBHelper;

/**
 * -- =============================================
 * -- Author     : Vicknesh Balasubramaniam
 * -- Create date: 5/2/2016
 * -- Description: AssigneeList .java
 * -- =============================================
 * HISTORY OF UPDATE
 * <p/>
 * NO     DEVELOPER         DATETIME                      DESCRIPTION
 * *******************************************************************************
 * 1
 * 2
 */
public class CRUDHelper {
    public static void insertToken(Context context, Token token) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBHelper.TOKEN_COLUMN_ACCESS_TOKEN, token.getToken());
        contentValues.put(DBHelper.TOKEN_COLUMN_USER_ID, token.getUserId());
        contentValues.put(DBHelper.TOKEN_COLUMN_EXPIRED_DATE, token.getExpireDate());
        context.getContentResolver().insert(TokenProvider.CONTENT_URI, contentValues);
    }


    public static Token getToken(Context context) {
        Cursor cursor = null;
        Token token = null;
        String mOrder = DBHelper.TOKEN_COLUMN_ID + " ASC " + "LIMIT 1";
        try {
            cursor = context.getContentResolver().query(TokenProvider.CONTENT_URI, null, null, null, mOrder);
            if (cursor != null && cursor.moveToFirst()) {
                token = Token.getToken(cursor, 0);
            }
        } catch (Exception ex) {
            ex.fillInStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return token;
    }
}
