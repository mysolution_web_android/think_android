package com.applab.taskmgr.AssignTo.model;

/**
 * Created by user on 22-Feb-16.
 */

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.applab.taskmgr.Utilities.DBHelper;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AssignTo implements Parcelable {

    public AssignTo() {

    }

    public AssignTo(Parcel in) {
        readFromParcel(in);
    }

    /**
     * Called from the constructor to create this
     * object from a parcel.
     *
     * @param in parcel from which to re-create object.
     */
    public void readFromParcel(Parcel in) {
        this.UserId = in.readString();
        this.CompanyId = in.readInt();
        this.CompanyName = in.readString();
        this.CompanyAddress = in.readString();
        this.Name = in.readString();
        this.Email = in.readString();
        this.ContactNo = in.readString();
        this.MobileNo = in.readString();
        this.Designation = in.readString();
        this.DepartmentId = in.readString();
        this.DepartmentName = in.readString();
        this.ProfileImage = in.readString();
        this.UserStartDate = in.readString();
        this.UserEndDate = in.readString();
        this.PasswordLastChangeDate = in.readString();
        this.LastUpdateDate = in.readString();
        this.isSelected = in.readByte() != 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.UserId);
        dest.writeInt(this.CompanyId);
        dest.writeString(this.CompanyName);
        dest.writeString(this.CompanyAddress);
        dest.writeString(this.Name);
        dest.writeString(this.Email);
        dest.writeString(this.ContactNo);
        dest.writeString(this.MobileNo);
        dest.writeString(this.Designation);
        dest.writeString(this.DepartmentId);
        dest.writeString(this.DepartmentName);
        dest.writeString(this.ProfileImage);
        dest.writeString(this.UserStartDate);
        dest.writeString(this.UserEndDate);
        dest.writeString(this.PasswordLastChangeDate);
        dest.writeByte((byte) (isSelected ? 1 : 0));
    }

    @SuppressWarnings("unchecked")
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public AssignTo createFromParcel(Parcel in) {
            return new AssignTo(in);
        }

        @Override
        public AssignTo[] newArray(int size) {
            return new AssignTo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @SerializedName("UserId")
    @Expose
    private String UserId;
    @SerializedName("CompanyId")
    @Expose
    private Integer CompanyId;
    @SerializedName("CompanyName")
    @Expose
    private String CompanyName;
    @SerializedName("CompanyAddress")
    @Expose
    private String CompanyAddress;
    @SerializedName("Name")
    @Expose
    private String Name;
    @SerializedName("Email")
    @Expose
    private String Email;
    @SerializedName("ContactNo")
    @Expose
    private String ContactNo;
    @SerializedName("MobileNo")
    @Expose
    private String MobileNo;
    @SerializedName("Designation")
    @Expose
    private String Designation;
    @SerializedName("DepartmentId")
    @Expose
    private String DepartmentId;
    @SerializedName("DepartmentName")
    @Expose
    private String DepartmentName;
    @SerializedName("ProfileImage")
    @Expose
    private String ProfileImage;
    @SerializedName("UserStartDate")
    @Expose
    private String UserStartDate;
    @SerializedName("UserEndDate")
    @Expose
    private String UserEndDate;
    @SerializedName("PasswordLastChangeDate")
    @Expose
    private String PasswordLastChangeDate;
    @SerializedName("LastUpdateDate")
    @Expose
    private String LastUpdateDate;

    private Boolean isSelected = false;

    public Boolean getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(Boolean isSelected) {
        this.isSelected = isSelected;
    }

    /**
     * @return The UserId
     */
    public String getUserId() {
        return UserId;
    }

    /**
     * @param UserId The UserId
     */
    public void setUserId(String UserId) {
        this.UserId = UserId;
    }

    /**
     * @return The CompanyId
     */
    public Integer getCompanyId() {
        return CompanyId;
    }

    /**
     * @param CompanyId The CompanyId
     */
    public void setCompanyId(Integer CompanyId) {
        this.CompanyId = CompanyId;
    }

    /**
     * @return The CompanyName
     */
    public String getCompanyName() {
        return CompanyName;
    }

    /**
     * @param CompanyName The CompanyName
     */
    public void setCompanyName(String CompanyName) {
        this.CompanyName = CompanyName;
    }

    /**
     * @return The CompanyAddress
     */
    public String getCompanyAddress() {
        return CompanyAddress;
    }

    /**
     * @param CompanyAddress The CompanyAddress
     */
    public void setCompanyAddress(String CompanyAddress) {
        this.CompanyAddress = CompanyAddress;
    }

    /**
     * @return The Name
     */
    public String getName() {
        return Name;
    }

    /**
     * @param Name The Name
     */
    public void setName(String Name) {
        this.Name = Name;
    }

    /**
     * @return The Email
     */
    public String getEmail() {
        return Email;
    }

    /**
     * @param Email The Email
     */
    public void setEmail(String Email) {
        this.Email = Email;
    }

    /**
     * @return The ContactNo
     */
    public String getContactNo() {
        return ContactNo;
    }

    /**
     * @param ContactNo The ContactNo
     */
    public void setContactNo(String ContactNo) {
        this.ContactNo = ContactNo;
    }

    /**
     * @return The MobileNo
     */
    public String getMobileNo() {
        return MobileNo;
    }

    /**
     * @param MobileNo The MobileNo
     */
    public void setMobileNo(String MobileNo) {
        this.MobileNo = MobileNo;
    }

    /**
     * @return The Designation
     */
    public String getDesignation() {
        return Designation;
    }

    /**
     * @param Designation The Designation
     */
    public void setDesignation(String Designation) {
        this.Designation = Designation;
    }

    /**
     * @return The DepartmentId
     */
    public String getDepartmentId() {
        return DepartmentId;
    }

    /**
     * @param DepartmentId The DepartmentId
     */
    public void setDepartmentId(String DepartmentId) {
        this.DepartmentId = DepartmentId;
    }

    /**
     * @return The DepartmentName
     */
    public String getDepartmentName() {
        return DepartmentName;
    }

    /**
     * @param DepartmentName The DepartmentName
     */
    public void setDepartmentName(String DepartmentName) {
        this.DepartmentName = DepartmentName;
    }

    /**
     * @return The ProfileImage
     */
    public String getProfileImage() {
        return ProfileImage;
    }

    /**
     * @param ProfileImage The ProfileImage
     */
    public void setProfileImage(String ProfileImage) {
        this.ProfileImage = ProfileImage;
    }

    /**
     * @return The UserStartDate
     */
    public String getUserStartDate() {
        return UserStartDate;
    }

    /**
     * @param UserStartDate The UserStartDate
     */
    public void setUserStartDate(String UserStartDate) {
        this.UserStartDate = UserStartDate;
    }

    /**
     * @return The UserEndDate
     */
    public String getUserEndDate() {
        return UserEndDate;
    }

    /**
     * @param UserEndDate The UserEndDate
     */
    public void setUserEndDate(String UserEndDate) {
        this.UserEndDate = UserEndDate;
    }

    /**
     * @return The PasswordLastChangeDate
     */
    public String getPasswordLastChangeDate() {
        return PasswordLastChangeDate;
    }

    /**
     * @param PasswordLastChangeDate The PasswordLastChangeDate
     */
    public void setPasswordLastChangeDate(String PasswordLastChangeDate) {
        this.PasswordLastChangeDate = PasswordLastChangeDate;
    }

    /**
     * @return The LastUpdateDate
     */
    public String getLastUpdateDate() {
        return LastUpdateDate;
    }

    /**
     * @param LastUpdateDate The LastUpdateDate
     */
    public void setLastUpdateDate(String LastUpdateDate) {
        this.LastUpdateDate = LastUpdateDate;
    }

    public static AssignTo getAssignTo(Cursor cursor, int position) {
        cursor.moveToPosition(position);
        AssignTo assignTo = new AssignTo();
        assignTo.setUserId(cursor.getString(cursor.getColumnIndex(DBHelper.USER_COLUMN_USER_ID)));
        assignTo.setName(cursor.getString(cursor.getColumnIndex(DBHelper.USER_COLUMN_NAME)));
        assignTo.setCompanyId(cursor.getInt(cursor.getColumnIndex(DBHelper.USER_COLUMN_COMPANY_ID)));
        assignTo.setCompanyName(cursor.getString(cursor.getColumnIndex(DBHelper.USER_COLUMN_COMPANY_NAME)));
        assignTo.setCompanyAddress(cursor.getString(cursor.getColumnIndex(DBHelper.USER_COLUMN_COMPANY_ADDRESS)));
        assignTo.setDepartmentId(cursor.getString(cursor.getColumnIndex(DBHelper.USER_COLUMN_DEPARTMENT_ID)));
        assignTo.setDepartmentName(cursor.getString(cursor.getColumnIndex(DBHelper.USER_COLUMN_DEPARTMENT_NAME)));
        assignTo.setDesignation(cursor.getString(cursor.getColumnIndex(DBHelper.USER_COLUMN_DESIGNATION)));
        assignTo.setContactNo(cursor.getString(cursor.getColumnIndex(DBHelper.USER_COLUMN_CONTACT_NO)));
        assignTo.setMobileNo(cursor.getString(cursor.getColumnIndex(DBHelper.USER_COLUMN_MOBILE_NO)));
        assignTo.setProfileImage(cursor.getString(cursor.getColumnIndex(DBHelper.USER_COLUMN_PROFILE_IMAGE)));
        assignTo.setUserStartDate(cursor.getString(cursor.getColumnIndex(DBHelper.USER_COLUMN_USER_START_DATE)));
        assignTo.setUserEndDate(cursor.getString(cursor.getColumnIndex(DBHelper.USER_COLUMN_USER_END_DATE)));
        assignTo.setPasswordLastChangeDate(cursor.getString(cursor.getColumnIndex(DBHelper.USER_COLUMN_PASSWORD_LAST_CHANGED_DATE)));
        assignTo.setLastUpdateDate(cursor.getString(cursor.getColumnIndex(DBHelper.USER_COLUMN_LAST_UPDATED_DATE)));
        return assignTo;
    }

}
