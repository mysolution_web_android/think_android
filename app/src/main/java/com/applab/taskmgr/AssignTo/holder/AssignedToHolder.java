package com.applab.taskmgr.AssignTo.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applab.taskmgr.R;

/**
 * -- =============================================
 * -- Author     : Vicknesh Balasubramaniam
 * -- Create date: 16/2/2017
 * -- Description: AssigneeList .java
 * -- =============================================
 * HISTORY OF UPDATE
 * <p/>
 * NO     DEVELOPER         DATETIME                      DESCRIPTION
 * *******************************************************************************
 * 1
 * 2
 */
public class AssignedToHolder extends RecyclerView.ViewHolder {
    private TextView txtName;
    private TextView txtPosition;
    private TextView txtDepartment;
    private ImageView imgProfile;
    private ImageView cboChecker;
    private RelativeLayout cboClick;

    public AssignedToHolder(View itemView) {
        super(itemView);

        txtName = (TextView) itemView.findViewById(R.id.list_name);
        txtPosition = (TextView) itemView.findViewById(R.id.list_position);
        txtDepartment = (TextView) itemView.findViewById(R.id.list_department);
        imgProfile = (ImageView) itemView.findViewById(R.id.list_avatar);
        cboChecker = (ImageView) itemView.findViewById(R.id.list_check);
        cboClick = (RelativeLayout) itemView.findViewById(R.id.list_checkClick);
    }

    public TextView getTxtName() {
        return txtName;
    }

    public TextView getTxtPosition() {
        return txtPosition;
    }

    public TextView getTxtDepartment() {
        return txtDepartment;
    }

    public ImageView getImgProfile() {
        return imgProfile;
    }

    public ImageView getCboChecker() {
        return cboChecker;
    }

    public RelativeLayout getCboClick() {
        return cboClick;
    }

    public void setCboChecker(ImageView cboChecker) {
        this.cboChecker = cboChecker;
    }

    public void setCboClick(RelativeLayout cboClick) {
        this.cboClick = cboClick;
    }
}
