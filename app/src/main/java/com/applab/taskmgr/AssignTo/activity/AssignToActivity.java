package com.applab.taskmgr.AssignTo.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.applab.taskmgr.AssignTo.adapter.AssignedToAdapter;
import com.applab.taskmgr.AssignTo.model.AssignTo;
import com.applab.taskmgr.AssignTo.provider.UserProvider;
import com.applab.taskmgr.AssignTo.webapi.HttpHelperAssign;
import com.applab.taskmgr.Menu.fragment.NavigationDrawerFragment;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Utilities.DBHelper;
import com.applab.taskmgr.Utilities.ItemClickSupport;
import java.util.ArrayList;

/**
 * -- =============================================
 * -- Author     : Vicknesh Balasubramaniam
 * -- Create date: 16/2/2017
 * -- Description: AssigneeList .java
 * -- =============================================
 * HISTORY OF UPDATE
 * <p/>
 * NO     DEVELOPER         DATETIME                      DESCRIPTION
 * *******************************************************************************
 * 1
 * 2
 */

public class AssignToActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    private Toolbar mToolbar;
    private TextView mTxtToolbarTitle;
    public static ArrayList<String> mAssignToUserId = new ArrayList<>();
    public static ArrayList<String> mAssignToUserName = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private Cursor mCursor;
    private RecyclerView.LayoutManager mLayoutManager;
    private AssignedToAdapter assignedToAdapter;
    private NavigationDrawerFragment mDrawerFragment;
    private String TAG = AssignToActivity.class.getSimpleName();
    private int mLoaderId = 7844;
    private EditText mEditSearch;
    String selection;
    String selectionArgs[];
    private ProgressBar mProgressBar;
    private LinearLayout mBtnSave, mBtnCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assign_to);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mEditSearch = (EditText) findViewById(R.id.ediSearch);

        mToolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(mToolbar);
        mTxtToolbarTitle = (TextView) mToolbar.findViewById(R.id.txtTitle);
        mTxtToolbarTitle.setText(getString(R.string.title_activity_assign_to));

        mDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        mDrawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        mDrawerFragment.mDrawerToggle.setDrawerIndicatorEnabled(false);
        mBtnSave = (LinearLayout) findViewById(R.id.btnSave);
        mBtnCancel = (LinearLayout) findViewById(R.id.btnCancel);
        mToolbar.setNavigationIcon(R.mipmap.menu);
        mToolbar.setNavigationOnClickListener(mToolbarOnClickListener);

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recyclerView);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        setIntent(getIntent());
        assignedToAdapter = new AssignedToAdapter(this, mCursor);
        mRecyclerView.setAdapter(assignedToAdapter);

        mRecyclerView.setLayoutManager(mLayoutManager);
        ItemClickSupport.addTo(mRecyclerView).setOnItemClickListener(mItemClickListener);
        ItemClickSupport.addTo(mRecyclerView).setOnItemLongClickListener(mItemLongClickListener);
        mEditSearch.setOnEditorActionListener(mEditSearchOnEditorActionListener);
        mBtnSave.setOnClickListener(mBtnSaveOnClickListener);
        mBtnCancel.setOnClickListener(mBtnCancelOnCLickListener);
    }


    private View.OnClickListener mBtnSaveOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.putExtra("arrayUserId", mAssignToUserId);
            intent.putExtra("arrayUserName", mAssignToUserName);
            setResult(RESULT_OK, intent);
            finish();
        }
    };

    private View.OnClickListener mBtnCancelOnCLickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    public void setIntent(Intent intent) {
        if (intent.getStringArrayListExtra("arrayUserId") != null) {
            mAssignToUserId = getIntent().getStringArrayListExtra("arrayUserId");
            mAssignToUserName = getIntent().getStringArrayListExtra("arrayUserName");
        }
    }

    private TextView.OnEditorActionListener mEditSearchOnEditorActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                if (!mEditSearch.getText().toString().equals("")) {
                    selection = DBHelper.USER_COLUMN_NAME + " LIKE ? OR "
                            + DBHelper.USER_COLUMN_DEPARTMENT_NAME + " LIKE ? OR "
                            + DBHelper.USER_COLUMN_DESIGNATION + " LIKE ?";
                    selectionArgs = new String[]{"%" + mEditSearch.getText().toString() + "%", "%" + mEditSearch.getText().toString() + "%",
                            "%" + mEditSearch.getText().toString() + "%"};
                } else {
                    selection = null;
                    selectionArgs = null;
                }
                AssignToActivity.this.getSupportLoaderManager().restartLoader(mLoaderId, null, AssignToActivity.this);
                InputMethodManager imm = (InputMethodManager) AssignToActivity.this.getSystemService(AssignToActivity.this.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                return true;
            }
            return false;
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        mProgressBar.setVisibility(View.VISIBLE);
        IntentFilter iff = new IntentFilter(TAG);
        LocalBroadcastManager.getInstance(getBaseContext()).registerReceiver(broadcastReceiver, iff);
        AssignToActivity.this.getSupportLoaderManager().initLoader(mLoaderId, null, this);
        HttpHelperAssign.getUserList(AssignToActivity.this, TAG);
    }

    @Override
    protected void onPause() {
        super.onPause();

        LocalBroadcastManager.getInstance(getBaseContext()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onBackPressed() {
        mDrawerFragment.setCloseDrawer(this);
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(TAG)) {
                if (intent.getBooleanExtra("isFetched", false)) {
                    Handler handlerTimer = new Handler();
                    handlerTimer.postDelayed(new Runnable() {
                        public void run() {
                            mProgressBar.setVisibility(View.GONE);
                        }
                    }, 2000);
                }
            }
        }
    };

    private ItemClickSupport.OnItemClickListener mItemClickListener = new ItemClickSupport.OnItemClickListener() {
        @Override
        public void onItemClicked(RecyclerView recyclerView, final int position, View v) {
            ImageView img = (ImageView) v.findViewById(R.id.list_check);
            AssignTo current = (AssignTo) img.getTag();
            if (current.getIsSelected()) {
                if (mAssignToUserId.contains(current.getUserId())) {
                    int pos = mAssignToUserId.indexOf(current.getUserId());
                    mAssignToUserId.remove(pos);
                    mAssignToUserName.remove(pos);
                }
                current.setIsSelected(false);
                img.setImageResource(R.mipmap.uncheck_box_2);
            } else {
                mAssignToUserId.add(current.getUserId());
                mAssignToUserName.add(current.getName());
                current.setIsSelected(true);
                img.setImageResource(R.mipmap.check_box_2);
            }
            img.setTag(current);
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_assign_to, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return mDrawerFragment.mDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }


    private ItemClickSupport.OnItemLongClickListener mItemLongClickListener = new ItemClickSupport.OnItemLongClickListener() {
        @Override
        public boolean onItemLongClicked(RecyclerView recyclerView, final int position, View v) {
            return false;
        }
    };

    private View.OnClickListener mToolbarOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mDrawerFragment.setOpenCloseDrawer();
        }
    };

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (mLoaderId == id) {
            return new CursorLoader(AssignToActivity.this, UserProvider.CONTENT_URI, null, selection, selectionArgs, null);
        } else {
            return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == mLoaderId) {
            if (data != null) {
                mCursor = data;
                assignedToAdapter.swapCursor(mCursor);
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        assignedToAdapter.swapCursor(null);
    }

}
