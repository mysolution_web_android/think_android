package com.applab.taskmgr.AssignTo.adapter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.applab.taskmgr.AssignTo.activity.AssignToActivity;
import com.applab.taskmgr.AssignTo.holder.AssignedToHolder;
import com.applab.taskmgr.AssignTo.model.AssignTo;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Utilities.CircleTransform;
import com.bumptech.glide.Glide;

import java.util.ArrayList;


public class AssignedToAdapter extends RecyclerView.Adapter<AssignedToHolder> {
    private Context mContext;
    private LayoutInflater layoutInflater;
    private Cursor mCursor;

    public AssignedToAdapter(Context context, Cursor cursor) {
        this.mContext = context;
        this.mCursor = cursor;
        this.layoutInflater = LayoutInflater.from(mContext);
    }

    @Override
    public AssignedToHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.custom_assigned_list, parent, false);
        return new AssignedToHolder(view);
    }

    @Override
    public void onBindViewHolder(final AssignedToHolder holder, final int position) {
        if (mCursor != null) {
            final AssignTo assignTo = AssignTo.getAssignTo(mCursor, position);
            holder.getTxtName().setText(assignTo.getName());
            holder.getTxtPosition().setText(assignTo.getDesignation());
            holder.getTxtDepartment().setText(assignTo.getDepartmentName());

            Glide.with(mContext)
                    .load(assignTo.getProfileImage())
                    .placeholder(R.mipmap.ic_action_person_light)
                    .transform(new CircleTransform(mContext))
                    .into(holder.getImgProfile());

            if (AssignToActivity.mAssignToUserId.contains(assignTo.getUserId())) {
                assignTo.setIsSelected(true);
                holder.getCboChecker().setImageResource(R.mipmap.check_box_2);
            } else {
                assignTo.setIsSelected(false);
                holder.getCboChecker().setImageResource(R.mipmap.uncheck_box_2);
            }
            holder.getCboChecker().setTag(assignTo);
            holder.getCboChecker().setOnClickListener(cboCheckOnClickListener);
        }
    }

    private View.OnClickListener cboCheckOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ImageView img = (ImageView) v.findViewById(R.id.list_check);
            AssignTo assignTo = (AssignTo) img.getTag();
            ArrayList<String> arrayAsigneeId = AssignToActivity.mAssignToUserId;
            ArrayList<String> arrayAsigneeName = AssignToActivity.mAssignToUserName;
            if (assignTo.getIsSelected()) {
                if (arrayAsigneeId.contains(assignTo.getUserId())) {
                    int pos = arrayAsigneeId.indexOf(assignTo.getUserId());
                    arrayAsigneeId.remove(pos);
                    arrayAsigneeName.remove(pos);
                }
                assignTo.setIsSelected(false);
                img.setImageResource(R.mipmap.uncheck_box_2);
            } else {
                arrayAsigneeId.add(assignTo.getUserId());
                arrayAsigneeName.add(assignTo.getName());
                assignTo.setIsSelected(true);
                img.setImageResource(R.mipmap.check_box_2);
            }
            AssignToActivity.mAssignToUserId = arrayAsigneeId;
            AssignToActivity.mAssignToUserName = arrayAsigneeName;
            img.setTag(assignTo);
        }
    };


    @Override
    public int getItemCount() {
        return mCursor != null ? mCursor.getCount() : 0;
    }

    public Cursor swapCursor(Cursor cursor) {
        if (this.mCursor == cursor) {
            return null;
        }

        Cursor oldCursor = this.mCursor;
        this.mCursor = cursor;
        if (cursor != null) {
            notifyDataSetChanged();
        }

        return oldCursor;
    }
}
