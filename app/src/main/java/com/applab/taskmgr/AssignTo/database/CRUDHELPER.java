package com.applab.taskmgr.AssignTo.database;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.applab.taskmgr.AssignTo.model.AssignTo;
import com.applab.taskmgr.AssignTo.provider.UserProvider;
import com.applab.taskmgr.Utilities.DBHelper;
import com.applab.taskmgr.Utilities.Utilities;

/**
 * Created by user on 22-Feb-16.
 */
public class CRUDHELPER {


    public static void insertUser(Context context, String TAG, AssignTo[] assignTo) {
        DBHelper helper = new DBHelper(context);
        try {
            ContentValues[] contentValueses = new ContentValues[assignTo.length];
            for (int i = 0; i < assignTo.length; i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.USER_COLUMN_USER_ID, assignTo[i].getUserId());
                contentValues.put(DBHelper.USER_COLUMN_NAME, assignTo[i].getName());
                contentValues.put(DBHelper.USER_COLUMN_COMPANY_ID, assignTo[i].getCompanyId());
                contentValues.put(DBHelper.USER_COLUMN_COMPANY_NAME, assignTo[i].getCompanyName());
                contentValues.put(DBHelper.USER_COLUMN_COMPANY_ADDRESS, assignTo[i].getCompanyAddress());
                contentValues.put(DBHelper.USER_COLUMN_CONTACT_NO, assignTo[i].getContactNo());
                contentValues.put(DBHelper.USER_COLUMN_MOBILE_NO, assignTo[i].getMobileNo());
                contentValues.put(DBHelper.USER_COLUMN_PROFILE_IMAGE, assignTo[i].getProfileImage());
                contentValues.put(DBHelper.USER_COLUMN_DESIGNATION, assignTo[i].getDesignation());
                contentValues.put(DBHelper.USER_COLUMN_DEPARTMENT_ID, assignTo[i].getDepartmentId());
                contentValues.put(DBHelper.USER_COLUMN_DEPARTMENT_NAME, assignTo[i].getDepartmentName());
                contentValues.put(DBHelper.USER_COLUMN_USER_START_DATE, Utilities.setToLocalDateTime(Utilities.DATE_FORMAT, Utilities.DATE_FORMAT, assignTo[i].getUserStartDate()));
                contentValues.put(DBHelper.USER_COLUMN_USER_END_DATE, Utilities.setToLocalDateTime(Utilities.DATE_FORMAT, Utilities.DATE_FORMAT, assignTo[i].getUserEndDate()));
                contentValues.put(DBHelper.USER_COLUMN_PASSWORD_LAST_CHANGED_DATE, Utilities.setToLocalDateTime(Utilities.DATE_FORMAT, Utilities.DATE_FORMAT, assignTo[i].getPasswordLastChangeDate()));
                contentValues.put(DBHelper.USER_COLUMN_LAST_UPDATED_DATE, Utilities.setToLocalDateTime(Utilities.DATE_FORMAT, Utilities.DATE_FORMAT, assignTo[i].getLastUpdateDate()));
                contentValueses[i] = contentValues;
            }

            boolean isEmpty = true;
            for (ContentValues contentValues : contentValueses) {
                if (contentValues != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                context.getContentResolver().bulkInsert(UserProvider.CONTENT_URI, contentValueses);
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Download Error :" + ex.toString());
        } finally {
            Intent intent = new Intent(TAG);
            intent.putExtra("isFetched", true);
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        }

    }
}
