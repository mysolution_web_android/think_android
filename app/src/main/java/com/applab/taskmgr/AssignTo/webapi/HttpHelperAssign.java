package com.applab.taskmgr.AssignTo.webapi;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.applab.taskmgr.AssignTo.model.AssignTo;
import com.applab.taskmgr.AssignTo.provider.UserProvider;
import com.applab.taskmgr.Login.database.CRUDHelper;
import com.applab.taskmgr.Login.model.Token;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Utilities.AppController;
import com.applab.taskmgr.Utilities.GsonRequest;
import com.applab.taskmgr.Utilities.Utilities;

import java.util.HashMap;

/**
 * Created by user on 22-Feb-16.
 */
public class HttpHelperAssign {
    public static void getUserList(Context context, String TAG) {
        final Token token = CRUDHelper.getToken(context);
        if (token != null) {
            if (token.getToken() != null) {
                HashMap<String, String> header = new HashMap<String, String>();
                header.put("Authorization", token.getToken());
                GsonRequest<AssignTo[]> mGsonRequest = new GsonRequest<AssignTo[]>(
                        Request.Method.GET,
                        context.getString(R.string.base_url) + "User/List",
                        AssignTo[].class,
                        header,
                        responseListener(context, TAG),
                        errorProfileListener(context, TAG)
                ) {
                };

                mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utilities.TIMEOUT,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
            }
        }

    }

    public static Response.Listener<AssignTo[]> responseListener(final Context context, final String TAG) {
        return new Response.Listener<AssignTo[]>() {
            @Override
            public void onResponse(AssignTo[] response) {
                if (response != null) {
                    if (response.length > 0) {
                        Log.i(TAG, "Succesfully entered data : " + response);
                        context.getContentResolver().delete(UserProvider.CONTENT_URI, null, null);
                        com.applab.taskmgr.AssignTo.database.CRUDHELPER.insertUser(context, TAG, response);
                    } else {

                    }
                } else {
                    Intent intent = new Intent(TAG);
                    intent.putExtra("isFetched", true);
                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                }

            }
        };
    }

    public static Response.ErrorListener errorProfileListener(final Context context, final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Utilities.serverHandlingError(context, error);
                Intent intent = new Intent(TAG);
                intent.putExtra("isFetched", true);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }
        };
    }
}
