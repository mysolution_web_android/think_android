package com.applab.taskmgr.Task.database;

import android.content.ContentValues;
import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.applab.taskmgr.R;
import com.applab.taskmgr.Task.model.Task;
import com.applab.taskmgr.Task.provider.AssigneeProvider;
import com.applab.taskmgr.Task.provider.TaskProvider;
import com.applab.taskmgr.Utilities.DBHelper;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by user on 17-Feb-16.
 */
public class CRUDHelper {
    public static void insertNewTask(Task[] tasks, Context context, String TAG) {
        DBHelper helper = new DBHelper(context);
        try {
            ContentValues[] contentValueses = new ContentValues[tasks.length];
            ArrayList<ContentValues> contentValuesArrayList = new ArrayList<>();
            for (int i = 0; i < tasks.length; i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.TASK_COLUMN_ASSIGNER_ID, tasks[i].getAssignerId());
                contentValues.put(DBHelper.TASK_COLUMN_ASSIGNER_NAME, tasks[i].getAssignerName());
                contentValues.put(DBHelper.TASK_COLUMN_ASSIGNER_PHOTO, tasks[i].getAssignerPhoto());
                contentValues.put(DBHelper.TASK_COLUMN_FILE, tasks[i].getFile());
                contentValues.put(DBHelper.TASK_COLUMN_TASK_ID, tasks[i].getTaskId());
                contentValues.put(DBHelper.TASK_COLUMN_CREATE_DATE, tasks[i].getCreateDate());
                contentValues.put(DBHelper.TASK_COLUMN_TITLE, tasks[i].getTaskTitle());
                contentValues.put(DBHelper.TASK_COLUMN_DESCRIPTION, tasks[i].getTaskDescription());
                contentValues.put(DBHelper.TASK_COLUMN_LOCTION, tasks[i].getLocation());
                contentValues.put(DBHelper.TASK_COLUMN_PRIORITY, tasks[i].getPriority());
                contentValues.put(DBHelper.TASK_COLUMN_MAP, tasks[i].getMap());
                contentValues.put(DBHelper.TASK_COLUMN_UPDATE_BY, tasks[i].getUpdateBy());
                contentValues.put(DBHelper.TASK_COLUMN_UPDATE_DATE, tasks[i].getUpdateDate());
                contentValues.put(DBHelper.TASK_COLUMN_DUE_DATE, tasks[i].getTaskDueDate());
                contentValues.put(DBHelper.TASK_COLUMN_COMPLETION_STATUS, tasks[i].getCompletionStatus());
                contentValues.put(DBHelper.TASK_COLUMN_DATE, tasks[i].getTaskDate());
                contentValues.put(DBHelper.TASK_COLUMN_TYPE, context.getString(R.string.new_tasks));
                if (tasks[i].getFile() != null) {
                    String filePath = new File(Environment.getExternalStorageDirectory() + "/" + context.getResources().getString(R.string.folder_name) + "/" +
                            context.getString(R.string.task_attachment)).getAbsolutePath() + "/" + tasks[i].getTaskId() + "/" + tasks[i].getFile().split("/")[tasks[i].getFile().split("/").length - 1];
                    contentValues.put(DBHelper.TASK_COLUMN_STORED_LOCATION, filePath);
                }
                contentValueses[i] = contentValues;
                for (int j = 0; j < tasks[i].getAssigneeList().size(); j++) {
                    contentValues = new ContentValues();
                    contentValues.put(DBHelper.ASSIGNEELIST_COLUMN_USER_ID, tasks[i].getAssigneeList().get(j).getUserId());
                    contentValues.put(DBHelper.ASSIGNEELIST_COLUMN_USER_NAME, tasks[i].getAssigneeList().get(j).getUserName());
                    contentValues.put(DBHelper.ASSIGNEELIST_COLUMN_CREATE_DATE, tasks[i].getAssigneeList().get(j).getCreateDate());
                    contentValues.put(DBHelper.ASSIGNEELIST_COLUMN_TASK_ID, tasks[i].getTaskId());
                    contentValues.put(DBHelper.ASSIGNEELIST_COLUMN_TASK_TYPE, context.getString(R.string.new_tasks));
                    if (!contentValuesArrayList.contains(contentValues)) {
                        contentValuesArrayList.add(contentValues);
                    }
                }
            }
            boolean isEmpty = true;
            for (ContentValues contentValues : contentValueses) {
                if (contentValues != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                context.getContentResolver().bulkInsert(TaskProvider.CONTENT_URI, contentValueses);
            }

            isEmpty = true;
            contentValueses = contentValuesArrayList.toArray(new ContentValues[contentValuesArrayList.size()]);
            for (ContentValues contentValues : contentValueses) {
                if (contentValues != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                context.getContentResolver().bulkInsert(AssigneeProvider.CONTENT_URI, contentValueses);
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Download Error :" + ex.toString());
        }
    }

    //MYTASK
    public static void insertMyTask(Task[] tasks, Context context, String TAG) {
        DBHelper helper = new DBHelper(context);
        try {
            ContentValues[] contentValueses = new ContentValues[tasks.length];
            ArrayList<ContentValues> contentValuesArrayList = new ArrayList<>();
            for (int i = 0; i < tasks.length; i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.TASK_COLUMN_ASSIGNER_ID, tasks[i].getAssignerId());
                contentValues.put(DBHelper.TASK_COLUMN_ASSIGNER_NAME, tasks[i].getAssignerName());
                contentValues.put(DBHelper.TASK_COLUMN_ASSIGNER_PHOTO, tasks[i].getAssignerPhoto());
                contentValues.put(DBHelper.TASK_COLUMN_FILE, tasks[i].getFile());
                contentValues.put(DBHelper.TASK_COLUMN_TASK_ID, tasks[i].getTaskId());
                contentValues.put(DBHelper.TASK_COLUMN_CREATE_DATE, tasks[i].getCreateDate());
                contentValues.put(DBHelper.TASK_COLUMN_TITLE, tasks[i].getTaskTitle());
                contentValues.put(DBHelper.TASK_COLUMN_DESCRIPTION, tasks[i].getTaskDescription());
                contentValues.put(DBHelper.TASK_COLUMN_LOCTION, tasks[i].getLocation());
                contentValues.put(DBHelper.TASK_COLUMN_PRIORITY, tasks[i].getPriority());
                contentValues.put(DBHelper.TASK_COLUMN_MAP, tasks[i].getMap());
                contentValues.put(DBHelper.TASK_COLUMN_UPDATE_BY, tasks[i].getUpdateBy());
                contentValues.put(DBHelper.TASK_COLUMN_UPDATE_DATE, tasks[i].getUpdateDate());
                contentValues.put(DBHelper.TASK_COLUMN_DUE_DATE, tasks[i].getTaskDueDate());
                contentValues.put(DBHelper.TASK_COLUMN_COMPLETION_STATUS, tasks[i].getCompletionStatus());
                contentValues.put(DBHelper.TASK_COLUMN_DATE, tasks[i].getTaskDate());
                contentValues.put(DBHelper.TASK_COLUMN_TYPE, context.getString(R.string.my_pending_task));
                if (tasks[i].getFile() != null) {
                    String filePath = new File(Environment.getExternalStorageDirectory() + "/" + context.getResources().getString(R.string.folder_name) + "/" +
                            context.getString(R.string.task_attachment)).getAbsolutePath() + "/" + tasks[i].getTaskId() + "/" + tasks[i].getFile().split("/")[tasks[i].getFile().split("/").length - 1];
                    contentValues.put(DBHelper.TASK_COLUMN_STORED_LOCATION, filePath);
                }
                contentValueses[i] = contentValues;
                for (int j = 0; j < tasks[i].getAssigneeList().size(); j++) {
                    contentValues = new ContentValues();
                    contentValues.put(DBHelper.ASSIGNEELIST_COLUMN_USER_ID, tasks[i].getAssigneeList().get(j).getUserId());
                    contentValues.put(DBHelper.ASSIGNEELIST_COLUMN_USER_NAME, tasks[i].getAssigneeList().get(j).getUserName());
                    contentValues.put(DBHelper.ASSIGNEELIST_COLUMN_CREATE_DATE, tasks[i].getAssigneeList().get(j).getCreateDate());
                    contentValues.put(DBHelper.ASSIGNEELIST_COLUMN_TASK_ID, tasks[i].getTaskId());
                    contentValues.put(DBHelper.ASSIGNEELIST_COLUMN_TASK_TYPE, context.getString(R.string.my_pending_task));
                    if (!contentValuesArrayList.contains(contentValues)) {
                        contentValuesArrayList.add(contentValues);
                    }
                }
            }
            boolean isEmpty = true;
            for (ContentValues contentValues : contentValueses) {
                if (contentValues != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                context.getContentResolver().bulkInsert(TaskProvider.CONTENT_URI, contentValueses);
            }

            isEmpty = true;
            contentValueses = contentValuesArrayList.toArray(new ContentValues[contentValuesArrayList.size()]);
            for (ContentValues contentValues : contentValueses) {
                if (contentValues != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                context.getContentResolver().bulkInsert(AssigneeProvider.CONTENT_URI, contentValueses);
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Download Error :" + ex.toString());
        }
    }

    //Completed Task
    public static void insertCompletedTask(Task[] tasks, Context context, String TAG) {
        DBHelper helper = new DBHelper(context);
        try {
            ContentValues[] contentValueses = new ContentValues[tasks.length];
            ArrayList<ContentValues> contentValuesArrayList = new ArrayList<>();
            for (int i = 0; i < tasks.length; i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.TASK_COLUMN_ASSIGNER_ID, tasks[i].getAssignerId());
                contentValues.put(DBHelper.TASK_COLUMN_ASSIGNER_NAME, tasks[i].getAssignerName());
                contentValues.put(DBHelper.TASK_COLUMN_ASSIGNER_PHOTO, tasks[i].getAssignerPhoto());
                contentValues.put(DBHelper.TASK_COLUMN_TASK_ID, tasks[i].getTaskId());
                contentValues.put(DBHelper.TASK_COLUMN_FILE, tasks[i].getFile());
                contentValues.put(DBHelper.TASK_COLUMN_CREATE_DATE, tasks[i].getCreateDate());
                contentValues.put(DBHelper.TASK_COLUMN_TITLE, tasks[i].getTaskTitle());
                contentValues.put(DBHelper.TASK_COLUMN_DESCRIPTION, tasks[i].getTaskDescription());
                contentValues.put(DBHelper.TASK_COLUMN_LOCTION, tasks[i].getLocation());
                contentValues.put(DBHelper.TASK_COLUMN_PRIORITY, tasks[i].getPriority());
                contentValues.put(DBHelper.TASK_COLUMN_MAP, tasks[i].getMap());
                contentValues.put(DBHelper.TASK_COLUMN_UPDATE_BY, tasks[i].getUpdateBy());
                contentValues.put(DBHelper.TASK_COLUMN_UPDATE_DATE, tasks[i].getUpdateDate());
                contentValues.put(DBHelper.TASK_COLUMN_DUE_DATE, tasks[i].getTaskDueDate());
                contentValues.put(DBHelper.TASK_COLUMN_COMPLETION_STATUS, tasks[i].getCompletionStatus());
                contentValues.put(DBHelper.TASK_COLUMN_DATE, tasks[i].getTaskDate());
                contentValues.put(DBHelper.TASK_COLUMN_TYPE, context.getString(R.string.completed_task));
                if (tasks[i].getFile() != null) {
                    String filePath = new File(Environment.getExternalStorageDirectory() + "/" + context.getResources().getString(R.string.folder_name) + "/" +
                            context.getString(R.string.task_attachment)).getAbsolutePath() + "/" + tasks[i].getTaskId() + "/" + tasks[i].getFile().split("/")[tasks[i].getFile().split("/").length - 1];
                    contentValues.put(DBHelper.TASK_COLUMN_STORED_LOCATION, filePath);
                }
                contentValueses[i] = contentValues;
                for (int j = 0; j < tasks[i].getAssigneeList().size(); j++) {
                    contentValues = new ContentValues();
                    contentValues.put(DBHelper.ASSIGNEELIST_COLUMN_USER_ID, tasks[i].getAssigneeList().get(j).getUserId());
                    contentValues.put(DBHelper.ASSIGNEELIST_COLUMN_USER_NAME, tasks[i].getAssigneeList().get(j).getUserName());
                    contentValues.put(DBHelper.ASSIGNEELIST_COLUMN_CREATE_DATE, tasks[i].getAssigneeList().get(j).getCreateDate());
                    contentValues.put(DBHelper.ASSIGNEELIST_COLUMN_TASK_ID, tasks[i].getTaskId());
                    contentValues.put(DBHelper.ASSIGNEELIST_COLUMN_TASK_TYPE, context.getString(R.string.completed_task));
                    if (!contentValuesArrayList.contains(contentValues)) {
                        contentValuesArrayList.add(contentValues);
                    }
                }
            }
            boolean isEmpty = true;
            for (ContentValues contentValues : contentValueses) {
                if (contentValues != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                context.getContentResolver().bulkInsert(TaskProvider.CONTENT_URI, contentValueses);
            }

            isEmpty = true;
            contentValueses = contentValuesArrayList.toArray(new ContentValues[contentValuesArrayList.size()]);
            for (ContentValues contentValues : contentValueses) {
                if (contentValues != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                context.getContentResolver().bulkInsert(AssigneeProvider.CONTENT_URI, contentValueses);
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Download Error :" + ex.toString());
        }
    }

    //Cancelled Task
    public static void insertCancelledTask(Task[] tasks, Context context, String TAG) {
        DBHelper helper = new DBHelper(context);
        try {
            ContentValues[] contentValueses = new ContentValues[tasks.length];
            ArrayList<ContentValues> contentValuesArrayList = new ArrayList<>();
            for (int i = 0; i < tasks.length; i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.TASK_COLUMN_ASSIGNER_ID, tasks[i].getAssignerId());
                contentValues.put(DBHelper.TASK_COLUMN_ASSIGNER_NAME, tasks[i].getAssignerName());
                contentValues.put(DBHelper.TASK_COLUMN_ASSIGNER_PHOTO, tasks[i].getAssignerPhoto());
                contentValues.put(DBHelper.TASK_COLUMN_TASK_ID, tasks[i].getTaskId());
                contentValues.put(DBHelper.TASK_COLUMN_FILE, tasks[i].getFile());
                contentValues.put(DBHelper.TASK_COLUMN_CREATE_DATE, tasks[i].getCreateDate());
                contentValues.put(DBHelper.TASK_COLUMN_TITLE, tasks[i].getTaskTitle());
                contentValues.put(DBHelper.TASK_COLUMN_DESCRIPTION, tasks[i].getTaskDescription());
                contentValues.put(DBHelper.TASK_COLUMN_LOCTION, tasks[i].getLocation());
                contentValues.put(DBHelper.TASK_COLUMN_PRIORITY, tasks[i].getPriority());
                contentValues.put(DBHelper.TASK_COLUMN_MAP, tasks[i].getMap());
                contentValues.put(DBHelper.TASK_COLUMN_UPDATE_BY, tasks[i].getUpdateBy());
                contentValues.put(DBHelper.TASK_COLUMN_UPDATE_DATE, tasks[i].getUpdateDate());
                contentValues.put(DBHelper.TASK_COLUMN_DUE_DATE, tasks[i].getTaskDueDate());
                contentValues.put(DBHelper.TASK_COLUMN_COMPLETION_STATUS, tasks[i].getCompletionStatus());
                contentValues.put(DBHelper.TASK_COLUMN_DATE, tasks[i].getTaskDate());
                contentValues.put(DBHelper.TASK_COLUMN_TYPE, context.getString(R.string.cancelled_task));
                if (tasks[i].getFile() != null) {
                    String filePath = new File(Environment.getExternalStorageDirectory() + "/" + context.getResources().getString(R.string.folder_name) + "/" +
                            context.getString(R.string.task_attachment)).getAbsolutePath() + "/" + tasks[i].getTaskId() + "/" + tasks[i].getFile().split("/")[tasks[i].getFile().split("/").length - 1];
                    contentValues.put(DBHelper.TASK_COLUMN_STORED_LOCATION, filePath);
                }
                contentValues.put(DBHelper.TASK_COLUMN_STORED_LOCATION, context.getString(R.string.cancelled_task));
                contentValueses[i] = contentValues;
                for (int j = 0; j < tasks[i].getAssigneeList().size(); j++) {
                    contentValues = new ContentValues();
                    contentValues.put(DBHelper.ASSIGNEELIST_COLUMN_USER_ID, tasks[i].getAssigneeList().get(j).getUserId());
                    contentValues.put(DBHelper.ASSIGNEELIST_COLUMN_USER_NAME, tasks[i].getAssigneeList().get(j).getUserName());
                    contentValues.put(DBHelper.ASSIGNEELIST_COLUMN_CREATE_DATE, tasks[i].getAssigneeList().get(j).getCreateDate());
                    contentValues.put(DBHelper.ASSIGNEELIST_COLUMN_TASK_ID, tasks[i].getTaskId());
                    contentValues.put(DBHelper.ASSIGNEELIST_COLUMN_TASK_TYPE, context.getString(R.string.cancelled_task));
                    if (!contentValuesArrayList.contains(contentValues)) {
                        contentValuesArrayList.add(contentValues);
                    }
                }
            }
            boolean isEmpty = true;
            for (ContentValues contentValues : contentValueses) {
                if (contentValues != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                context.getContentResolver().bulkInsert(TaskProvider.CONTENT_URI, contentValueses);
            }

            isEmpty = true;
            contentValueses = contentValuesArrayList.toArray(new ContentValues[contentValuesArrayList.size()]);
            for (ContentValues contentValues : contentValueses) {
                if (contentValues != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                context.getContentResolver().bulkInsert(AssigneeProvider.CONTENT_URI, contentValueses);
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Download Error :" + ex.toString());
        }
    }

    //Created Task
    public static void insertCreatedTask(Task[] tasks, Context context, String TAG) {
        DBHelper helper = new DBHelper(context);
        try {
            ContentValues[] contentValueses = new ContentValues[tasks.length];
            ArrayList<ContentValues> contentValuesArrayList = new ArrayList<>();
            for (int i = 0; i < tasks.length; i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.TASK_COLUMN_ASSIGNER_ID, tasks[i].getAssignerId());
                contentValues.put(DBHelper.TASK_COLUMN_ASSIGNER_NAME, tasks[i].getAssignerName());
                contentValues.put(DBHelper.TASK_COLUMN_ASSIGNER_PHOTO, tasks[i].getAssignerPhoto());
                contentValues.put(DBHelper.TASK_COLUMN_TASK_ID, tasks[i].getTaskId());
                contentValues.put(DBHelper.TASK_COLUMN_FILE, tasks[i].getFile());
                contentValues.put(DBHelper.TASK_COLUMN_CREATE_DATE, tasks[i].getCreateDate());
                contentValues.put(DBHelper.TASK_COLUMN_TITLE, tasks[i].getTaskTitle());
                contentValues.put(DBHelper.TASK_COLUMN_DESCRIPTION, tasks[i].getTaskDescription());
                contentValues.put(DBHelper.TASK_COLUMN_LOCTION, tasks[i].getLocation());
                contentValues.put(DBHelper.TASK_COLUMN_PRIORITY, tasks[i].getPriority());
                contentValues.put(DBHelper.TASK_COLUMN_MAP, tasks[i].getMap());
                contentValues.put(DBHelper.TASK_COLUMN_UPDATE_BY, tasks[i].getUpdateBy());
                contentValues.put(DBHelper.TASK_COLUMN_UPDATE_DATE, tasks[i].getUpdateDate());
                contentValues.put(DBHelper.TASK_COLUMN_DUE_DATE, tasks[i].getTaskDueDate());
                contentValues.put(DBHelper.TASK_COLUMN_COMPLETION_STATUS, tasks[i].getCompletionStatus());
                contentValues.put(DBHelper.TASK_COLUMN_DATE, tasks[i].getTaskDate());
                contentValues.put(DBHelper.TASK_COLUMN_TYPE, context.getString(R.string.created_task));
                if (tasks[i].getFile() != null) {
                    String filePath = new File(Environment.getExternalStorageDirectory() + "/" + context.getResources().getString(R.string.folder_name) + "/" +
                            context.getString(R.string.task_attachment)).getAbsolutePath() + "/" + tasks[i].getTaskId() + "/" + tasks[i].getFile().split("/")[tasks[i].getFile().split("/").length - 1];
                    contentValues.put(DBHelper.TASK_COLUMN_STORED_LOCATION, filePath);
                }
                contentValueses[i] = contentValues;
                for (int j = 0; j < tasks[i].getAssigneeList().size(); j++) {
                    contentValues = new ContentValues();
                    contentValues.put(DBHelper.ASSIGNEELIST_COLUMN_USER_ID, tasks[i].getAssigneeList().get(j).getUserId());
                    contentValues.put(DBHelper.ASSIGNEELIST_COLUMN_USER_NAME, tasks[i].getAssigneeList().get(j).getUserName());
                    contentValues.put(DBHelper.ASSIGNEELIST_COLUMN_CREATE_DATE, tasks[i].getAssigneeList().get(j).getCreateDate());
                    contentValues.put(DBHelper.ASSIGNEELIST_COLUMN_TASK_ID, tasks[i].getTaskId());
                    contentValues.put(DBHelper.ASSIGNEELIST_COLUMN_TASK_TYPE, context.getString(R.string.created_task));
                    if (!contentValuesArrayList.contains(contentValues)) {
                        contentValuesArrayList.add(contentValues);
                    }
                }
            }
            boolean isEmpty = true;
            for (ContentValues contentValues : contentValueses) {
                if (contentValues != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                context.getContentResolver().bulkInsert(TaskProvider.CONTENT_URI, contentValueses);
            }

            isEmpty = true;
            contentValueses = contentValuesArrayList.toArray(new ContentValues[contentValuesArrayList.size()]);
            for (ContentValues contentValues : contentValueses) {
                if (contentValues != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                context.getContentResolver().bulkInsert(AssigneeProvider.CONTENT_URI, contentValueses);
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Download Error :" + ex.toString());
        }
    }

    //Single Task
    public static void insertSingleTask(Task task, Context context, String TAG, String type) {
        DBHelper helper = new DBHelper(context);
        try {
            ContentValues contentValues = new ContentValues();
            ArrayList<ContentValues> contentValuesArrayList = new ArrayList<>();
            contentValues.put(DBHelper.TASK_COLUMN_ASSIGNER_ID, task.getAssignerId());
            contentValues.put(DBHelper.TASK_COLUMN_ASSIGNER_NAME, task.getAssignerName());
            contentValues.put(DBHelper.TASK_COLUMN_ASSIGNER_PHOTO, task.getAssignerPhoto());
            contentValues.put(DBHelper.TASK_COLUMN_TASK_ID, task.getTaskId());
            contentValues.put(DBHelper.TASK_COLUMN_CREATE_DATE, task.getCreateDate());
            contentValues.put(DBHelper.TASK_COLUMN_TITLE, task.getTaskTitle());
            contentValues.put(DBHelper.TASK_COLUMN_DESCRIPTION, task.getTaskDescription());
            contentValues.put(DBHelper.TASK_COLUMN_LOCTION, task.getLocation());
            contentValues.put(DBHelper.TASK_COLUMN_PRIORITY, task.getPriority());
            contentValues.put(DBHelper.TASK_COLUMN_MAP, task.getMap());
            contentValues.put(DBHelper.TASK_COLUMN_UPDATE_BY, task.getUpdateBy());
            contentValues.put(DBHelper.TASK_COLUMN_UPDATE_DATE, task.getUpdateDate());
            contentValues.put(DBHelper.TASK_COLUMN_DUE_DATE, task.getTaskDueDate());
            contentValues.put(DBHelper.TASK_COLUMN_COMPLETION_STATUS, task.getCompletionStatus());
            contentValues.put(DBHelper.TASK_COLUMN_DATE, task.getTaskDate());
            contentValues.put(DBHelper.TASK_COLUMN_TYPE, type);
            if (task.getFile() != null) {
                String filePath = new File(Environment.getExternalStorageDirectory() + "/" + context.getResources().getString(R.string.folder_name) + "/" +
                        context.getString(R.string.task_attachment)).getAbsolutePath() + "/" + task.getTaskId() + "/" + task.getFile().split("/")[task.getFile().split("/").length - 1];
                contentValues.put(DBHelper.TASK_COLUMN_STORED_LOCATION, filePath);
            }
            int row = context.getContentResolver().update(TaskProvider.CONTENT_URI, contentValues, DBHelper.TASK_COLUMN_TASK_ID + "=?", new String[]{task.getTaskId().toString()});
            if (row == 0)
                context.getContentResolver().insert(TaskProvider.CONTENT_URI, contentValues);

            for (int j = 0; j < task.getAssigneeList().size(); j++) {
                contentValues = new ContentValues();
                contentValues.put(DBHelper.ASSIGNEELIST_COLUMN_USER_ID, task.getAssigneeList().get(j).getUserId());
                contentValues.put(DBHelper.ASSIGNEELIST_COLUMN_USER_NAME, task.getAssigneeList().get(j).getUserName());
                contentValues.put(DBHelper.ASSIGNEELIST_COLUMN_CREATE_DATE, task.getAssigneeList().get(j).getCreateDate());
                contentValues.put(DBHelper.ASSIGNEELIST_COLUMN_TASK_ID, task.getTaskId());
                contentValues.put(DBHelper.ASSIGNEELIST_COLUMN_TASK_TYPE, type);
                if (!contentValuesArrayList.contains(contentValues)) {
                    contentValuesArrayList.add(contentValues);
                }
            }
            boolean isEmpty = true;
            ContentValues[] contentValueses = contentValuesArrayList.toArray(new ContentValues[contentValuesArrayList.size()]);
            for (ContentValues content : contentValueses) {
                if (content != null) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                context.getContentResolver().bulkInsert(AssigneeProvider.CONTENT_URI, contentValueses);
            }
        } catch (Exception ex) {
            Log.i(TAG, "Content Provider Single Task Error :" + ex.toString());
        }
    }
}
