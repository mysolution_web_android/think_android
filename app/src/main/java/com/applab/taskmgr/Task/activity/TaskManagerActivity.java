package com.applab.taskmgr.Task.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.applab.taskmgr.CreateNewTask.activity.CreateNewTaskActivity;
import com.applab.taskmgr.Menu.fragment.NavigationDrawerFragment;
import com.applab.taskmgr.Menu.webapi.HttpHelper;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Tabs.SlidingTabLayout;
import com.applab.taskmgr.Task.adapter.TaskManagerPagerAdapter;

/**
 * -- =============================================
 * -- Author     : Vicknesh Balasubramaniam
 * -- Create date: 19/1/2016
 * -- Description: AssigneeList .java
 * -- =============================================
 * HISTORY OF UPDATE
 * <p/>
 * NO     DEVELOPER         DATETIME                      DESCRIPTION
 * *******************************************************************************
 * 1
 * 2
 */

public class TaskManagerActivity extends AppCompatActivity {
    private Toolbar mToolbar;
    private TextView mTxtTitle;
    private ViewPager mPager;
    private SlidingTabLayout mTabs;
    private int mPage = 0;
    private NavigationDrawerFragment mDrawerFragment;
    private FloatingActionButton fab;
    private String TAG = TaskManagerActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_manager);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(mCreateNewTask);
        mToolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(mToolbar);
        mTxtTitle = (TextView) mToolbar.findViewById(R.id.txtTitle);
        mTxtTitle.setText(getBaseContext().getResources().getString(R.string.title_activity_task_manager));
        mTabs = (SlidingTabLayout) findViewById(R.id.tabs);
        mTabs.setDistributeEvenly(true);
        mTabs.setCustomTabView(R.layout.custom_tab_view, R.id.tabText, R.id.tabNumber);
        mTabs.setBackgroundColor(ContextCompat.getColor(this, R.color.tabs_secondary_grey));
        mTabs.setSelectedIndicatorColors(ContextCompat.getColor(this, android.R.color.white));
        mPage = getIntent().getIntExtra("ARG", mPage);
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setOffscreenPageLimit(1);
        mPager.setAdapter(new TaskManagerPagerAdapter(getSupportFragmentManager(), getSupportLoaderManager(), this, mTabs, mPager));
        mPager.addOnPageChangeListener(mPagerOnPageChangeListener);
        mTabs.setViewPager(mPager);
        mPager.setCurrentItem(mPage);

        mDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        mDrawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        mDrawerFragment.mDrawerToggle.setDrawerIndicatorEnabled(false);
        setPage(mPage);

        mToolbar.setNavigationIcon(R.mipmap.menu);
        mToolbar.setNavigationOnClickListener(mToolbarOnClickListener);
    }

    private void setPage(int page) {
        if (page == 0) {
            mDrawerFragment.setSelectedPosition(0);
        } else if (page == 1) {
            mDrawerFragment.setSelectedPosition(1);
        } else if (page == 2) {
            mDrawerFragment.setSelectedPosition(2);
        } else if (page == 3) {
            mDrawerFragment.setSelectedPosition(3);
        } else if (page == 4) {
            mDrawerFragment.setSelectedPosition(4);
        }
    }

    public static String getPriority(int position, Context context) {
        String type = context.getString(R.string.low);
        if (position == 0) {
            type = context.getString(R.string.low);
        } else if (position == 1) {
            type = context.getString(R.string.high);
        }
        return type;
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(TAG)) {
                mPage = intent.getIntExtra("ARG", 0);
                mPager.setCurrentItem(intent.getIntExtra("ARG", 0));
                setPage(mPage);
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter iff = new IntentFilter(TAG);
        HttpHelper.getNewTaskCount(TaskManagerActivity.this, TAG);
        LocalBroadcastManager.getInstance(getBaseContext()).registerReceiver(broadcastReceiver, iff);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getBaseContext()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onBackPressed() {
        mDrawerFragment.setCloseDrawer(this);
    }

    private ViewPager.OnPageChangeListener mPagerOnPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            mPage = position;
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_task_manager, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return mDrawerFragment.mDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

    private View.OnClickListener mToolbarOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mDrawerFragment.setOpenCloseDrawer();
        }
    };

    private View.OnClickListener mCreateNewTask = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(TaskManagerActivity.this, CreateNewTaskActivity.class));
        }
    };
}
