package com.applab.taskmgr.Task.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applab.taskmgr.R;

/**
 -- =============================================
 -- Author     : Vicknesh Balasubramaniam
 -- Create date: 29/1/2016
 -- Description: AssigneeList .java
 -- =============================================
 HISTORY OF UPDATE

 NO     DEVELOPER         DATETIME                      DESCRIPTION
 ********************************************************************************
 1
 2
 */
public class TaskManagerHolder extends RecyclerView.ViewHolder {
    private TextView tv1;
    private TextView tv2;
    private ImageView imageView;
    private TextView date_time;
    private TextView assigned_by;
    private TextView priority;
    private RelativeLayout rl;
    private ImageView statusImage;

    public TaskManagerHolder(View itemView) {
        super(itemView);

        tv1 = (TextView) itemView.findViewById(R.id.list_title);
        tv2 = (TextView) itemView.findViewById(R.id.list_address);
        imageView = (ImageView) itemView.findViewById(R.id.list_avatar);

        date_time = (TextView) itemView.findViewById(R.id.date);
        assigned_by = (TextView) itemView.findViewById(R.id.assignBy);

        priority = (TextView) itemView.findViewById(R.id.priority_text);
        rl = (RelativeLayout) itemView.findViewById(R.id.rl);

        statusImage = (ImageView)itemView.findViewById(R.id.imgStatus);
    }

    public ImageView getStatusImage() {
        return statusImage;
    }

    public TextView getTv1() {
        return tv1;
    }

    public TextView getTv2() {
        return tv2;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public TextView getDate_time() {
        return date_time;
    }

    public TextView getAssigned_by() {
        return assigned_by;
    }

    public TextView getPriority() {
        return priority;
    }

    public RelativeLayout getRelativeLayout() {
        return rl;
    }
}
