package com.applab.taskmgr.Task.fragment;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.applab.taskmgr.MyTask.activity.MyTaskActivity;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Task.adapter.TaskAdapter;
import com.applab.taskmgr.Task.adapter.TaskManagerSectionAdapter;
import com.applab.taskmgr.Task.model.Task;
import com.applab.taskmgr.Task.provider.TaskProvider;
import com.applab.taskmgr.Task.webapi.HttpHelperTask;
import com.applab.taskmgr.Utilities.DBHelper;
import com.applab.taskmgr.Utilities.ItemClickSupport;
import com.applab.taskmgr.Utilities.Utilities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * -- =============================================
 * -- Author     : Vicknesh Balasubramaniam
 * -- Create date: 19/1/2016
 * -- Description: AssigneeList .java
 * -- =============================================
 * HISTORY OF UPDATE
 * <p/>
 * NO     DEVELOPER         DATETIME                      DESCRIPTION
 * *******************************************************************************
 * 1
 * 2
 */
public class MyTaskFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private TaskAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView mRecyclerView;
    private static final String TAG = MyTaskFragment.class.getSimpleName();
    private int mLoaderId = 3359;
    private Cursor mCursor;
    private TaskManagerSectionAdapter.Section[] mDummy;
    private TaskManagerSectionAdapter mSectionAdapter;
    private List<TaskManagerSectionAdapter.Section> mSections;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_task, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());

        mAdapter = new TaskAdapter(getActivity(), mCursor, getActivity().getString(R.string.my_pending_task));
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(mLayoutManager);
        setSection(0);
        ItemClickSupport.addTo(mRecyclerView).setOnItemClickListener(mItemClickListener);

        return view;
    }

    private void setSection(int position) {
        //This is the code to provide a sectioned list
        mSections = new ArrayList<>();

        //Sections
        mSections.add(new TaskManagerSectionAdapter.Section(0, getString(R.string.ongoing), R.mipmap.calendar_1));
        mSections.add(new TaskManagerSectionAdapter.Section(position, getString(R.string.upcoming), R.mipmap.clock_forward));

        //Add your adapter to the sectionAdapter
        mDummy = new TaskManagerSectionAdapter.Section[mSections.size()];
        mSectionAdapter = new TaskManagerSectionAdapter(getActivity(), R.layout.custom_section, R.id.section_text, R.id.imageSection, mAdapter);

        mSectionAdapter.setSections(mSections.toArray(mDummy));

        mRecyclerView.setAdapter(mSectionAdapter);
    }

    private void setSectionByCursor(Cursor cursor) {
        for (int i = 0; i < cursor.getCount(); i++) {
            cursor.moveToPosition(i);
            Date date = Utilities.setCalendarDate(Utilities.DATE_FORMAT, Utilities.setToLocalDateTime(Utilities.DATE_FORMAT, Utilities.DATE_FORMAT, cursor.getString(cursor.getColumnIndex(DBHelper.TASK_COLUMN_CREATE_DATE))));
            if (date.after(new Date())) {
                setSection(i);
                break;
            } else if (i == cursor.getCount() - 1) {
                setSection(i);
                break;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getSupportLoaderManager().initLoader(mLoaderId, null, this);
        HttpHelperTask.getMyTaskApi(getActivity(), TAG);
    }

    private ItemClickSupport.OnItemClickListener mItemClickListener = new ItemClickSupport.OnItemClickListener() {
        @Override
        public void onItemClicked(RecyclerView recyclerView, final int position, View v) {
            TextView txtName = (TextView) v.findViewById(R.id.list_title);
            if (txtName != null) {
                Task task = (Task) txtName.getTag();
                Intent intent = new Intent(getContext(), MyTaskActivity.class);
                intent.putExtra("Task", task);
                startActivity(intent);
            }
        }
    };


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (mLoaderId == id) {
            return new CursorLoader(getActivity(), TaskProvider.CONTENT_URI, null,
                    DBHelper.TASK_COLUMN_TYPE + "=?", new String[]{getActivity().getString(R.string.my_pending_task)}, "DATETIME(" + DBHelper.TASK_COLUMN_DATE + ")" + " ASC ");
        } else {
            return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == mLoaderId) {
            if (data != null) {
                mCursor = data;
                setSectionByCursor(mCursor);
                mAdapter.swapCursor(mCursor);
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }
}
