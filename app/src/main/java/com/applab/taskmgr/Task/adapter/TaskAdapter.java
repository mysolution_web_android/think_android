package com.applab.taskmgr.Task.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applab.taskmgr.R;
import com.applab.taskmgr.Task.activity.TaskManagerActivity;
import com.applab.taskmgr.Task.holder.TaskManagerHolder;
import com.applab.taskmgr.Task.model.Task;
import com.applab.taskmgr.Utilities.CircleTransform;
import com.applab.taskmgr.Utilities.Utilities;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

/**
 * -- =============================================
 * -- Author     : Vicknesh Balasubramaniam
 * -- Create date: 2/2/2016
 * -- Description: AssigneeList .java
 * -- =============================================
 * HISTORY OF UPDATE
 * <p/>
 * NO     DEVELOPER         DATETIME                      DESCRIPTION
 * *******************************************************************************
 * 1
 * 2
 */
public class TaskAdapter extends RecyclerView.Adapter<TaskManagerHolder> {
    private Context mContext;
    private LayoutInflater mInflater;
    private Cursor mCursor;
    private String mType;

    public TaskAdapter(Context context, Cursor cursor, String type) {
        mContext = context;
        mCursor = cursor;
        mType = type;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public TaskManagerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = mInflater.inflate(R.layout.custom_task_item_row, parent, false);
        return new TaskManagerHolder(v);
    }

    @Override
    public void onBindViewHolder(TaskManagerHolder holder, int position) {
        if (mCursor != null) {
            Task task = Task.getTask(mCursor, position);
            holder.getDate_time().setText(Utilities.setToLocalDateTime(Utilities.DATE_FORMAT, "EEEE, dd MMMM yyyy", task.getTaskDueDate()));
            holder.getTv1().setText(task.getTaskTitle());
            holder.getTv1().setTag(task);
            holder.getTv2().setText(task.getLocation());
            holder.getAssigned_by().setText(task.getAssignerName());

            Glide.with(mContext)
                    .load(task.getAssignerPhoto())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.mipmap.ic_action_person_light)
                    .transform(new CircleTransform(mContext))
                    .into(holder.getImageView());
            holder.getStatusImage().setVisibility(View.GONE);
            holder.getRelativeLayout().setVisibility(View.GONE);
            if (mType.equals(mContext.getString(R.string.completed_task))) {
                if (!task.getCompletionStatus().equals(0)) {
                    holder.getStatusImage().setImageResource(R.mipmap.check);
                    holder.getStatusImage().setVisibility(View.VISIBLE);
                } else {
                    holder.getStatusImage().setVisibility(View.GONE);
                }
            } else if (mType.equals(mContext.getString(R.string.cancelled_task))) {
                holder.getStatusImage().setImageResource(R.mipmap.decline);
                holder.getStatusImage().setVisibility(View.VISIBLE);
            } else if (mType.equals(mContext.getString(R.string.my_pending_task)) || mType.equals(mContext.getString(R.string.new_tasks))) {
                if (task.getPriority() < 2) {
                    holder.getRelativeLayout().setVisibility(View.VISIBLE);
                    holder.getPriority().setText(TaskManagerActivity.getPriority(task.getPriority(), mContext));
                    if (task.getPriority().equals(1)) {
                        holder.getRelativeLayout().setBackgroundResource(R.color.red);
                    } else if (task.getPriority().equals(0)) {
                        holder.getRelativeLayout().setBackgroundResource(R.color.hide_btn);
                    } else {
                        holder.getRelativeLayout().setVisibility(View.GONE);
                    }
                } else {
                    holder.getRelativeLayout().setVisibility(View.GONE);
                }
            }
        }
    }


    @Override
    public int getItemCount() {
        return mCursor != null ? mCursor.getCount() : 0;
    }


    public Cursor swapCursor(Cursor cursor) {
        if (this.mCursor == cursor) {
            return null;
        }
        Cursor oldCursor = this.mCursor;
        this.mCursor = cursor;
        if (cursor != null) {
            notifyDataSetChanged();
        }
        return oldCursor;
    }
}
