package com.applab.taskmgr.Task.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Arrays;
import java.util.Comparator;

/**
 -- =============================================
 -- Author     : Vicknesh Balasubramaniam
 -- Create date: 1/2/2016
 -- Description: AssigneeList .java
 -- =============================================
 HISTORY OF UPDATE

 NO     DEVELOPER         DATETIME                      DESCRIPTION
 ********************************************************************************
 1
 2
 */
public class TaskManagerSectionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Context mContext;
    private static final int SECTION_TYPE = 0;

    private boolean mValid = true;
    private int mSectionResourceId;
    private int mTextResourceId;
    private int mImageResourceId;
    private LayoutInflater mLayoutInflater;
    private RecyclerView.Adapter mBaseAdapter;
    private SparseArray<Section> mSections = new SparseArray<>();

    public TaskManagerSectionAdapter(Context context, int sectionResourceId, int textResourceId,int imageResourceId, RecyclerView.Adapter baseAdapter) {
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mSectionResourceId = sectionResourceId;
        mTextResourceId = textResourceId;
        mImageResourceId = imageResourceId;
        mBaseAdapter = baseAdapter;
        mContext = context;

        mBaseAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                mValid = mBaseAdapter.getItemCount() > 0;
                notifyDataSetChanged();
            }

            @Override
            public void onItemRangeChanged(int positionStart, int itemCount) {
                mValid = mBaseAdapter.getItemCount() > 0;
                notifyItemRangeChanged(positionStart, itemCount);
            }

            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                mValid = mBaseAdapter.getItemCount() > 0;
                notifyItemRangeChanged(positionStart, itemCount);
            }

            @Override
            public void onItemRangeRemoved(int positionStart, int itemCount) {
                mValid = mBaseAdapter.getItemCount() > 0;
                notifyItemRangeRemoved(positionStart, itemCount);
            }
        });
    }

    public static class SectionViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public ImageView image;

        public SectionViewHolder(View view, int mTextResourceid,int mImageResourceid) {
            super(view);
            title = (TextView) view.findViewById(mTextResourceid);
            image = (ImageView) view.findViewById(mImageResourceid);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int typeView) {
        if (typeView == SECTION_TYPE) {
            final View view = LayoutInflater.from(mContext).inflate(mSectionResourceId, parent, false);
            return new SectionViewHolder(view, mTextResourceId,mImageResourceId);
        } else {
            return mBaseAdapter.onCreateViewHolder(parent, typeView - 1);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder sectionViewHolder, int positon) {
        if (isSectionHeaderPosition(positon)) {
            ((SectionViewHolder) sectionViewHolder).title.setText(mSections.get(positon).title);
            ((SectionViewHolder)sectionViewHolder).image.setImageResource(mSections.get(positon).image);
        } else {
            mBaseAdapter.onBindViewHolder(sectionViewHolder, sectionedPositonToPosition(positon));
        }
    }

    @Override
    public int getItemViewType(int position) {
        return isSectionHeaderPosition(position)
                ? SECTION_TYPE
                : mBaseAdapter.getItemViewType(sectionedPositonToPosition(position)) + 1;
    }

    public static class Section {
        int firstPositon;
        int secondPosition;
        CharSequence title;
        int image;

        public Section(int firstPositon, CharSequence title,int image) {
            this.firstPositon = firstPositon;
            this.title = title;
            this.image = image;
        }

        public CharSequence getTitle() {
            return title;
        }
    }

    public void setSections(Section[] sections) {
        mSections.clear();

        Arrays.sort(sections, new Comparator<Section>() {
            @Override
            public int compare(Section o, Section o1) {
                return (o.firstPositon == o1.firstPositon)
                        ? 0
                        : ((o.firstPositon < o1.firstPositon) ? -1 : 1);
            }
        });

        int offset = 0; // offset positions for the headers we're adding

        for (Section section : sections) {
            section.secondPosition = section.firstPositon + offset;
            mSections.append(section.secondPosition, section);
            ++offset;
        }

        notifyDataSetChanged();
    }

    public int positionToSectionedPosition(int position) {
        int offset = 0;
        for (int i = 0; i < mSections.size(); i++) {
            if (mSections.valueAt(i).firstPositon > position) {
                break;
            }
            ++offset;
        }

        return position + offset;
    }

    public int sectionedPositonToPosition(int sectionPosition) {
        if (isSectionHeaderPosition(sectionPosition)) {
            return RecyclerView.NO_POSITION;
        }

        int offset = 0;
        for (int i = 0; i < mSections.size(); i++) {
            if (mSections.valueAt(i).secondPosition > sectionPosition) {
                break;
            }
            --offset;
        }
        return sectionPosition + offset;
    }

    public boolean isSectionHeaderPosition(int position) {
        return mSections.get(position) != null;
    }

    @Override
    public long getItemId(int position) {
        return isSectionHeaderPosition(position)
                ? Integer.MAX_VALUE - mSections.indexOfKey(position)
                : mBaseAdapter.getItemId(sectionedPositonToPosition(position));
    }

    @Override
    public int getItemCount() {
        return (mValid ? mBaseAdapter.getItemCount() + mSections.size() : 0);
    }
}
