package com.applab.taskmgr.Task.adapter;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;

import com.applab.taskmgr.Menu.provider.MenuProvider;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Tabs.SlidingTabLayout;
import com.applab.taskmgr.Task.fragment.CancelledFragment;
import com.applab.taskmgr.Task.fragment.CompletedFragment;
import com.applab.taskmgr.Task.fragment.CreatedTaskFragment;
import com.applab.taskmgr.Task.fragment.MyTaskFragment;
import com.applab.taskmgr.Task.fragment.NewTaskFragment;
import com.applab.taskmgr.Utilities.DBHelper;

public class TaskManagerPagerAdapter extends FragmentPagerAdapter implements LoaderManager.LoaderCallbacks<Cursor> {
    String[] mTabs;
    Context mContext;
    ViewPager mPager;
    SlidingTabLayout mSlidingTabLayout;
    private int mLoaderId = 2212;

    public TaskManagerPagerAdapter(FragmentManager fm, LoaderManager loaderManager, Context context, SlidingTabLayout slidingTabLayout, ViewPager viewPager) {
        super(fm);
        mContext = context;
        mTabs = context.getResources().getStringArray(R.array.task_tabs);
        mSlidingTabLayout = slidingTabLayout;
        mPager = viewPager;
        loaderManager.initLoader(mLoaderId, null, TaskManagerPagerAdapter.this);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment item = null;
        MyTaskFragment myTaskFragment = new MyTaskFragment();
        NewTaskFragment newFragment = new NewTaskFragment();
        CompletedFragment completedFragment = new CompletedFragment();
        CancelledFragment cancelledFragment = new CancelledFragment();
        CreatedTaskFragment createdTaskFragment = new CreatedTaskFragment();
        if (position == 0) {
            item = myTaskFragment;
        } else if (position == 1) {
            item = newFragment;
        } else if (position == 2) {
            item = completedFragment;
        } else if (position == 3) {
            item = cancelledFragment;
        } else {
            item = createdTaskFragment;
        }
        return item;
    }

    @Override
    public int getCount() {
        return mContext.getResources().getStringArray(R.array.task_tabs).length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTabs[position];
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (mLoaderId == id) {
            return new CursorLoader(mContext, MenuProvider.CONTENT_URI, null, DBHelper.MENU_COLUMN_TITLE + "=?", new String[]{mContext.getString(R.string.title_new_task)}, null);
        } else {
            return null;
        }

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (mLoaderId == loader.getId()) {
            if (data != null) {
                if (data.getCount() > 0) {
                    data.moveToPosition(0);
                    if (data.getInt(data.getColumnIndex(DBHelper.MENU_COLUMN_NO)) > 0) {
                        int no = data.getInt(data.getColumnIndex(DBHelper.MENU_COLUMN_NO));
                        String temp = "/" + String.valueOf(no);
                        if (no > 9) {
                            temp = "/+9";
                        }
                        mTabs[1] = mContext.getResources().getStringArray(R.array.task_tabs)[1] + temp;
                        this.notifyDataSetChanged();
                        mSlidingTabLayout.setViewPager(mPager);
                    }else{
                        mTabs[1] = mContext.getResources().getStringArray(R.array.task_tabs)[1];
                        this.notifyDataSetChanged();
                        mSlidingTabLayout.setViewPager(mPager);
                    }
                }
            }
        }
    }


    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}