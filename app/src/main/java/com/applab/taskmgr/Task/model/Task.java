package com.applab.taskmgr.Task.model;


import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.applab.taskmgr.Utilities.DBHelper;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * -- =============================================
 * -- Author     : Vicknesh Balasubramaniam
 * -- Create date: 20/1/2016
 * -- Description: AssigneeList .java
 * -- =============================================
 * HISTORY OF UPDATE
 * <p/>
 * NO     DEVELOPER         DATETIME                      DESCRIPTION
 * *******************************************************************************
 * 1
 * 2
 */

public class Task implements Parcelable {

    public Task() {
    }

    public Task(Parcel in) {
        readFromParcel(in);
    }

    @SerializedName("TaskId")
    @Expose
    private Integer TaskId;
    @SerializedName("TaskTitle")
    @Expose
    private String TaskTitle;
    @SerializedName("TaskDescription")
    @Expose
    private String TaskDescription;
    @SerializedName("TaskDate")
    @Expose
    private String TaskDate;
    @SerializedName("TaskDueDate")
    @Expose
    private String TaskDueDate;
    @SerializedName("Location")
    @Expose
    private String Location;
    @SerializedName("Map")
    @Expose
    private String Map;
    @SerializedName("Priority")
    @Expose
    private Integer Priority;
    @SerializedName("File")
    @Expose
    private String File;
    @SerializedName("AssignerPhoto")
    @Expose
    private String AssignerPhoto;
    @SerializedName("AssignerId")
    @Expose
    private String AssignerId;
    @SerializedName("AssignerName")
    @Expose
    private String AssignerName;
    @SerializedName("CompletionStatus")
    @Expose
    private Integer CompletionStatus;
    @SerializedName("CreateDate")
    @Expose
    private String CreateDate;
    @SerializedName("UpdateBy")
    @Expose
    private String UpdateBy;
    @SerializedName("UpdateDate")
    @Expose
    private String UpdateDate;
    @SerializedName("AssigneeList")
    @Expose
    private List<com.applab.taskmgr.Task.model.AsigneeList> AssigneeList = new ArrayList<com.applab.taskmgr.Task.model.AsigneeList>();

    @SerializedName("Check")
    @Expose
    private Integer Check;

    @SerializedName("Type")
    @Expose
    private String type;

    @SerializedName("StoredLocation")
    @Expose
    private String storedLocation;

    public String getStoredLocation() {
        return storedLocation;
    }

    public void setStoredLocation(String storedLocation) {
        this.storedLocation = storedLocation;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getCheck() {
        return Check;
    }

    public void setCheck(Integer check) {
        Check = check;
    }

    /**
     * @return The TaskId
     */
    public Integer getTaskId() {
        return TaskId;
    }

    /**
     * @param TaskId The TaskId
     */
    public void setTaskId(Integer TaskId) {
        this.TaskId = TaskId;
    }

    /**
     * @return The TaskTitle
     */
    public String getTaskTitle() {
        return TaskTitle;
    }

    /**
     * @param TaskTitle The TaskTitle
     */
    public void setTaskTitle(String TaskTitle) {
        this.TaskTitle = TaskTitle;
    }

    /**
     * @return The TaskDescription
     */
    public String getTaskDescription() {
        return TaskDescription;
    }

    /**
     * @param TaskDescription The TaskDescription
     */
    public void setTaskDescription(String TaskDescription) {
        this.TaskDescription = TaskDescription;
    }

    /**
     * @return The TaskDate
     */
    public String getTaskDate() {
        return TaskDate;
    }

    /**
     * @param TaskDate The TaskDate
     */
    public void setTaskDate(String TaskDate) {
        this.TaskDate = TaskDate;
    }

    /**
     * @return The TaskDueDate
     */
    public String getTaskDueDate() {
        return TaskDueDate;
    }

    /**
     * @param TaskDueDate The TaskDueDate
     */
    public void setTaskDueDate(String TaskDueDate) {
        this.TaskDueDate = TaskDueDate;
    }

    /**
     * @return The Location
     */
    public String getLocation() {
        return Location;
    }

    /**
     * @param Location The Location
     */
    public void setLocation(String Location) {
        this.Location = Location;
    }

    /**
     * @return The Map
     */
    public String getMap() {
        return Map;
    }

    /**
     * @param Map The Map
     */
    public void setMap(String Map) {
        this.Map = Map;
    }

    /**
     * @return The Priority
     */
    public Integer getPriority() {
        return Priority;
    }

    /**
     * @param Priority The Priority
     */
    public void setPriority(Integer Priority) {
        this.Priority = Priority;
    }

    /**
     * @return The File
     */
    public String getFile() {
        return File;
    }

    /**
     * @param File The File
     */
    public void setFile(String File) {
        this.File = File;
    }

    /**
     * @return The AssignerPhoto
     */
    public String getAssignerPhoto() {
        return AssignerPhoto;
    }

    /**
     * @param AssignerPhoto The AssignerPhoto
     */
    public void setAssignerPhoto(String AssignerPhoto) {
        this.AssignerPhoto = AssignerPhoto;
    }

    /**
     * @return The AssignerId
     */
    public String getAssignerId() {
        return AssignerId;
    }

    /**
     * @param AssignerId The AssignerId
     */
    public void setAssignerId(String AssignerId) {
        this.AssignerId = AssignerId;
    }

    /**
     * @return The AssignerName
     */
    public String getAssignerName() {
        return AssignerName;
    }

    /**
     * @param AssignerName The AssignerName
     */
    public void setAssignerName(String AssignerName) {
        this.AssignerName = AssignerName;
    }

    /**
     * @return The CompletionStatus
     */
    public Integer getCompletionStatus() {
        return CompletionStatus;
    }

    /**
     * @param CompletionStatus The CompletionStatus
     */
    public void setCompletionStatus(Integer CompletionStatus) {
        this.CompletionStatus = CompletionStatus;
    }

    /**
     * @return The CreateDate
     */
    public String getCreateDate() {
        return CreateDate;
    }

    /**
     * @param CreateDate The CreateDate
     */
    public void setCreateDate(String CreateDate) {
        this.CreateDate = CreateDate;
    }

    /**
     * @return The UpdateBy
     */
    public String getUpdateBy() {
        return UpdateBy;
    }

    /**
     * @param UpdateBy The UpdateBy
     */
    public void setUpdateBy(String UpdateBy) {
        this.UpdateBy = UpdateBy;
    }

    /**
     * @return The UpdateDate
     */
    public String getUpdateDate() {
        return UpdateDate;
    }

    /**
     * @param UpdateDate The UpdateDate
     */
    public void setUpdateDate(String UpdateDate) {
        this.UpdateDate = UpdateDate;
    }

    /**
     * @return The AssigneeList
     */
    public List<com.applab.taskmgr.Task.model.AsigneeList> getAssigneeList() {
        return AssigneeList;
    }

    /**
     * @param AssigneeList The AssigneeList
     */
    public void setAssigneeList(List<com.applab.taskmgr.Task.model.AsigneeList> AssigneeList) {
        this.AssigneeList = AssigneeList;
    }


    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.TaskId);
        dest.writeString(this.TaskTitle);
        dest.writeString(this.TaskDescription);
        dest.writeString(this.TaskDate);
        dest.writeString(this.TaskDueDate);
        dest.writeString(this.Location);
        dest.writeString(this.Map);
        dest.writeInt(this.Priority);
        dest.writeString(this.File);
        dest.writeString(this.AssignerPhoto);
        dest.writeString(this.AssignerId);
        dest.writeString(this.AssignerName);
        dest.writeInt(this.CompletionStatus);
        dest.writeString(this.CreateDate);
        dest.writeString(this.UpdateBy);
        dest.writeString(this.UpdateDate);
        dest.writeInt(this.Check);
        dest.writeString(this.type);
        dest.writeString(this.storedLocation);
    }

    /**
     * Called from the constructor to create this
     * object from a parcel.
     *
     * @param in parcel from which to re-create object.
     */
    public void readFromParcel(Parcel in) {
        this.TaskId = in.readInt();
        this.TaskTitle = in.readString();
        this.TaskDescription = in.readString();
        this.TaskDate = in.readString();
        this.TaskDueDate = in.readString();
        this.Location = in.readString();
        this.Map = in.readString();
        this.Priority = in.readInt();
        this.File = in.readString();
        this.AssignerPhoto = in.readString();
        this.AssignerId = in.readString();
        this.AssignerName = in.readString();
        this.CompletionStatus = in.readInt();
        this.CreateDate = in.readString();
        this.UpdateBy = in.readString();
        this.UpdateDate = in.readString();
        this.Check = in.readInt();
        this.type = in.readString();
        this.storedLocation = in.readString();
    }

    @SuppressWarnings("unchecked")
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public Task createFromParcel(Parcel in) {
            return new Task(in);
        }

        @Override
        public Task[] newArray(int size) {
            return new Task[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    public static Task getTask(Cursor cursor, int position) {
        cursor.moveToPosition(position);
        Task task = new Task();
        task.setTaskId(cursor.getInt(cursor.getColumnIndex(DBHelper.TASK_COLUMN_TASK_ID)));
        task.setLocation(cursor.getString(cursor.getColumnIndex(DBHelper.TASK_COLUMN_LOCTION)));
        task.setAssignerName(cursor.getString(cursor.getColumnIndex(DBHelper.TASK_COLUMN_ASSIGNER_NAME)));
        task.setTaskTitle(cursor.getString(cursor.getColumnIndex(DBHelper.TASK_COLUMN_TITLE)));
        task.setTaskDate(cursor.getString(cursor.getColumnIndex(DBHelper.TASK_COLUMN_DATE)));
        task.setTaskDueDate(cursor.getString(cursor.getColumnIndex(DBHelper.TASK_COLUMN_DUE_DATE)));
        task.setCreateDate(cursor.getString(cursor.getColumnIndex(DBHelper.TASK_COLUMN_CREATE_DATE)));
        task.setCompletionStatus(cursor.getInt(cursor.getColumnIndex(DBHelper.TASK_COLUMN_COMPLETION_STATUS)));
        task.setMap(cursor.getString(cursor.getColumnIndex(DBHelper.TASK_COLUMN_MAP)));
        task.setAssignerId(cursor.getString(cursor.getColumnIndex(DBHelper.TASK_COLUMN_ASSIGNER_ID)));
        task.setAssignerPhoto(cursor.getString(cursor.getColumnIndex(DBHelper.TASK_COLUMN_ASSIGNER_PHOTO)));
        task.setPriority(cursor.getInt(cursor.getColumnIndex(DBHelper.TASK_COLUMN_PRIORITY)));
        task.setFile(cursor.getString(cursor.getColumnIndex(DBHelper.TASK_COLUMN_FILE)));
        task.setUpdateBy(cursor.getString(cursor.getColumnIndex(DBHelper.TASK_COLUMN_UPDATE_BY)));
        task.setUpdateDate(cursor.getString(cursor.getColumnIndex(DBHelper.TASK_COLUMN_UPDATE_DATE)));
        task.setTaskDescription(cursor.getString(cursor.getColumnIndex(DBHelper.TASK_COLUMN_DESCRIPTION)));
        task.setType(cursor.getString(cursor.getColumnIndex(DBHelper.TASK_COLUMN_TYPE)));
        task.setStoredLocation(cursor.getString(cursor.getColumnIndex(DBHelper.TASK_COLUMN_STORED_LOCATION)));
        if (cursor.isNull(cursor.getColumnIndex(DBHelper.TASK_COLUMN_LAST_CHECK_STATUS))) {
            task.setCheck(1);
        } else {
            task.setCheck(cursor.getInt(cursor.getColumnIndex(DBHelper.TASK_COLUMN_LAST_CHECK_STATUS)));
            if (task.getCheck() == 1) {
                task.setCheck(0);
            } else {
                task.setCheck(1);
            }
        }
        return task;
    }
}


