package com.applab.taskmgr.Task.webapi;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.applab.taskmgr.Login.database.CRUDHelper;
import com.applab.taskmgr.Login.model.Token;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Task.model.Task;
import com.applab.taskmgr.Task.provider.AssigneeProvider;
import com.applab.taskmgr.Task.provider.TaskProvider;
import com.applab.taskmgr.Utilities.AppController;
import com.applab.taskmgr.Utilities.DBHelper;
import com.applab.taskmgr.Utilities.GsonRequest;
import com.applab.taskmgr.Utilities.Utilities;

import java.util.HashMap;
import java.util.Map;

/**
 * -- =============================================
 * -- Author     : Vicknesh Balasubramaniam
 * -- Create date: 15/1/2016
 * -- Description: AssigneeList .java
 * -- =============================================
 * HISTORY OF UPDATE
 * <p/>
 * NO     DEVELOPER         DATETIME                      DESCRIPTION
 * *******************************************************************************
 * 1
 * 2
 */

public class HttpHelperTask {
    /*GET(WebAPI) Method Seperator*/
    //NewTask
    public static void getNewTaskAPI(Context context, String TAG) {
        final Token token = CRUDHelper.getToken(context);
        if (token != null) {
            if (token.getToken() != null) {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", token.getToken());
                GsonRequest<Task[]> mGsonRequest = new GsonRequest<Task[]>(
                        Request.Method.GET,
                        context.getString(R.string.base_url) + "Task/NewTask",
                        Task[].class,
                        params,
                        responseNewTask(context, TAG),
                        errorNewTask(context, TAG)) {
                };
                mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utilities.TIMEOUT,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
            }
        }
    }

    //My Task
    public static void getMyTaskApi(Context context, String TAG) {
        Token token = CRUDHelper.getToken(context);
        if (token != null) {
            if (token.getToken() != null) {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", token.getToken());
                GsonRequest<Task[]> mGsonRequest = new GsonRequest<Task[]>(
                        Request.Method.GET,
                        context.getString(R.string.base_url) + "Task/MyPendingTask",
                        Task[].class,
                        params,
                        responseMyTask(context, TAG),
                        errorMyTask(context, TAG)) {
                };
                mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utilities.TIMEOUT,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
            }
        }
    }

    //Completed Task
    public static void getCompletedTaskApi(Context context, String TAG) {
        Token token = CRUDHelper.getToken(context);
        if (token != null) {
            if (token.getToken() != null) {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", token.getToken());
                GsonRequest<Task[]> mGsonRequest = new GsonRequest<Task[]>(
                        Request.Method.GET,
                        context.getString(R.string.base_url) + "Task/MyCompletedTask",
                        Task[].class,
                        params,
                        responseCompletedTask(context, TAG),
                        errorCompletedTask(context, TAG)) {
                };
                mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utilities.TIMEOUT,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
            }
        }
    }

    //Cancelled Task
    public static void getCancelledTaskApi(Context context, String TAG) {
        Token token = CRUDHelper.getToken(context);
        if (token != null) {
            if (token.getToken() != null) {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", token.getToken());
                GsonRequest<Task[]> mGsonRequest = new GsonRequest<Task[]>(
                        Request.Method.GET,
                        context.getString(R.string.base_url) + "Task/MyCancelledTask",
                        Task[].class,
                        params,
                        responseCancelledTask(context, TAG),
                        errorCancelledTask(context, TAG)) {
                };
                mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utilities.TIMEOUT,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
            }
        }
    }

    //Created Task
    public static void getCreatedTaskApi(Context context, String TAG) {
        Token token = CRUDHelper.getToken(context);
        if (token != null) {
            if (token.getToken() != null) {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", token.getToken());
                GsonRequest<Task[]> mGsonRequest = new GsonRequest<Task[]>(
                        Request.Method.GET,
                        context.getString(R.string.base_url) + "Task/MyCreatedTask",
                        Task[].class,
                        params,
                        responseCreatedTask(context, TAG),
                        errorCreatedTask(context, TAG)) {
                };
                mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utilities.TIMEOUT,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
            }
        }
    }

    //Single Task
    public static void getSingleTask(Context context, String TAG, String taskId, String type) {
        Token token = CRUDHelper.getToken(context);
        if (token != null) {
            if (token.getToken() != null) {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", token.getToken());
                GsonRequest<Task> mGsonRequest = new GsonRequest<Task>(
                        Request.Method.GET,
                        context.getString(R.string.base_url) + "Task/SingleTask?task_id=" + Utilities.encode(taskId),
                        Task.class,
                        params,
                        responseSingleTask(context, TAG, taskId, type),
                        errorSingleTask(context, TAG)) {
                };
                mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utilities.TIMEOUT,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
            }
        }
    }

    /*Seperator for Response*/
    //New Task
    public static Response.Listener<Task[]> responseNewTask(final Context context, final String TAG) {
        return new Response.Listener<Task[]>() {
            @Override
            public void onResponse(Task[] response) {
                context.getContentResolver().delete(TaskProvider.CONTENT_URI, DBHelper.TASK_COLUMN_TYPE + "=?", new String[]{context.getString(R.string.new_tasks)});
                context.getContentResolver().delete(AssigneeProvider.CONTENT_URI, DBHelper.ASSIGNEELIST_COLUMN_TASK_TYPE + "=?", new String[]{context.getString(R.string.new_tasks)});
                com.applab.taskmgr.Task.database.CRUDHelper.insertNewTask(response, context, TAG);
                Log.i(TAG, "Success- " + response);
            }
        };
    }

    public static Response.ErrorListener errorNewTask(final Context context, final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Intent intent = new Intent(TAG);
                intent.putExtra("Error", Utilities.PROCEED_FAILED);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }
        };
    }

    //MyTask
    public static Response.Listener<Task[]> responseMyTask(final Context context, final String TAG) {
        return new Response.Listener<Task[]>() {
            @Override
            public void onResponse(Task[] response) {
                Log.i(TAG, "Message: " + response.toString());
                context.getContentResolver().delete(TaskProvider.CONTENT_URI, DBHelper.TASK_COLUMN_TYPE + "=?", new String[]{context.getString(R.string.my_pending_task)});
                context.getContentResolver().delete(AssigneeProvider.CONTENT_URI, DBHelper.ASSIGNEELIST_COLUMN_TASK_TYPE + "=?", new String[]{context.getString(R.string.my_pending_task)});
                com.applab.taskmgr.Task.database.CRUDHelper.insertMyTask(response, context, TAG);
            }
        };
    }

    public static Response.ErrorListener errorMyTask(final Context context, final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Intent intent = new Intent(TAG);
                intent.putExtra("Error", Utilities.PROCEED_FAILED);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }
        };
    }

    //Completed Task
    public static Response.Listener<Task[]> responseCompletedTask(final Context context, final String TAG) {
        return new Response.Listener<Task[]>() {
            @Override
            public void onResponse(Task[] response) {
                context.getContentResolver().delete(TaskProvider.CONTENT_URI, DBHelper.TASK_COLUMN_TYPE + "=?", new String[]{context.getString(R.string.completed_task)});
                context.getContentResolver().delete(AssigneeProvider.CONTENT_URI, DBHelper.ASSIGNEELIST_COLUMN_TASK_TYPE + "=?", new String[]{context.getString(R.string.completed_task)});
                com.applab.taskmgr.Task.database.CRUDHelper.insertCompletedTask(response, context, TAG);
            }
        };
    }

    public static Response.ErrorListener errorCompletedTask(final Context context, final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Intent intent = new Intent(TAG);
                intent.putExtra("Error", Utilities.PROCEED_FAILED);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }
        };
    }

    //Cancelled Task
    public static Response.Listener<Task[]> responseCancelledTask(final Context context, final String TAG) {
        return new Response.Listener<Task[]>() {
            @Override
            public void onResponse(Task[] response) {
                context.getContentResolver().delete(TaskProvider.CONTENT_URI, DBHelper.TASK_COLUMN_TYPE + "=?", new String[]{context.getString(R.string.cancelled_task)});
                context.getContentResolver().delete(AssigneeProvider.CONTENT_URI, DBHelper.ASSIGNEELIST_COLUMN_TASK_TYPE + "=?", new String[]{context.getString(R.string.cancelled_task)});
                com.applab.taskmgr.Task.database.CRUDHelper.insertCancelledTask(response, context, TAG);
            }
        };
    }

    public static Response.ErrorListener errorCancelledTask(final Context context, final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Intent intent = new Intent(TAG);
                intent.putExtra("Error", Utilities.PROCEED_FAILED);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }
        };
    }

    //Created Task
    public static Response.Listener<Task[]> responseCreatedTask(final Context context, final String TAG) {
        return new Response.Listener<Task[]>() {
            @Override
            public void onResponse(Task[] response) {
                context.getContentResolver().delete(TaskProvider.CONTENT_URI, DBHelper.TASK_COLUMN_TYPE + "=?", new String[]{context.getString(R.string.created_task)});
                context.getContentResolver().delete(AssigneeProvider.CONTENT_URI, DBHelper.ASSIGNEELIST_COLUMN_TASK_TYPE + "=?", new String[]{context.getString(R.string.created_task)});
                com.applab.taskmgr.Task.database.CRUDHelper.insertCreatedTask(response, context, TAG);
            }
        };
    }

    public static Response.ErrorListener errorCreatedTask(final Context context, final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Intent intent = new Intent(TAG);
                intent.putExtra("Error", Utilities.PROCEED_FAILED);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }
        };
    }

    //Single Task
    public static Response.Listener<Task> responseSingleTask(final Context context, final String TAG, final String taskId, final String type) {
        return new Response.Listener<Task>() {
            @Override
            public void onResponse(Task response) {
                context.getContentResolver().delete(TaskProvider.CONTENT_URI, DBHelper.TASK_COLUMN_TASK_ID + "=?", new String[]{taskId});
                context.getContentResolver().delete(AssigneeProvider.CONTENT_URI, DBHelper.ASSIGNEELIST_COLUMN_TASK_ID + "=?", new String[]{taskId});
                com.applab.taskmgr.Task.database.CRUDHelper.insertSingleTask(response, context, TAG, type);
            }
        };
    }

    public static Response.ErrorListener errorSingleTask(final Context context, final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utilities.serverHandlingError(context, error);
            }
        };
    }

}
