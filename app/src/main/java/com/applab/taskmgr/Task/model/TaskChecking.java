package com.applab.taskmgr.Task.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * -- =============================================
 * -- Author     : Vicknesh Balasubramaniam
 * -- Create date: 28/1/2016
 * -- Description: AssigneeList .java
 * -- =============================================
 * HISTORY OF UPDATE
 * <p/>
 * NO     DEVELOPER         DATETIME                      DESCRIPTION
 * *******************************************************************************
 * 1
 * 2
 */

public class TaskChecking implements Parcelable {
    private String checkIn;
    private String checkOut;
    private String imgProfile;

    public TaskChecking() {

    }

    public String getImgProfile() {
        return imgProfile;
    }

    public void setImgProfile(String imgProfile) {
        this.imgProfile = imgProfile;
    }

    public TaskChecking(Parcel in) {
        readFromParcel(in);
    }

    public String getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(String checkIn) {
        this.checkIn = checkIn;
    }

    public String getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(String checkOut) {
        this.checkOut = checkOut;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.checkIn);
        dest.writeString(this.checkOut);
        dest.writeString(this.imgProfile);
    }

    /**
     * Called from the constructor to create this
     * object from a parcel.
     *
     * @param in parcel from which to re-create object.
     */
    public void readFromParcel(Parcel in) {
        this.checkIn = in.readString();
        this.checkOut = in.readString();
        this.imgProfile = in.readString();
    }

    @SuppressWarnings("unchecked")
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public TaskChecking createFromParcel(Parcel in) {
            return new TaskChecking(in);
        }

        @Override
        public TaskChecking[] newArray(int size) {
            return new TaskChecking[size];
        }
    };

}
