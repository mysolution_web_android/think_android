package com.applab.taskmgr.Task.model;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.applab.taskmgr.Utilities.DBHelper;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * -- =============================================
 * -- Author     : Vicknesh Balasubramaniam
 * -- Create date: 17/2/2016
 * -- Description: AssigneeList .java
 * -- =============================================
 * HISTORY OF UPDATE
 * <p/>
 * NO     DEVELOPER         DATETIME                      DESCRIPTION
 * *******************************************************************************
 * 1
 * 2
 */
public class AsigneeList implements Parcelable {
    @SerializedName("UserId")
    @Expose
    private String UserId;
    @SerializedName("UserName")
    @Expose
    private String UserName;
    @SerializedName("CreateDate")
    @Expose
    private String CreateDate;

    public AsigneeList() {
    }

    ;

    public AsigneeList(Parcel in) {
        readFromParcel(in);
    }

    /**
     * @return The UserId
     */
    public String getUserId() {
        return UserId;
    }

    /**
     * @param UserId The UserId
     */
    public void setUserId(String UserId) {
        this.UserId = UserId;
    }

    /**
     * @return The UserName
     */
    public String getUserName() {
        return UserName;
    }

    /**
     * @param UserName The UserName
     */
    public void setUserName(String UserName) {
        this.UserName = UserName;
    }

    /**
     * @return The CreateDate
     */
    public String getCreateDate() {
        return CreateDate;
    }

    /**
     * @param CreateDate The CreateDate
     */
    public void setCreateDate(String CreateDate) {
        this.CreateDate = CreateDate;
    }

    public void readFromParcel(Parcel in) {
        this.UserId = in.readString();
        this.UserName = in.readString();
        this.CreateDate = in.readString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.UserId);
        dest.writeString(this.UserName);
        dest.writeString(this.CreateDate);
    }

    @SuppressWarnings("unchecked")
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public AsigneeList createFromParcel(Parcel in) {
            return new AsigneeList(in);
        }

        @Override
        public AsigneeList[] newArray(int size) {
            return new AsigneeList[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    public static AsigneeList getAssigneeList(Cursor cursor, int position) {
        cursor.moveToPosition(position);
        AsigneeList asigneeList = new AsigneeList();
        asigneeList.setUserId(cursor.getString(cursor.getColumnIndex(DBHelper.ASSIGNEELIST_COLUMN_USER_ID)));
        asigneeList.setUserName(cursor.getString(cursor.getColumnIndex(DBHelper.ASSIGNEELIST_COLUMN_USER_NAME)));
        asigneeList.setCreateDate(cursor.getString(cursor.getColumnIndex(DBHelper.ASSIGNEELIST_COLUMN_CREATE_DATE)));
        return asigneeList;
    }
}
