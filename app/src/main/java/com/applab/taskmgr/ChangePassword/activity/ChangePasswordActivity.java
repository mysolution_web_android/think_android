package com.applab.taskmgr.ChangePassword.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.applab.taskmgr.ChangePassword.webapi.HttpHelper;
import com.applab.taskmgr.Menu.fragment.NavigationDrawerFragment;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Utilities.ItemClickSupport;
import com.applab.taskmgr.Utilities.Utilities;

/**
 * -- =============================================
 * -- Author     : Vicknesh Balasubramaniam
 * -- Create date: 24/1/2016
 * -- Description: AssigneeList .java
 * -- =============================================
 * HISTORY OF UPDATE
 * <p/>
 * NO     DEVELOPER         DATETIME                      DESCRIPTION
 * *******************************************************************************
 * 1
 * 2
 */

public class ChangePasswordActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private TextView mTxtToolbarTitle;

    private NavigationDrawerFragment mDrawerFragment;
    private DrawerLayout mDrawerLayout;

    private EditText editCurrentPassword;
    private EditText editNewPassword;
    private EditText editConfirmPassword;

    private LinearLayout btnSaveNewPassword, btnCancel;

    private RelativeLayout mRl;
    private ProgressBar mProgressBar;

    private String TAG = ChangePasswordActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        editCurrentPassword = (EditText) findViewById(R.id.editCurrentPassword);
        editNewPassword = (EditText) findViewById(R.id.editNewPassword);
        editConfirmPassword = (EditText) findViewById(R.id.editConfirmPassword);

        btnSaveNewPassword = (LinearLayout) findViewById(R.id.btnSave);
        btnCancel = (LinearLayout) findViewById(R.id.btnCancel);

        mRl = (RelativeLayout) findViewById(R.id.fadeRL);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        mToolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(mToolbar);
        mTxtToolbarTitle = (TextView) mToolbar.findViewById(R.id.txtTitle);
        mTxtToolbarTitle.setText(getString(R.string.title_activity_change_password));

        mDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        mDrawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        mDrawerFragment.setSelectedPosition(6);
        mDrawerFragment.mDrawerToggle.setDrawerIndicatorEnabled(false);

        mToolbar.setNavigationIcon(R.mipmap.menu);
        mToolbar.setNavigationOnClickListener(mToolbarOnClickListener);

        btnSaveNewPassword.setOnClickListener(mBtnSaveNewPassword);
        btnCancel.setOnClickListener(mBtnCancalOnClickListener);
    }

    private View.OnClickListener mBtnCancalOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    private View.OnClickListener mBtnSaveNewPassword = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String oldPassword = editCurrentPassword.getText().toString();
            String newPassword = editNewPassword.getText().toString();
            String confirmPassword = editConfirmPassword.getText().toString();

            if (oldPassword.length() == 0 || newPassword.length() == 0 || confirmPassword.length() == 0) {
                Utilities.showError(ChangePasswordActivity.this,Utilities.CODE_PASSWORD_FIELD,Utilities.PASSWORD_FIELD);
            } else {
                if (newPassword.equals(confirmPassword)) {
                    Utilities.setFadeProgressBarVisibility(true, mRl, mProgressBar);
                    HttpHelper.putChangePassword(ChangePasswordActivity.this, TAG,
                            oldPassword, newPassword, confirmPassword);
                } else {
                    Utilities.showError(ChangePasswordActivity.this, Utilities.CODE_PASSWORD_NOT_MATCH, Utilities.PASSWORD_NOT_MATCH);
                }
            }

        }
    };

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(TAG)) {
                if (intent.getBooleanExtra("failed", false)) {
                    Utilities.setFadeProgressBarVisibility(false, mRl, mProgressBar);
                }
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter iff = new IntentFilter(TAG);
        LocalBroadcastManager.getInstance(getBaseContext()).registerReceiver(broadcastReceiver, iff);
    }

    @Override
    protected void onPause() {
        super.onPause();

        LocalBroadcastManager.getInstance(getBaseContext()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_setting, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        mDrawerFragment.setCloseDrawer(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return mDrawerFragment.mDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }


    private ItemClickSupport.OnItemLongClickListener mItemLongClickListener = new ItemClickSupport.OnItemLongClickListener() {
        @Override
        public boolean onItemLongClicked(RecyclerView recyclerView, final int position, View v) {
            return false;
        }
    };

    private View.OnClickListener mToolbarOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mDrawerFragment.setOpenCloseDrawer();
        }
    };
}
