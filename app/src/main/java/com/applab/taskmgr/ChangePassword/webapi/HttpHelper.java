package com.applab.taskmgr.ChangePassword.webapi;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.applab.taskmgr.Login.activity.login.LoginPage;
import com.applab.taskmgr.Login.database.CRUDHelper;
import com.applab.taskmgr.Login.model.Token;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Utilities.AppController;
import com.applab.taskmgr.Utilities.GsonRequest;
import com.applab.taskmgr.Utilities.Utilities;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by user on 26-Feb-16.
 */
public class HttpHelper {
    public static void putChangePassword(Context context, String TAG, final String oldPassword,
                                         final String newPassword, final String confirmPassword) {
        final Token token = CRUDHelper.getToken(context);
        if (token != null) {
            if (token.getToken() != null) {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", token.getToken());
                GsonRequest<String> mGsonRequest = new GsonRequest<String>(
                        Request.Method.PUT,
                        context.getString(R.string.base_url) + "Account/ChangePassword",
                        String.class,
                        params,
                        responsePasswordListener(context, TAG),
                        errorPasswordListener(context, TAG)) {
                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        String httpbody = "OldPassword=" + oldPassword + "&NewPassword=" +
                                newPassword + "&ConfirmPassword=" + confirmPassword;
                        return httpbody.getBytes();
                    }
                };

                mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utilities.TIMEOUT
                        , DefaultRetryPolicy.DEFAULT_MAX_RETRIES
                        , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
            }
        }
    }

    public static Response.Listener<String> responsePasswordListener(final Context context, final String TAG) {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Utilities.showError(context, "", response);
                Intent intent = new Intent(context, LoginPage.class);
                context.startActivity(intent);
                ((Activity)context).finish();
            }
        };
    }

    public static Response.ErrorListener errorPasswordListener(final Context context, final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Intent intent = new Intent(TAG);
                intent.putExtra("failed", true);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                Utilities.serverHandlingError(context, error);
            }
        };
    }
}
