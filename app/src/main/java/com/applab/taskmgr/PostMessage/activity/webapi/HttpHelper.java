package com.applab.taskmgr.PostMessage.activity.webapi;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Task.model.Task;
import com.applab.taskmgr.Utilities.AppController;
import com.applab.taskmgr.Utilities.Utilities;

import java.io.File;
import java.util.HashMap;

/**
 * Created by user on 1/3/2016.
 */
public class HttpHelper {
    //region profile region
    public static void postApi(Context context, String TAG, Task task, String message, File attachment, MultiPartRequest.MultipartProgressListener multipartProgressListener) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", com.applab.taskmgr.Login.database.CRUDHelper.getToken(context).getToken());
        MultiPartRequest<String> mGsonRequest = new MultiPartRequest<String>(
                Request.Method.POST,
                context.getString(R.string.base_url) + "Task/SendMessage?task_id=" + Utilities.encode(task.getTaskId()),
                String.class,
                headers,
                responsePostMessageListener(context, TAG),
                errorPostMessageListener(context, TAG),
                attachment,
                multipartProgressListener, message) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utilities.TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<String> responsePostMessageListener(final Context context, final String TAG) {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Utilities.showError(context, Utilities.CODE_SUCCESS_UPDATED, Utilities.SUCCESS_UPDATED);
                Intent intent = new Intent(TAG);
                intent.putExtra("success", true);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }
        };
    }

    public static Response.ErrorListener errorPostMessageListener(final Context context, final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Intent intent = new Intent(TAG);
                intent.putExtra("failed", true);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                Utilities.serverHandlingError(context, error);
            }
        };
    }
    //endregion
}
