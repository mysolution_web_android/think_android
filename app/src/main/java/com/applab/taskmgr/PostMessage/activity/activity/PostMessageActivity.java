package com.applab.taskmgr.PostMessage.activity.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applab.taskmgr.PostMessage.activity.service.PostMessageUploadService;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Task.model.Task;
import com.applab.taskmgr.Utilities.Utilities;

import java.io.File;

/**
 * -- =============================================
 * -- Author     : Lau Pong
 * -- Create date: 16/02/2016
 * -- Description: PostMessageActivity.java
 * -- =============================================
 * HISTORY OF UPDATE
 * <p/>
 * NO     DEVELOPER         DATETIME                      DESCRIPTION
 * *******************************************************************************
 * 1
 * 2
 */

public class PostMessageActivity extends AppCompatActivity {
    private Toolbar mToolbar;
    private TextView mTxtToolbarTitle, mTxtPhoto;
    private EditText mEdiMessage;
    private LinearLayout mBtnSave, mBtnCancel, mBtnAttachment, mBtnPhoto;
    private ImageView mImgPhoto;
    private RelativeLayout mBtnDelete, mRlPhoto, mRl;
    private ProgressBar mProgressBar;

    private Uri mFileUri;
    private File mFile;
    private String TAG = PostMessageActivity.class.getSimpleName();
    private Task mTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_message);

        mToolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(mToolbar);
        mToolbar.setFitsSystemWindows(false);
        mToolbar.setPadding(0, 0, 0, 0);
        mTxtToolbarTitle = (TextView) mToolbar.findViewById(R.id.txtTitle);
        mTxtToolbarTitle.setText(getString(R.string.title_activity_post_message));

        mTxtPhoto = (TextView) findViewById(R.id.txtPhoto);
        mEdiMessage = (EditText) findViewById(R.id.ediMessage);

        mBtnSave = (LinearLayout) findViewById(R.id.btnSave);
        mBtnCancel = (LinearLayout) findViewById(R.id.btnCancel);
        mBtnAttachment = (LinearLayout) findViewById(R.id.btnAttachment);
        mBtnPhoto = (LinearLayout) findViewById(R.id.btnPhoto);

        mImgPhoto = (ImageView) findViewById(R.id.imgPhoto);
        mBtnDelete = (RelativeLayout) findViewById(R.id.btnDelete);
        mRlPhoto = (RelativeLayout) findViewById(R.id.rlPhoto);
        mRl = (RelativeLayout) findViewById(R.id.fadeRL);

        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);

        mToolbar.setNavigationIcon(R.mipmap.back);
        mToolbar.setNavigationOnClickListener(mToolbarOnClickListener);

        mBtnPhoto.setOnClickListener(mBtnPhotoOnClickListener);
        mBtnSave.setOnClickListener(mBtnSaveOnClickListener);
        mBtnCancel.setOnClickListener(mBtnCancelOnClickListener);
        mBtnDelete.setOnClickListener(mBtnDeleteOnClickListener);
        mBtnAttachment.setOnClickListener(mBtnAttachmentOnClickListener);
        setIntent(getIntent());
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(TAG)) {
                if (intent.getBooleanExtra("success", false)) {
                    Utilities.setFadeProgressBarVisibility(false, mRl, mProgressBar);
                    finish();
                } else if (intent.getBooleanExtra("failed", false)) {
                    Utilities.setFadeProgressBarVisibility(false, mRl, mProgressBar);
                }
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter iff = new IntentFilter(TAG);
        LocalBroadcastManager.getInstance(getBaseContext()).registerReceiver(broadcastReceiver, iff);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Utilities.hideKeyboard(PostMessageActivity.this);
        LocalBroadcastManager.getInstance(getBaseContext()).unregisterReceiver(broadcastReceiver);
    }

    public void setIntent(Intent intent) {
        if (intent.getParcelableExtra("Task") != null) {
            mTask = intent.getParcelableExtra("Task");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case Utilities.SELECT_PHOTO_REQUEST_CODE:
                mFile = Utilities.previewSelectedImage(this, data.getData(), mImgPhoto, false, 0, true, null, Utilities.SIZE_LIMIT, false);
                setVisibility(true);
                break;
            case Utilities.CAMERA_CAPTURE_IMAGE_REQUEST_CODE:
                mFile = Utilities.previewCapturedImage(this, mFileUri, mImgPhoto, false, 0, true, null, Utilities.SIZE_LIMIT, false);
                setVisibility(true);
                break;
        }
    }

    private View.OnClickListener mBtnSaveOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(PostMessageActivity.this, PostMessageUploadService.class);
            intent.putExtra("Task", mTask);
            intent.putExtra("file", mFile);
            intent.putExtra("message", mEdiMessage.getText().toString());
            startService(intent);
            Utilities.setFadeProgressBarVisibility(true, mRl, mProgressBar);
            Utilities.hideKeyboard(PostMessageActivity.this);
        }
    };

    private View.OnClickListener mBtnCancelOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    private View.OnClickListener mBtnDeleteOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            setVisibility(false);
        }
    };

    private void setVisibility(boolean status) {
        if (!status) {
            mFile = null;
            mFileUri = null;
            mTxtPhoto.setVisibility(View.GONE);
            mRlPhoto.setVisibility(View.GONE);
            mBtnAttachment.setVisibility(View.VISIBLE);
            mBtnPhoto.setVisibility(View.VISIBLE);
            mRlPhoto.setVisibility(View.GONE);
        } else {
            mTxtPhoto.setVisibility(View.VISIBLE);
            mRlPhoto.setVisibility(View.VISIBLE);
            mBtnAttachment.setVisibility(View.GONE);
            mBtnPhoto.setVisibility(View.GONE);
            mRlPhoto.setVisibility(View.VISIBLE);
        }
    }

    private View.OnClickListener mBtnPhotoOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (Utilities.isDeviceSupportCamera(PostMessageActivity.this)) {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                mFileUri = Utilities.getOutputMediaFileUri(PostMessageActivity.this, true, null);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, mFileUri);
                startActivityForResult(cameraIntent, Utilities.CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
            } else {
                Utilities.showError(PostMessageActivity.this, Utilities.CODE_CAMERA_SUPPORT, Utilities.CAMERA_SUPPORT);
            }
        }
    };

    private View.OnClickListener mBtnAttachmentOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
            photoPickerIntent.setType("image/*");
            startActivityForResult(photoPickerIntent, Utilities.SELECT_PHOTO_REQUEST_CODE);
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_post_message, menu);
        return true;
    }

    private View.OnClickListener mToolbarOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    @Override
    public void onBackPressed() {
        finish();
    }
}
