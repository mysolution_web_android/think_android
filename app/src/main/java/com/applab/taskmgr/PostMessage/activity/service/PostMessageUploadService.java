package com.applab.taskmgr.PostMessage.activity.service;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.applab.taskmgr.PostMessage.activity.activity.PostMessageActivity;
import com.applab.taskmgr.PostMessage.activity.webapi.HttpHelper;
import com.applab.taskmgr.PostMessage.activity.webapi.MultiPartRequest;
import com.applab.taskmgr.R;
import com.applab.taskmgr.Task.model.Task;
import com.applab.taskmgr.Utilities.Utilities;

import java.io.File;

/**
 * Created by user on 1/3/2016.
 */
public class PostMessageUploadService extends IntentService implements MultiPartRequest.MultipartProgressListener {
    private String mMessage;
    private int mId = 151;
    private String TAG = PostMessageActivity.class.getSimpleName();
    private File mFile;
    private Task mTask;

    public PostMessageUploadService() {
        super(PostMessageUploadService.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        mMessage = intent.getStringExtra("message");
        mFile = (File) intent.getSerializableExtra("file");
        mTask = intent.getParcelableExtra("Task");
        if (mFile != null) {
            if (Utilities.getUsableSpace() < mFile.length()) {
                Utilities.showError(getBaseContext(), Utilities.CODE_LOCAL_SIZE_EXCEEDED, Utilities.LOCAL_SIZE_EXCEED);
            } else {
                if (Utilities.isValidImage(mFile, getBaseContext())) {
                    HttpHelper.postApi(getBaseContext(), TAG, mTask, mMessage, mFile, PostMessageUploadService.this);
                }
            }
        } else {
            HttpHelper.postApi(getBaseContext(), TAG, mTask, mMessage, null, PostMessageUploadService.this);
        }
    }

    @Override
    public void transferred(long transferred, long progress) {
        final NotificationManager mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setContentTitle(mFile.getName())
                .setContentText(Utilities.UPLOAD_PROGRESS)
                .setSmallIcon(R.mipmap.ic_launcher);

        // Start a lengthy operation in a background thread
        mBuilder.setProgress(100, (int) progress, false);
        // Displays the progress bar for the first time.
        mNotifyManager.notify(mId, mBuilder.build());
        // When the loop is finished, updates the notification
        if (100 == progress) {
            mBuilder.setContentText(Utilities.UPLOAD_COMPLETE)
                    // Removes the progress bar
                    .setProgress(0, 0, false);
            mNotifyManager.notify(mId, mBuilder.build());
        }
    }
}
