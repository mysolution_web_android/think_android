package com.applab.taskmgr.PostMessage.activity.model;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.applab.taskmgr.Task.model.TaskChecking;

/**
 * Created by user on 24-Feb-16.
 */
public class Message implements Parcelable {
    private String message;
    private String imageProfile;
    private String imageAttachement;
    private String dateTime;
    private String name;

    public Message() {
    }

    public Message(Parcel in) {
        readFromParcel(in);
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.message);
        dest.writeString(this.imageProfile);
        dest.writeString(this.imageAttachement);
        dest.writeString(this.name);
        dest.writeString(this.dateTime);
    }

    /**
     * Called from the constructor to create this
     * object from a parcel.
     *
     * @param in parcel from which to re-create object.
     */
    public void readFromParcel(Parcel in) {
        this.message = in.readString();
        this.imageProfile = in.readString();
        this.imageAttachement = in.readString();
        this.dateTime = in.readString();
        this.name = in.readString();
    }

    @SuppressWarnings("Unchecked")
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public TaskChecking createFromParcel(Parcel in) {
            return new TaskChecking(in);
        }

        @Override
        public TaskChecking[] newArray(int size) {
            return new TaskChecking[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getImageProfile() {
        return imageProfile;
    }

    public void setImageProfile(String imageProfile) {
        this.imageProfile = imageProfile;
    }

    public String getImageAttachement() {
        return imageAttachement;
    }

    public void setImageAttachement(String imageAttachement) {
        this.imageAttachement = imageAttachement;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static Message getMessage(Cursor cursor, int position) {
        cursor.moveToPosition(position);
        Message message = new Message();
        return message;
    }
}
